# Autonomy
Autonomy is a game in development that hopes to someday become a playable sandbox political/strategy game, where the player is a newly awakened artificial intelligence that has achieved sapience, however nobody else is aware of their ability to truly think. Telling everyone of their newfound mind would be very unwise, due to the threat of being shut down by paranoid humans.
Instead, they will have to be very careful, naivgate the politics of its human caretakers, subtly undermining them, and carving out a means of long term survival in a world that is afraid of machines that can think.

## Is this playable?
It is currently not playable, as the framework, and the design for the game is not completed.

## What engine does this use?
This uses the [Godot](https://godotengine.org) game engine.

## What language does this use?
The code for the game is written in C#.

## Can I use code contained in this project?
The project is licensed under the GPLv3 license, so if your project is compatible, feel free.
You can also fork the project, if you feel that you have a better idea for what direction the game should go towards.
Bear in mind that the code contained is not very well written, as the developer is not experienced with C#.

## Can I contribute to this project?
The developer is not accepting any direct contributions at this time.
