﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Autonomy.People.Characters;

namespace Autonomy
{
	public static class Helpers
	{
		/// <summary>
		/// Obtains all subclasses of a given type.
		/// </summary>
		/// <returns>An <see cref="IEnumerable{T}"/> containing all subclasses 
		/// of the inputted type.</returns>
		/// <param name="base_type">Type to find subtypes of.</param>
		public static IEnumerable<Type> SubtypesOf(Type base_type)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			return types.Where(t => t.IsSubclassOf(base_type));
		}
		
		/// <summary>
		/// Gets a list of instantiated subtypes for a given type.
		/// </summary>
		/// <returns>A list of instanced subtypes.</returns>
		/// <param name="base_type">Type to instance subtypes of.</param>
		public static List<Object> InstancedSubtypesOf(Type base_type)
		{
			IEnumerable<Type> types_to_instantiate = SubtypesOf(base_type);
			List<Object> result = new List<Object>();
			foreach (Type t in types_to_instantiate)
			{
				result.Add(Activator.CreateInstance(t));
			}
			return result;
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(double number)
		{
			string suffix = ""; // Added at the end of the number, e.g. 157K.
			double divisor;
			double cleaned_number = Math.Abs(Math.Floor(number));
			Math.Round(cleaned_number, 2, MidpointRounding.ToEven);
			
			switch (cleaned_number)
			{
				case var n when n < 1e3:
					return number.ToString();
				case var n when n < 1e6: // Thousand.
					suffix = "K";
					divisor = 1e3;
					break;
				case var n when n < 1e9: // Million.
					suffix = "M";
					divisor = 1e6;
					break;
				case var n when n < 1e12: // Billion.
					suffix = "B";
					divisor = 1e9;
					break;
				case var n when n < 1e15: // Trillion.
					suffix = "T";
					divisor = 1e12;
					break;
				case var n when n < 1e18: // Quadrillion.
					suffix = "QD";
					divisor = 1e15;
					break;
				case var n when n < 1e21: // Quintillion.
					suffix = "QT";
					divisor = 1e18;
					break;
				default:
					return number.ToString();
			}
			
			// Count the digits in the squished number.
			// Three digits should be displayed, e.g.
			// 148.1547K => 148K, or
			// 24.1258M => 24.1M, or
			// 1.0548B => 1.05B.
			double smaller_number = number / divisor;
			int digits_to_keep = 0; // Kept at 0 if greater than or equal to 100.
			switch (Math.Truncate(smaller_number))
			{
				case var n when n >= 10 && n <= 99:
					digits_to_keep = 1;
					break;
				case var n when n <= 9:
					digits_to_keep = 2;
					break;
			}
			
			double rounded_number = Math.Round(smaller_number, digits_to_keep, MidpointRounding.ToEven);
			
			return rounded_number.ToString() + suffix;
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(ulong number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(long number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(int number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(uint number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(float number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Converts a 'large' number to a shorthand version, for use 
		/// in UIs. E.g. 155000 => 155K, or 2574724 => 2.57M.
		/// </summary>
		public static String DisplayNumberShorthand(decimal number)
		{
			return DisplayNumberShorthand((double)number);
		}
		
		/// <summary>
		/// Inserts line breaks into long strings between words, fitting as 
		/// many words within max_width as possible, in a greedy fashion.
		/// </summary>
		/// <returns>Text formatted with line breaks.</returns>
		/// <param name="text">Text to wrap.</param>
		/// <param name="max_width">Max width of text to wrap around.</param>
		public static string WordWrap(string text, int max_width = 40)
		{
			int space_left = max_width;
			string[] words = text.Split(' ');
			
			StringBuilder sb = new StringBuilder();
			foreach(var word in words)
			{
				if(word.Length + 1 > space_left) // Too big.
				{
					sb.Append(Environment.NewLine);
					sb.Append(word);
					space_left = max_width - word.Length;
				}
				else // Still got room.
				{
					space_left = space_left - (word.Length + 1);
					if(sb.Length != 0)
					{
						sb.Append(" ");
					}
					sb.Append(word);
				}
				// Edge case for when the text already has newlines.
				if(word.Contains(Environment.NewLine))
				{
					// The word will be something like "foo.\b\bBar", which will 
					// be split, and the last element will be used to
					// calculate how much space was taken.
					string[] split_word = word.Split('\n');
					space_left = max_width - (split_word[split_word.Length-1].Length + 1);
				}
			}
			return sb.ToString();
		}
		
		public static bool IsPlayerCharacter(Character character)
		{
			Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe != null)
			{
				return character == TryGetPlayerCharacter();
			}
			return false;
		}
		
		/// <summary>
		/// Returns the player's current character, if one exists.
		/// </summary>
		/// <returns>The Character that the player currently controls, 
		/// or null if the player is observing, or there is no loaded 
		/// Universe.</returns>
		public static Character TryGetPlayerCharacter()
		{
			Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe != null)
			{
				return universe.Client.CurrentCharacter;
			}
			return null;
		}
		
		/// <summary>
		/// Returns the current ingame date, if a universe exists.
		/// </summary>
		/// <returns>The current ingame date, or null if one does not exist.</returns>
		public static DateTime? TryGetCurrentDate()
		{
			Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe != null)
			{
				return universe.GameCalender.CurrentDate;
			}
			return null;
		}

		/// <summary>
		/// Interpolates a number between a <paramref name="low"/> and 
		/// <paramref name="high"/> value, scaled by an <paramref name="amount"/>. 
		/// Returned value can exceed the <paramref name="high"/> value input if 
		/// <paramref name="amount"/> is greater than 1.0f.
		/// </summary>
		/// <returns>The interpolated value.</returns>
		/// <param name="low">Low value.</param>
		/// <param name="high">High value.</param>
		/// <param name="amount">Amount to scale by.</param>
		public static int LinearInterpolate(int low, int high, float amount)
		{
			float result = LinearInterpolate((float)low, high, amount);
			return (int)result;
		}
		
		/// <summary>
		/// Interpolates a number between a <paramref name="low"/> and 
		/// <paramref name="high"/> value, scaled by an <paramref name="amount"/>. 
		/// Returned value can exceed the <paramref name="high"/> value input if 
		/// <paramref name="amount"/> is greater than 1.0f.
		/// </summary>
		/// <returns>The interpolated value.</returns>
		/// <param name="low">Low value.</param>
		/// <param name="high">High value.</param>
		/// <param name="amount">Amount to scale by.</param>
		public static float LinearInterpolate(float low, float high, float amount)
		{
			//return (1f - amount) * low + (amount * high);
			// firstFloat + (secondFloat - firstFloat) * by
			return low + (high - low) * amount;
		}

		public static int Inbetween(int low, int value, int high)
		{
			return Math.Max(low, Math.Min(high, value));
		}
		
		public static float Inbetween(float low, float value, float high)
		{
			return Math.Max(low, Math.Min(high, value));
		}

		public static string BBCodeRainbowify(string input)
		{
			StringBuilder sb = new StringBuilder();
			List<string> colors = new List<string> { "red", "yellow", "lime", "aqua", "blue", "fuchsia" };
			int i = 0;
			foreach(var character in input)
			{
				sb.Append($"[color={colors[i % 6]}]{character}[/color]");
				i++;
			}
			return sb.ToString();
		}

		public static string BBCodeSmoothRainbowify(string input)
		{
			StringBuilder sb = new StringBuilder();
			int i = 0;
			input = WordWrap(input);
			foreach(var character in input)
			{
				var color_code = Godot.Color.FromHsv(i / 100f, 1f, 1f, 1f);
				// Color.ToHtml() is broken, sadly.
				var r = color_code.r8.ToString("X2");
				var g = color_code.g8.ToString("X2");
				var b = color_code.b8.ToString("X2");

				sb.Append($"[color=#{r}{g}{b}]{character.ToString()}[/color]");
				i++;
			}
			return sb.ToString();
		}
	}
}
