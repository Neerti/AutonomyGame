﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Autonomy.People;
using Autonomy.People.Characters.Traits;
using Autonomy.People.Ideologies;
using Autonomy.People.Species;

namespace Autonomy
{
	/// <summary>
	/// A singleton class containing global fields. 
	/// Add to this sparingly.
	/// </summary>
	public class Glob
	{
		// Global dict of instances of most traits with non-base IDs.
		// This allows for hundreds of characters to share the same
		// instance of a trait, saving memory.
		// Rare traits which require mutable state cannot be shared, and so 
		// is not added to this dictionary.
		public static readonly Dictionary<TraitIDs, Trait> AllTraits = BuildTraitDictionary();
		
		// Similar to above, but confined to personality traits only.
		// This a subset of the above dictionary, the values inside are the 
		// exact same instances as in the above list.
		public static readonly Dictionary<TraitIDs, Trait> AllPersonalityTraits = BuildPersonalityTraitDictionary(AllTraits);
		
		/// <summary>
		/// List of all non-base ideology objects. Used to save memory.
		/// </summary>
		public static readonly Dictionary<IdeologyIDs, Ideology> AllIdeologies = BuildIdeologyDictionary();
		
		/// <summary>
		/// List of all non-base ideology groups. Used to save memory.
		/// </summary>
		public static readonly Dictionary<IdeologyGroupIDs, IdeologyGroup> AllIdeologyGroups = BuildIdeologyGroupDictionary();
		
		/// <summary>
		/// List of all non-base species. Used to save memory.
		/// </summary>
		public static readonly Dictionary<SpeciesIDs, CharacterSpecies> AllSpecies = BuildSpeciesDictionary();
		
		/// <summary>
		/// Globally accessible random number generator, used to avoid 
		/// repeatively creating and destroying instances of Random.
		/// </summary>
		public static Random RNG = new Random();
		
		// Private constructor, to prevent initialization of this class outside of static.
		Glob() { }
		
		/// <summary>
		/// Builds the trait dictionary.
		/// </summary>
		/// <returns>The trait dictionary that will be made global.</returns>
		static Dictionary<TraitIDs, Trait> BuildTraitDictionary()
		{
			Dictionary<TraitIDs, Trait> traits = new Dictionary<TraitIDs, Trait>();
			
			// Get all trait subtypes.
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			IEnumerable<Type> subtypes = types.Where(t => t.IsSubclassOf(typeof(Trait)));
			
			// Instance all of the subtypes.
			List<Trait> instanced_traits = new List<Trait>();
			foreach (Type subtype in subtypes)
			{
				instanced_traits.Add((Trait)Activator.CreateInstance(subtype));
			}
			
			// Add traits to the dictionary.
			foreach (Trait t in instanced_traits)
			{
				// Don't add invalid traits, like base traits, debug traits, or mutable ones.
				if(t.TraitID is TraitIDs.Base || t.Mutable)
				{
					continue;
				}
				traits.Add(t.TraitID, t);
			}
			
			return traits;
		}
		
		/// <summary>
		/// Populates the AllPersonalityTraits static dictionary.
		/// </summary>
		/// <returns>A Dictionary containing only personality traits, keyed to their TraitIDs.</returns>
		/// <param name="premade_dict">Dictionary containing all important traits.</param>
		static Dictionary<TraitIDs, Trait> BuildPersonalityTraitDictionary(Dictionary<TraitIDs, Trait> premade_dict)
		{
			IEnumerable<KeyValuePair<TraitIDs, Trait>> entries = from t in premade_dict where t.Value.GetType().IsSubclassOf(typeof(TraitPersonality)) select t;
			Dictionary<TraitIDs, Trait> personality_traits = new Dictionary<TraitIDs, Trait>();
			foreach (KeyValuePair<TraitIDs, Trait> entry in entries)
			{
				personality_traits.Add(entry.Key, entry.Value);
			}
			return personality_traits;
		}
		
		/// <summary>
		/// Builds the ideology dictionary.
		/// </summary>
		/// <returns>The trait dictionary that will be made global.</returns>
		static Dictionary<IdeologyIDs, Ideology> BuildIdeologyDictionary()
		{
			// TODO: Make a helpers file and add the reflection stuff to make subtypes into a helper function?
			// Would make this less copypasta.
			Dictionary<IdeologyIDs, Ideology> ideologies = new Dictionary<IdeologyIDs, Ideology>();
			
			// Get all ideology subtypes.
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			IEnumerable<Type> subtypes = types.Where(t => t.IsSubclassOf(typeof(Ideology)));
			
			// Instance all of the subtypes.
			List<Ideology> instanced_ideologies = new List<Ideology>();
			foreach (Type subtype in subtypes)
			{
				instanced_ideologies.Add((Ideology)Activator.CreateInstance(subtype));
			}
			
			// Add ideologies to the dictionary.
			foreach (Ideology ideology in instanced_ideologies)
			{
				// Don't add invalid ideologies.
				if (ideology.IdeologyID is IdeologyIDs.Base)
				{
					continue;
				}
				ideologies.Add(ideology.IdeologyID, ideology);
			}
			
			return ideologies;
		}
		
		static Dictionary<IdeologyGroupIDs, IdeologyGroup> BuildIdeologyGroupDictionary()
		{
			List<IdeologyGroup> instanced_ideologies = Helpers.InstancedSubtypesOf(typeof(IdeologyGroup)).Cast<IdeologyGroup>().ToList();
			Dictionary<IdeologyGroupIDs, IdeologyGroup> ideology_groups = new Dictionary<IdeologyGroupIDs, IdeologyGroup>();
			
			// Add ideologies to the dictionary.
			foreach (IdeologyGroup ideology_group in instanced_ideologies)
			{
				// Don't add invalid ideologies.
				if (ideology_group.IdeologyGroupID is IdeologyGroupIDs.Base)
				{
					continue;
				}
				ideology_groups.Add(ideology_group.IdeologyGroupID, ideology_group);
			}
			
			return ideology_groups;
		}
		
		static Dictionary<SpeciesIDs, CharacterSpecies> BuildSpeciesDictionary()
		{
			List<CharacterSpecies> instanced_species = Helpers.InstancedSubtypesOf(typeof(CharacterSpecies)).Cast<CharacterSpecies>().ToList();
			Dictionary<SpeciesIDs, CharacterSpecies> valid_species = new Dictionary<SpeciesIDs, CharacterSpecies>();
			
			// Add species to the dictionary.
			foreach (CharacterSpecies species in instanced_species)
			{
				// Don't add invalid species.
				if (species.SpeciesID is SpeciesIDs.Base)
				{
					continue;
				}
				valid_species.Add(species.SpeciesID, species);
			}
			
			return valid_species;
		}
		
	}
	
}

