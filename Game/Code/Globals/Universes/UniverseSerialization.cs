﻿using System;
using Godot;
using Godot.Collections;
using Autonomy.Calenders;
using Autonomy.States;

namespace Autonomy.Universes
{
	public partial class Universe : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			// Condense the all_states list into a Godot Array.
			Array<string> state_jsons = new Array<string>();
			foreach (var state in all_states)
			{
				state_jsons.Add(Serialization.ObjectToJSON(state));
			}
			
			// TODO: Character list serialization here.
			
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(game_calender), Serialization.ObjectToJSON(game_calender)},
				{ nameof(all_states), state_jsons},
				{ nameof(state_serial_ticker), state_serial_ticker.ToString()}
			};
		}
		
		// Note to self: Load the state/character lists in two phases.
		// First phase makes blank instances of however many objects 
		// were serialized. Second phase fills out all the information 
		// after everything exists, in order to allow for saving 
		// references by using a serial number.
		
		public void Load(Dictionary<string, object> data)
		{
			game_calender = (GameCalender)Serialization.JSONToObject((string)data[nameof(game_calender)]);
			
			// Deserializing centralized lists of objects, like States or 
			// Characters, requires a two stage process in order to be able 
			// to preserve references between serialized objects.
			
			// The first stage is concerned with making 'blank' objects exist, 
			// so that references to those objects can be passed around with 
			// their serial numbers before they are 'filled in' by the 
			// second stage.
			
			// First stage.
			// Blank States.
			Godot.Collections.Array state_data = (Godot.Collections.Array)data[nameof(all_states)];
			for (int i = 0; i < state_data.Count; i++)
			{
				State state = new State(this);
			}
			
			// TODO: Blank Characters.
			
			// Second stage.
			// States.
			for (int i = 0; i < state_data.Count; i++)
			{
				State state = all_states[i];
				string json = (string)state_data[i];
				Serialization.JSONToExistingObject(json, state);
			}
			
			// TODO: Characters.
			
		}
	}
}
