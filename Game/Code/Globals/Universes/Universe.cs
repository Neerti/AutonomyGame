﻿using System;
using System.Collections.Generic;
using Autonomy.Calenders;
using Autonomy.States;
using Autonomy.Clients;

namespace Autonomy.Universes
{
	/// <summary>
	/// This object holds mutable information about a particular game. 
	/// It persists across multiple sessions, but is not shared between 
	/// seperate save files. 
	/// Note that this object is not directly a global object, but is 
	/// instead held by a LoadedUniverse object, which is static.
	/// </summary>
	public partial class Universe
	{
		Client client;
		
		/// <summary>
		/// Holds the client object that holds information for the player 
		/// participating in the current game. 
		/// </summary>
		public Client Client => client;
		
		/// <summary>
		/// Holds a GameCalender object, which determines the game's current 
		/// date, and also progresses the flow of time forward at various rates.
		/// </summary>
		GameCalender game_calender;
		public GameCalender GameCalender => game_calender;
		
		/// <summary>
		/// A list containing all State objects for a particular savefile, 
		/// indexed by the states' serial numbers.
		/// </summary>
		List<State> all_states = new List<State>();
		public List<State> AllStates => all_states;
		
		/// <summary>
		/// Acts as a ticker for assigning newly created State objects into 
		/// the list of states.
		/// </summary>
		int state_serial_ticker;
		
		/// <summary>
		/// Sets up the object into an initial state for when 
		/// a new game is started.
		/// </summary>
		public void Initialize()
		{
			client = new Client();
			game_calender = new GameCalender();
			state_serial_ticker = 0;
			all_states = new List<State>();
		}
		
		/// <summary>
		/// Adds a newly created State object into the list of
		/// all states.
		/// </summary>
		public void AddState(State new_state)
		{
			new_state.SetSerial(state_serial_ticker++);
			//all_states[serial] = new_state;
			all_states.Add(new_state);
		}
		
	}
}
