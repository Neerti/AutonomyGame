﻿using System;
using Autonomy.Universes;

namespace Autonomy
{
	/// <summary>
	/// This holds a reference to a currently active Universe object. 
	/// Note that the loaded universe might not exist, which occurs when 
	/// there is not a savefile currently being played, such as at the 
	/// main menu.
	/// </summary>
	public static class LoadedUniverse
	{
		static Universe loaded_universe;
		
		public static Universe GetUniverse()
		{
			return loaded_universe;
		}
		
		public static void NewUniverse()
		{
			loaded_universe = new Universe();
			loaded_universe.Initialize();
		}
		
		static void UnloadUniverse()
		{
			loaded_universe = null;
		}
		
		static void LoadUniverse(Universe new_universe)
		{
			loaded_universe = new_universe;
		}
		
	}
}
