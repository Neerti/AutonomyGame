﻿using System;

namespace Autonomy
{
	/// <summary>
	/// Static class for serializing and deserializing objects which 
	/// implement <see cref="ISaveable"/>.
	/// </summary>
	public static class Serialization
	{
		/* 
		 * Serialization and Deserialization is done in two phases each.
		 * For serializing, the object to be saved has its information 
		 * recorded into a Godot Dictionary with ISaveable.Save(). 
		 * This dictionary is then fed into the Godot JSON object, 
		 * which creates a string of JSON text.
		 * 
		 * The string can then be written to disk in a file, or fed into the 
		 * deserializer to make an almost-clone of the object that was saved.
		 * 
		 * Deserialization is mostly the same, just in reverse.
		 * A Godot Dictionary is created from the JSON string. Afterwards, 
		 * a bare object is dynamically instantiated from the Type key inside 
		 * the dictionary, then the rest of the object's information is 
		 * loaded with the ISavable.Load() method.
		 */
		
		// Serialization.
		
		// Step 1, convert an object's persistable information into a 
		// C# Dictionary.
		
		/// <summary>
		/// Converts an object's persistable information into a Godot Dictionary.
		/// </summary>
		/// <returns>The dictionary containing the object's information.</returns>
		/// <param name="object_to_save">Object to save.</param>
		public static Godot.Collections.Dictionary<string, object> EncodeToDictionary(ISaveable object_to_save)
		{
			return object_to_save.Save();
		}
		/*
		// Uncomment this if it is decided to try to convert from C# Dictionaries to Godot ones.
		
		// Step 2, convert the C# Dictionary into a Godot Dictionary, 
		// so that it can be marshalled to the Godot.JSON object.
		static Godot.Collections.Dictionary<string, object> DictionaryToGodotDictionary(System.Collections.Generic.Dictionary<string, object> csharp_dict)
		{
			var godot_dict = new Godot.Collections.Dictionary<string, object>();
			foreach (var pair in csharp_dict)
			{
				// If there is a dictionary inside the dictionary, that also needs to be converted.
				if(pair.Value.GetType() == typeof(System.Collections.Generic.Dictionary<string, object>))
				{
					System.Collections.Generic.Dictionary<string, object> deep_dict = (System.Collections.Generic.Dictionary<string, object>)pair.Value;
					Godot.Collections.Dictionary<string, object> converted_deep_dict = DictionaryToGodotDictionary(deep_dict);
					godot_dict.Add(pair.Key, converted_deep_dict);
				}
				else
				{
					godot_dict.Add(pair.Key, pair.Value);
				}
				
			}
			return godot_dict;
		}
		*/

		// Step 2, convert the Godot Dictionary into a JSON stirng.
		// The string can then be saved to disk for later, or fed 
		// into the deserialization methods to almost-clone the object.
		public static string Serialize(Godot.Collections.Dictionary<string, object> dict)
		{
			return Godot.JSON.Print(dict, "\t");
		}
		
		// Deserialization.
		
		// Step 1, convert the JSON string into a Godot Dictionary.
		public static Godot.Collections.Dictionary Deserialize(string json)
		{
			// Convert the JSON string to a dictionary.
			Godot.JSONParseResult result = Godot.JSON.Parse(json);
			if (result.Error is Godot.Error.Ok)
			{
				// No issues yet.
				Godot.Collections.Dictionary decoded_info = result.Result as Godot.Collections.Dictionary;
				return decoded_info;
			}
			
			// Something went wrong.
			throw new Exception("JSON Parsing Error: " + result.Error.ToString() + "\nLine " + result.ErrorLine + "\n" + result.ErrorString);
		}
		
		// Step 2, convert the Godot Dictionary into a newly instantiated object.
		// Afterwards, apply the information that was written into the JSON from 
		// before onto the object.
		
		/// <summary>
		/// Instantiates an object, and loads information that was previously 
		/// saved from a prior object into it.
		/// </summary>
		/// <returns>The newly created object.</returns>
		/// <param name="dict">Object information provided from ISavable.SaveObject().</param>
		public static object DecodeToObject(Godot.Collections.Dictionary dict, params object[] args)
		{
			Godot.Collections.Dictionary<string, object> object_info = DictToTypedDict(dict);
			
			// Instantiate a new bare object.
			Type new_type = Type.GetType((string)object_info["Type"]);
			ISaveable new_object = (ISaveable)Activator.CreateInstance(new_type, args);
			
			// Apply the object's information to it.
			new_object.Load(object_info);
			return new_object;
		}
		
		static Godot.Collections.Dictionary<string, object> DictToTypedDict(Godot.Collections.Dictionary dict)
		{
			// Convert the Dictionary to Dictionary<string, object>.
			Godot.Collections.Dictionary<string, object> object_info = new Godot.Collections.Dictionary<string, object>();
			foreach (var pair in dict)
			{
				object_info.Add(pair.Key.ToString(), pair.Value);
			}
			
			return object_info;
		}
		
		/// <summary>
		/// Converts an object which implements <see cref="ISaveable"/> into a 
		/// JSON dictionary.
		/// </summary>
		/// <returns>The object's serialized information.</returns>
		/// <param name="object_to_serialize">Object to serialize.</param>
		public static string ObjectToJSON(ISaveable object_to_serialize)
		{
			Godot.Collections.Dictionary<string, object> object_info = EncodeToDictionary(object_to_serialize);
			return Serialize(object_info);
		}
		
		/// <summary>
		/// Recreates a serialized object in JSON into an instance of the 
		/// object.
		/// </summary>
		/// <returns>The instantiated object.</returns>
		/// <param name="json_string">Serialized data to recreate the object.</param>
		public static Object JSONToObject(string json_string, params object[] args)
		{
			Godot.Collections.Dictionary dict = Deserialize(json_string);
			return DecodeToObject(dict, args);
		}
		
		/// <summary>
		/// Applies saved data onto an object which already exists.
		/// </summary>
		/// <param name="json_string">The serialized data to apply</param>
		/// <param name="thing">An object of the same type that was serialzed.</param>
		public static void JSONToExistingObject(string json_string, ISaveable thing)
		{
			Godot.Collections.Dictionary dict = Deserialize(json_string);
			Godot.Collections.Dictionary<string, object> object_info = DictToTypedDict(dict);
			
			// Apply the object's information to it.
			thing.Load(object_info);
			
		}
	}
}
