﻿namespace Autonomy.People
{
	public enum IdeologyIDs
	{
		Base,
		// Soupism (Debug).
		TomatoSoup,
		// Sandwichism (Debug).
		SubSandwich,
		BLTSandwich,
		// Unaligned.
		NonSapience,
		Apathy,
		Undecided,
		Enigmatic,
		// Transhumanism.
		TechnoProgressivism,
		BioImmortalism,
		Cyberneticism,
		Singulatarianism,
		// Bio-Conservatism
		TODO1
	}
	
	public enum IdeologyGroupIDs
	{
		Base,
		DebugSoupism,
		DebugSandwichism,
		Transhumanism,
		Unaligned,
		BioConservatism
	}
}
