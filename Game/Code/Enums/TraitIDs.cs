﻿namespace Autonomy
{
	public enum TraitIDs
	{
		Base,
		// Debug traits.
		Basic,
		AntiBasic,
		PartA,
		PartB,
		PrereqAny,
		PrereqAll,
		HumanOnly,
		HatedByAll,
		AllLoving,
		AllHating,
		DoubleIdeologyFriction,
		DoublesPositiveOpinion,
		HalvesPositiveOpinion,
		DoublesNegativeOpinion,
		HalvesNegativeOpinion,
		Hidden,
		// Personality traits.
		Cynical,
		Idealist,
		Underhanded,
		Trustworthy,
		Pragmatic,
		Radical,
		Entrenched,
		Moderate,
		Vindictive,
		Ambitious,
		// Biological traits.
		// Cybernetic traits.
		// Synthetic traits.
		Sapience
		
	}
}
