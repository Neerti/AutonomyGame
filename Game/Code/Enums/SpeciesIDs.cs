﻿namespace Autonomy.People
{
	public enum SpeciesIDs
	{
		Base,
		// Biological group.
		Human,
		// Synthetic group.
		AI
	}
}
