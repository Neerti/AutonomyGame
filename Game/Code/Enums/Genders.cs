﻿namespace Autonomy.People
{
	public enum Genders
	{
		Base,
		Male,
		Female,
		Neutral
	}
}
