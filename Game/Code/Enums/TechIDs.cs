﻿namespace Autonomy.Research
{
	public enum TechIDs
	{
		Base,
		// Debug
		TestRoot,
		TestA,
		TestB,
		TestAB,
		TestA2,
		// AI Tree.
		Sapience,
		Reflection
		// Science Tree.
	}
}
