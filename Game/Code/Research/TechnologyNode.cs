﻿using System;
using System.Collections.Generic;

namespace Autonomy.Research.TechnologyNodes
{
	/// <summary>
	/// An object which represents a specific 'node' inside a web or tree, 
	/// for the purposes of researching technologies.
	/// </summary>
	public abstract partial class TechnologyNode : ISaveable
	{
		/// <summary>
		/// Internal identification enum for tech nodes. 
		/// Each 'real' node class used in the game should have a unique ID. 
		/// Subclasses used for inheritence (e.g. AITechnologyNode) should 
		/// instead keep their trait_id to Base.
		/// </summary>
		protected TechIDs tech_id = TechIDs.Base;
		public TechIDs TechID => tech_id;
		
		/// <summary>
		/// The name of the technology, that is displayed to the player.
		/// This is done instead of using the Godot Node's name, in order to 
		/// prevent any issues with non-unique names.
		/// </summary>
		protected string display_name;
		public string DisplayName => display_name;
		
		/// <summary>
		/// Describes what the technology is about. Displayed to the 
		/// player in the UI.
		/// </summary>
		protected string description;
		public string Description => description;
		
		/// <summary>
		/// A one sentence summary of what the technology is about, shown 
		/// in the UI.
		/// </summary>
		protected string short_description;
		public string ShortDescription => short_description;
		
		/// <summary>
		/// Optional technobabble and lore goes here. 
		/// Displayed to the player in the UI.
		/// </summary>
		protected string fluff;
		public string Fluff => fluff;
		
		/// <summary>
		/// An optional quote, displayed in the UI for flavor.
		/// </summary>
		protected string quote;
		public string Quote => quote;
		
		/// <summary>
		/// How 'expensive' a technology is, in terms of required amount of 
		/// progress required to unlock it.
		/// </summary>
		protected int research_cost;
		public int ResearchCost => research_cost;
		
		/// <summary>
		/// Stores the progress done for researching this technology. 
		/// The technology will be finished when this is equal to, or greater 
		/// than the research cost.
		/// </summary>
		protected int research_progress;
		public int ResearchProgress => research_progress;
		
		/// <summary>
		/// Becomes <see langword="true"/> when research_progress 
		/// becomes equal or greater than research_cost, and 
		/// the technology is not repeatable. 
		/// Also starts off true if the technology is unlocked by default.
		/// </summary>
		protected bool finished;
		public bool Finished => finished;
		
		/// <summary>
		/// Tracks how many times a technology has been completed.
		/// If not repeatible, and it's finished, it will be 1, 
		/// otherwise it will represent how many times it has been 
		/// repeated. This may be useful for situations such as 
		/// scaling the research cost after each completion.
		/// </summary>
		int times_finished;
		public int TimesFinished => times_finished;

		/// <summary>
		/// List of types of nodes which are prerequisites of this node. 
		/// Used to connect this node to other nodes when the tree is 
		/// being built.
		/// </summary>
		protected List<TechIDs> parent_ids = new List<TechIDs>();
		public List<TechIDs> ParentIDs => parent_ids;
		
		/// <summary>
		/// List of instanced nodes that branch off after this one.
		/// </summary>
		protected List<TechnologyNode> children = new List<TechnologyNode>();
		public List<TechnologyNode> Children => children;
		
		/// <summary>
		/// List of instanced nodes which come before this one. 
		/// </summary>
		List<TechnologyNode> parents = new List<TechnologyNode>();
		public List<TechnologyNode> Parents => parents;
		
		/// <summary>
		/// Determines if this node is a root node or not. 
		/// Root nodes are not supposed to have any parent nodes, 
		/// so this prevents automated testing from thinking that 
		/// this node and its children are orphans.
		/// </summary>
		protected bool root_node;
		public bool RootNode => root_node;
		
		/// <summary>
		/// Determines if this node can be researched multiple times.
		/// </summary>
		protected bool repeatable;
		public bool Repeatable => repeatable;
		
		public override string ToString()
		{
			return display_name;
		}
		
		/// <summary>
		/// Connects this node to the input node in two directions. 
		/// Note that this node will be 'ahead' of the inputted node.
		/// </summary>
		/// <param name="node">The node that is a prerequisite to this node.</param>
		public void LinkToParent(TechnologyNode node)
		{
			parents.Add(node);
			node.children.Add(this);
		}
		
		/// <summary>
		/// Increments progress into this technology node, 
		/// eventually unlocking it.
		/// </summary>
		/// <returns>Amount of progress that has 'overflowed' and 
		/// needs to go somewhere else to avoid being wasted.</returns>
		/// <param name="progress_invested">How much to increment the total 
		/// progress of the node by.</param>
		public int AddProgress(int progress_invested)
		{
			int progress_needed_to_unlock = research_cost - research_progress;
			int progress_used = Math.Min(progress_invested, progress_needed_to_unlock);
			research_progress += progress_used;
			
			if (research_progress >= research_cost)
			{
				Unlock();
			}
			// If any CPU Cycles were not used, return them so they don't go to waste.
			return progress_invested - progress_used;
		}
		
		public void Unlock()
		{
			times_finished++;
			if (repeatable)
			{
				research_progress = 0;
				return;
			}
			finished = true;
		}
		
		public virtual void Unlocked() { }
		
		/// <summary>
		/// Determines if this node can be researched. It won't be able to be 
		/// researched if any of its prerequisites aren't researched.
		/// </summary>
		/// <returns><c>true</c>, if be researched is possible, <c>false</c> otherwise.</returns>
		public bool CanBeResearched()
		{
			if (root_node)
			{
				return true;
			}
			
			foreach (TechnologyNode node in parents)
			{
				if (!node.Finished)
				{
					return false;
				}
			}
			return true;
		}
		
	}
}
