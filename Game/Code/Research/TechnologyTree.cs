﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Autonomy.Research.TechnologyNodes;

namespace Autonomy.Research.TechnologyTrees
{
	/// <summary>
	/// An object which contains and manages many TechnologyNode objects, 
	/// and acts as the representation of a tech tree. 
	/// Note that TechnologyNodes have nothing to do with Godot nodes. 
	/// Another note is that this class is NOT a regular tree 
	/// data structure (because multiple parents are possible),
	/// and is more properly defined as a directed acyclic graph.
	/// </summary>
	public partial class TechnologyTree
	{
		/// <summary>
		/// The name of the tech tree, that is displayed to the player. 
		/// For AI players, there is more than one tech tree, so 
		/// to distinguish between them, they will be named.
		/// This is done instead of using the Godot Node's name, in order to 
		/// prevent any issues with non-unique names.
		/// </summary>
		protected string display_name;
		public string DisplayName => display_name;
		
		/// <summary>
		/// Dictionary of all nodes in the tree, indexed by their type.
		/// </summary>
		Dictionary<TechIDs, TechnologyNode> nodes = new Dictionary<TechIDs, TechnologyNode>();
		public Dictionary<TechIDs, TechnologyNode> Nodes => nodes;
		
		/// <summary>
		/// The type of node that this tree will generate. 
		/// This is used to limit the tree to specific kinds of nodes.
		/// </summary>
		protected Type node_type_to_build = typeof(TechnologyNode);
		
		/// <summary>
		/// Builds the tech tree out of all subtypes of node_type.
		/// </summary>
		public void BuildTree()
		{
			// Get all desired types via reflection.
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			IEnumerable<Type> subtypes = types.Where(x => x.IsSubclassOf(node_type_to_build));
			
			// Instance them and add them to the list of nodes.
			foreach (Type type in subtypes)
			{
				TechnologyNode new_node = (TechnologyNode)Activator.CreateInstance(type);
				if (new_node.TechID != TechIDs.Base)
				{
					nodes.Add(new_node.TechID, new_node);
				}
			}
			// Connect all of them together.
			ConnectNodes();
		}
		
		void ConnectNodes()
		{
			foreach (KeyValuePair<TechIDs, TechnologyNode> entry in nodes)
			{
				foreach (var type in entry.Value.ParentIDs)
				{
					TechnologyNode prerequsite_node = nodes[type];
					entry.Value.LinkToParent(prerequsite_node);
				}
			}
		}
		
		/// <summary>
		/// Gets a list of all ancestors of a node.
		/// </summary>
		/// <returns>All ancestors of the inputted node.</returns>
		/// <param name="starting_node">Node to walk up the tree from.</param>
		public List<TechnologyNode> GetAllParentsOfNode(TechnologyNode starting_node)
		{
			return RecursiveParents(starting_node, new List<TechnologyNode>());
		}
		
		List<TechnologyNode> RecursiveParents(TechnologyNode starting_node, List<TechnologyNode> parents_found)
		{
			parents_found.AddRange(starting_node.Parents);
			foreach (TechnologyNode parent in starting_node.Parents)
			{
				return RecursiveParents(parent, parents_found);
			}
			return parents_found;
		}
		
		/// <summary>
		/// Checks if the supplied node can be reached by any root node. 
		/// Unreachable nodes mean they likely cannot be researched.
		/// </summary>
		/// <returns><c>true</c>, if node is unreachable, <c>false</c> otherwise.</returns>
		/// <param name="tested_node">Node to determine if unreachable.</param>
		public bool IsNodeUnreachable(TechnologyNode tested_node)
		{
			if(tested_node.RootNode)
			{
				// We're the root!
				return false;
			}
			foreach (TechnologyNode node in GetAllParentsOfNode(tested_node))
			{
				if(node.RootNode)
				{
					// One of our ancestors is a root node.
					return false;
				}
			}
			// No root nodes were in our ancestry.
			return true;
		}
		
		/// <summary>
		/// Iterates over all nodes in this tech tree for any nodes that are 
		/// unreachable from any root nodes.
		/// </summary>
		/// <returns>Nodes that are unreachable.</returns>
		public List<TechnologyNode> CheckForUnreachableNodes()
		{
			List<TechnologyNode> unreachable_nodes = new List<TechnologyNode>();
			foreach (KeyValuePair<TechIDs, TechnologyNode> entry in nodes)
			{
				if (IsNodeUnreachable(entry.Value))
				{
					unreachable_nodes.Add(entry.Value);
				}
			}
			return unreachable_nodes;
		}
		
	}
}
