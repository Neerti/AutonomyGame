﻿using System;
using Godot.Collections;

namespace Autonomy.Research.TechnologyNodes
{
	public abstract partial class TechnologyNode : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(ResearchProgress), ResearchProgress.ToString()},
				{ nameof(Finished), Finished.ToString()},
				{ nameof(TimesFinished), TimesFinished.ToString()}
				// The other variables like name/id/etc are inherent to 
				// the Type, so they don't need to be serialized.
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			research_progress =		Int32.Parse		((string)data[nameof(ResearchProgress)]);
			finished =				Boolean.Parse	((string)data[nameof(Finished)]);
			times_finished =		Int32.Parse		((string)data[nameof(TimesFinished)]);
		}
	}

}
