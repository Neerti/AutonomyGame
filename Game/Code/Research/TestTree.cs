﻿using System;
using System.Collections.Generic;
using Autonomy.Research.TechnologyTrees.Testing;
using Autonomy.Research.TechnologyNodes.Testing;

namespace Autonomy.Research.TechnologyTrees.Testing
{
	/// <summary>
	/// Provides a sample tech tree for use in unit testing.
	/// </summary>
	public class TestTree : TechnologyTree
	{
		public TestTree()
		{
			display_name = "Test Tree";
			node_type_to_build = typeof(TestNode);
		}
	}
}

namespace Autonomy.Research.TechnologyNodes.Testing
{
	/// <summary>
	/// Generic testing node. Subtypes of this will be automatically 
	/// created in relevant unit tests.
	/// </summary>
	public class TestNode : TechnologyNode
	{
		public TestNode()
		{
			research_cost = 100;
		}
	}
	
	/// <summary>
	/// Node that is the root of the tech tree.
	/// </summary>
	public class NodeRoot : TestNode
	{
		public NodeRoot()
		{
			tech_id = TechIDs.TestRoot;
			display_name = "I Am Root";
			root_node = true; // So other unit tests don't think this tree is unreachable.
			finished = true;
		}
	}
	
	/// <summary>
	/// Node with root as it's parent.
	/// </summary>
	public class NodeA : TestNode
	{
		public NodeA()
		{
			tech_id = TechIDs.TestA;
			display_name = "A";
			parent_ids.Add( TechIDs.TestRoot );
		}
	}
	
	/// <summary>
	/// Node with root as it's parent.
	/// </summary>
	public class NodeB : TestNode
	{
		public NodeB()
		{
			tech_id = TechIDs.TestB;
			display_name = "B";
			parent_ids.Add( TechIDs.TestRoot );
		}
	}
	
	/// <summary>
	/// Node with both the A and B nodes as parents.
	/// </summary>
	public class NodeAB : TestNode
	{
		public NodeAB()
		{
			tech_id = TechIDs.TestAB;
			display_name = "Wants A and B";
			parent_ids.AddRange(new List<TechIDs> { TechIDs.TestA, TechIDs.TestB } );
		}
	}
	
	/// <summary>
	/// Node with A as it's parent, and NOT b.
	/// </summary>
	public class NodeA2 : TestNode
	{
		public NodeA2()
		{
			tech_id = TechIDs.TestA2;
			display_name = "Descendant of A";
			parent_ids.Add( TechIDs.TestA );
		}
	}
}
