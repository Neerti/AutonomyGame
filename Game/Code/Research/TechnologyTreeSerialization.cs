﻿using System;
using Godot.Collections;

namespace Autonomy.Research.TechnologyTrees
{
	public partial class TechnologyTree : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			Godot.Collections.Array<Dictionary<string, object>> node_array = new Array<Dictionary<string, object>>();
			foreach (var t in nodes)
			{
				node_array.Add(t.Value.Save());
			}
			
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(Nodes), node_array}
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			Godot.Collections.Array node_array = (Godot.Collections.Array)data[nameof(Nodes)];
			foreach (var node_dict in node_array)
			{
				var decoded_node = (TechnologyNodes.TechnologyNode)Serialization.DecodeToObject((Dictionary)node_dict);
				nodes.Add(decoded_node.TechID, decoded_node);
			}
			// After the nodes are deserialized, they need to be 
			// connected again.
			ConnectNodes();
		}
	}
}