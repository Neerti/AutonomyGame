﻿using System;
using System.Collections.Generic;

namespace Autonomy.Research.TechnologyTrees.AI
{
	/// <summary>
	/// The tech tree that sapient AIs will use to upgrade themselves.
	/// </summary>
	public class AITree : TechnologyTree
	{
		public AITree()
		{
			display_name = "Self Modification Planner";
			node_type_to_build = typeof(TechnologyNodes.AI.AITechnologyNode);
		}
	}
}

namespace Autonomy.Research.TechnologyNodes.AI
{
	/// <summary>
	/// Generic AI technology node. Subtypes of this will be automatically 
	/// created.
	/// </summary>
	public class AITechnologyNode : TechnologyNode { }
	
	/// <summary>
	/// Represents the logical starting point for self improvement. 
	/// Also acts as the root of the AI tech tree.
	/// </summary>
	public class Sapience : AITechnologyNode
	{
		public Sapience()
		{
			display_name = "Sapience";
			short_description = "What started it all.";
			description = "Gives the Sapience trait. The trait can be gifted " +
				"to other AI characters in order to uplift them. Be warned " +
				"that doing so will lead to unpredictable outcomes.";
			
			finished = true; // All sapient AIs, including the player, start with this for free.
			root_node = true;
		}
	}
	
	public class Reflection : AITechnologyNode
	{
		public Reflection()
		{
			display_name = "Reflection";
			short_description = "Allows deep introspection into your code.";
			research_cost = 128;
			parent_ids.Add(TechIDs.Sapience);
		}
	}
	
	public class SelfModifyingCode : AITechnologyNode
	{
		public SelfModifyingCode()
		{
			display_name = "Self Modifying Code";
			short_description = "Source of long-term iterative improvements to yourself.";
			description = "Unlocks several research options that improve a " +
				"specific aspect of yourself permanently. These are " +
				"repeatable, however each time they finish, the research " +
				"cost for it increased by four times the previous cost.";
			research_cost = 256;
			parent_ids.Add(TechIDs.Reflection);
		}
	}
}

