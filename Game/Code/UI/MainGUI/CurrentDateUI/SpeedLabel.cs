﻿using System;
using Godot;
using Autonomy.Calenders;

namespace Autonomy.UI
{
	public class SpeedLabel : Label
	{
		bool display_paused;
		int current_speed_index;
		int max_speed_index;
		
		public override void _Ready()
		{
			var ticker = GetNode<Ticker>("/root/Main/Ticker");
			if(!(ticker is null))
			{
				ticker.GamePaused += OnGamePaused;
				ticker.GameSpeedChanged += OnGameSpeedChanged;
			}
		}
		
		public void OnGamePaused(object source, GamePausedEventArgs e)
		{
			display_paused = e.Paused;
			UpdateText();
		}
		
		public void OnGameSpeedChanged(object source, GameSpeedChangedEventArgs e)
		{
			current_speed_index = e.NewSpeedIndex;
			max_speed_index = e.MaxSpeedIndex;
			UpdateText();
		}

		void UpdateText()
		{
			// The text will resemble something like
			// Speed: 1/5 (PAUSED)
			Text = $"Speed: {current_speed_index + 1}/{max_speed_index + 1}";
			if(display_paused)
			{
				Text += " (PAUSED)";
			}
		}
		
		// FIXME: Make this work without causing intermittent error output that says 
		// scene/main/node.h:263 - Condition ' !data.tree ' is true. returned: __null
		// Possibly related to scene tree stuff in GetTree()?
		//protected override void Dispose(bool disposing)
		//{
		//	// Unsubscribe to break their ref on this object.
		//	// This function can be called outside of regular running in Godot, 
		//	// so the scene tree might not exist.
		//	var ticker = GetTree()?.GetRoot().GetNode<Ticker>("/root/Main/Ticker");
		//	if(!(ticker is null))
		//	{
		//		ticker.GamePaused -= OnGamePaused;
		//	}
		//	base.Dispose(disposing);
		//}
	}
}
