using Godot;
using System;

namespace Autonomy.UI
{
	public class CurrentDateLabel : Label
	{
		DateTime current_date;
		
		/// <summary>
		/// Updates the displayed date on the label. 
		/// Should be called when starting out, and every day that passes.
		/// </summary>
		void UpdateDate()
		{
			// TODO: Format this in the user's preferred date format, as defined
			// in the future preferences object/file.
			Text = current_date.ToString("MM/dd/yyyy");
		}
		
		void OnDateChanged(object source, Calenders.DateChangedEventArgs e)
		{
			current_date = e.NewDate;
			UpdateDate();
		}
		
		public override void _Ready()
		{
			var universe = LoadedUniverse.GetUniverse();
			if(universe is null)
			{
				Text = "NO DATE FOUND";
				return;
			}
			universe.GameCalender.DateChanged += OnDateChanged;
			current_date = universe.GameCalender.CurrentDate; // Pull the current date, just this one time.
			UpdateDate();
		}
		
		protected override void Dispose(bool disposing)
		{
			// Unsubscribe to break their ref on this object.
			Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe != null)
			{
				universe.GameCalender.DateChanged -= OnDateChanged;
			}
			base.Dispose(disposing);
		}
	}
}

