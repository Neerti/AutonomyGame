﻿using System;
using Godot;
using Autonomy.Calenders;

namespace Autonomy.UI
{
	public class IncreaseSpeedButton : Button
	{
		public override void _Pressed()
		{
			var ticker = GetNode<Ticker>("/root/Main/Ticker");
			if(!(ticker is null))
			{
				ticker.SetSpeed(ticker.SpeedIndex + 1);
			}
		}
	}
}
