﻿using Autonomy.Calenders;
using Godot;
using System;

namespace Autonomy.UI
{
	public class PauseButton : Button
	{
		public override void _Ready()
		{
			var ticker = GetNode<Ticker>("/root/Main/Ticker");
			if(!(ticker is null))
			{
				ticker.GamePaused += OnGamePaused;
			}
		}
		
		public override void _Toggled(bool buttonPressed)
		{
			var ticker = GetNode<Ticker>("/root/Main/Ticker");
			ticker.PauseGame(!ticker.IsPaused());
		}
		
		public void OnGamePaused(object source, GamePausedEventArgs e)
		{
			Pressed = e.Paused;
		}

		// FIXME: Make this work without causing intermittent error output that says 
		// scene/main/node.h:263 - Condition ' !data.tree ' is true. returned: __null
		// Possibly related to scene tree stuff in GetTree()?
		//protected override void Dispose(bool disposing)
		//{
		//	// Unsubscribe to break their ref on this object.
		//	// This function can be called outside of regular running in Godot, 
		//	// so the scene tree might not exist.
		//	var ticker = GetTree()?.GetRoot().GetNode<Ticker>("/root/Main/Ticker");
		//	if(!(ticker is null))
		//	{
		//		ticker.GamePaused -= OnGamePaused;
		//	}
		//	base.Dispose(disposing);
		//}
	}
}
