﻿using System;
using Godot;
using Autonomy;
using Autonomy.People;
using Autonomy.People.Characters;

namespace Autonomy.UI
{
	public class CharacterNameButton : Button
	{
		Character current_character;

		Character CurrentCharacter
		{
			get
			{
				return current_character;
			}

			set
			{
				current_character = value;
				UpdateText();
			}
		}

		public void UpdateText()
		{
			if(CurrentCharacter != null)
			{
				SetText(CurrentCharacter.DisplayName);
				return;
			}
			SetText("(Observer)");
		}

		public override void _Ready()
		{
			// TODO: Make a hub object so this doens't need to talk to the client to subscribe.
			Autonomy.Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe == null)
			{
				SetText("NO UNIVERSE FOUND");
				return;
			}
			universe.Client.CharacterChanged += OnCharacterChanged;
			CurrentCharacter = universe.Client.CurrentCharacter;
			UpdateText();
		}

		public void OnCharacterChanged(object source, Autonomy.Clients.CharacterChangedEventArgs e)
		{
			CurrentCharacter = e.NewCharacter;
		}

		/// <summary>
		/// Pressing the button will open a character window for the player's current character.
		/// </summary>
		public override void _Pressed()
		{
			// TODO: Make a WindowManager object to do this?
			PackedScene character_window_scene = (PackedScene)ResourceLoader.Load("res://Code/UI/CharacterWindow/CharacterWindow.tscn");
			var window = (CharacterWindow)character_window_scene.Instance();
			window.Subject = CurrentCharacter;
			GetNode<CanvasLayer>("/root/Main/GUI").AddChild(window);
			
			// Testing.
			window = (CharacterWindow)character_window_scene.Instance();
			Character character = new Character("John Doe", Genders.Male, SpeciesIDs.Human, IdeologyIDs.TechnoProgressivism);
			character.FillOutCharacter();
			
			var target = Helpers.TryGetPlayerCharacter();
			var date = Helpers.TryGetCurrentDate().Value;
			character.AddTemporaryOpinionModifier(target, -25, "For no raisins.", date, new TimeSpan(30, 0, 0, 0));
			
			window.Subject = character;
			GetNode<CanvasLayer>("/root/Main/GUI").AddChild(window);
		}

		protected override void Dispose(bool disposing)
		{
			// Unsubscribe to break their ref on this object.
			Universes.Universe universe = LoadedUniverse.GetUniverse();
			if(universe != null)
			{
				universe.Client.CharacterChanged -= OnCharacterChanged;
			}
			base.Dispose(disposing);
		}
	}
}

