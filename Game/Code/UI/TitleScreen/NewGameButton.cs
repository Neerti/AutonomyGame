﻿using System;
using Godot;
using Autonomy.GameBuilders;

namespace Autonomy.UI
{
	public class NewGameButton : Button
	{
		public override void _Pressed()
		{
			// TODO: Add the ability to start with different settings.
			GameBuilder builder = new GameBuilder();
			GetTree().Root.AddChild(builder);
			builder.StartNewGame();
		}
	}
}