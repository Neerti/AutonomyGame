﻿using Godot;
using System;

namespace Autonomy.UI
{
	public class QuitButton : Button
	{
		public override void _Pressed()
		{
			GetTree().Quit();
		}
	}
}