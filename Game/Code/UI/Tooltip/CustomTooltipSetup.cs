﻿using System;
using System.Collections.Generic;
using Godot;

namespace Autonomy.UI
{
	public static class ControlExtensions
	{
		const string CUSTOM_TOOLTIP_SCENE_TREE_PATH = "/root/Main/Tooltip/CustomTooltip";
		
		/// <summary>
		/// Automatically subscribes to the custom tooltip object, informing it 
		/// when the mouse is over this <see cref="Control"/> node. Note that 
		/// this node must also implement <see cref="ITooltipDisplayable"/> for 
		/// tooltips to function.
		/// </summary>
		/// <param name="control">Control.</param>
		public static void SetupCustomTooltip(this Control control)
		{
			var tooltip = control.GetTree().GetRoot().GetNodeOrNull<CustomTooltip>(CUSTOM_TOOLTIP_SCENE_TREE_PATH);
			if(!(tooltip is null))
			{
				control.MouseFilter = Control.MouseFilterEnum.Stop; // This is needed for the below signals to function.
				control.Connect("mouse_entered", tooltip, nameof(tooltip.SetCustomTooltip), new Godot.Collections.Array { control });
				control.Connect("mouse_exited", tooltip, nameof(tooltip.HideTooltip));
			}
		}
		
		/// <summary>
		/// Disconnects the signals established by <see cref="SetupCustomTooltip(Control)"/>. 
		/// Can be safely called if they are not actually connected.
		/// </summary>
		/// <param name="control">Control-derived node which had subscribed to the custom tooltip object.</param>
		public static void UnsetCustomTooltip(this Control control)
		{
			var tooltip = control.GetTree().GetRoot().GetNodeOrNull<CustomTooltip>(CUSTOM_TOOLTIP_SCENE_TREE_PATH);
			if(!(tooltip is null))
			{
				if(control.IsConnected("mouse_entered", tooltip, nameof(tooltip.SetCustomTooltip)))
				{
					control.Disconnect("mouse_entered", tooltip, nameof(tooltip.SetCustomTooltip));
					control.Disconnect("mouse_exited", tooltip, nameof(tooltip.HideTooltip));
				}
			}
		}
	}
}