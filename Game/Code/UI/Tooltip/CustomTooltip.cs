using System;
using System.Collections.Generic;

using Godot;

namespace Autonomy.UI
{
	/// <summary>
	/// Implementation of a customized tooltip, which allows more flexibility 
	/// than the built-in version.
	/// </summary>
	public class CustomTooltip : MarginContainer
	{
		/// <summary>
		/// How offsetted the tooltip is from the mouse, so it is not always 
		/// directly underneath it.
		/// </summary>
		const float TOOLTIP_OFFSET = 5f;
		
		/// <summary>
		/// How long to wait until showing the tooltip, after mousing over a 
		/// subscribed Control node.
		/// </summary>
		/// <value>The tooltip delay.</value>
		public float TooltipDelay { get; private set; } = 0.5f;
		
		Timer tooltip_delay_timer;
		
		/// <summary>
		/// Tells the tooltip object to display information about the passed 
		/// <see cref="Control"/> object. It must implement <see cref="ITooltipDisplayable"/> 
		/// in order to be able to display anything.
		/// </summary>
		/// <param name="control">Control node which implements <see cref="ITooltipDisplayable"/>.</param>
		public void SetCustomTooltip(Control control)
		{
			// The obvious thing would be to have the parameter typed to the interface, 
			// but Godot can't handle sending non-Godot objects through Signals.
			if(control is ITooltipDisplayable) // Instead we do an ugly test and cast.
			{
				var displayable = (ITooltipDisplayable)control;
				SetTooltipText(displayable.TooltipTitle(), displayable.TooltipContents());
				return;
			}
			throw new Exception(nameof(SetCustomTooltip) + " was passed a Control node that " +
				"did not implement " + nameof(ITooltipDisplayable) + ". It was passed a '" +
				control + " (" + control.GetType() + ") instead.");
		}
		
		/// <summary>
		/// Makes the tooltip be able to be seen by the player.
		/// </summary>
		public void ShowTooltip()
		{
			RectSize = new Vector2(RectSize.x, 0);
			MoveToMouse();
			// The alpha is altered instead of toggling the visiblity, in order 
			// to avoid visiblity intefering with resizing.
			Modulate = new Color(1, 1, 1, 1);
			tooltip_delay_timer.Stop();
		}
		
		/// <summary>
		/// Makes the tooltip be hidden from the player.
		/// </summary>
		public void HideTooltip()
		{
			// The alpha is altered instead of toggling the visiblity, in order 
			// to avoid visiblity intefering with resizing.
			Modulate = new Color(1, 1, 1, 0);
			tooltip_delay_timer.Stop();
		}
		
		/// <summary>
		/// Changes the tooltip's title and contents.
		/// </summary>
		/// <param name="new_title">Title to change the tooltip to. Can be null.</param>
		/// <param name="new_body">List of strings representing paragraphs for the tooltip.</param>
		public void SetTooltipText(string new_title, List<string> new_body)
		{
			Label title_label = GetNode<Label>("PanelContainer/TooltipContent/TitleLabel");
			title_label.Text = new_title;
			if(title_label.Text == string.Empty)
			{
				title_label.Visible = false;
			}
			else
			{
				title_label.Visible = true;
			}
			TooltipBody tooltip_body = GetNode<TooltipBody>("PanelContainer/TooltipContent/TooltipBody");
			tooltip_body.UpdateBody(new_body);
			tooltip_delay_timer.Start();
		}
		
		public override void _Ready()
		{
			// Stops the tooltip from getting clicked on if the mouse ever 
			// moves over it.
			MouseFilter = MouseFilterEnum.Ignore;
			foreach(var thing in GetChildren())
			{
				if(!(thing is Control))
				{
					continue;
				}
				var node = (Control)thing;
				node.MouseFilter = MouseFilterEnum.Ignore;
			}
			// Set up the tooltip delay timer.
			tooltip_delay_timer = GetNode<Timer>("DisplayDelayTimer");
			tooltip_delay_timer.WaitTime = TooltipDelay;
			tooltip_delay_timer.Connect("timeout", this, "ShowTooltip");
			// Hide the tooltip.
			Modulate = new Color(1, 1, 1, 0);
		}

		public override void _Process(float delta)
		{
			if(Modulate.a8 == 0)
			{
				return;
			}
			MoveToMouse();
		}
		
		void MoveToMouse()
		{
			var mouse_position = GetGlobalMousePosition();
			
			// Offset the tooltip a little to the right and downward from the mouse.
			// It can't be clicked on anyways, but it looks nicer and is easier to read.
			var new_x = mouse_position.x + TOOLTIP_OFFSET;
			var new_y = mouse_position.y + TOOLTIP_OFFSET;
			
			// Avoid having the tooltip slide off the window.
			var window_size = OS.GetWindowSize();
			if(new_x + RectSize.x > window_size.x)
			{
				new_x = window_size.x - RectSize.x;
			}
			
			if(new_y + RectSize.y > window_size.y)
			{
				new_y = window_size.y - RectSize.y;
			}
			SetGlobalPosition(new Vector2(new_x, new_y));
		}
		
	}
}


