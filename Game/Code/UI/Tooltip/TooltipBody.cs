using Godot;
using System.Collections.Generic;

namespace Autonomy.UI
{
	public class TooltipBody : VBoxContainer
	{
		public void UpdateBody(List<string> body)
		{
			// First, clear what's already there.
			foreach(var child in GetChildren())
			{
				var node = (Node)child;
				RemoveChild(node);
				node.QueueFree();
			}
			
			// Now make new labels and horizontal rules.
			foreach(var paragraph in body)
			{
				if(GetChildCount() != 0)
				{
					// Every paragraph after the first gets a horizontal rule.
					HSeparator separator = new HSeparator();
					AddChild(separator);
				}
				BetterRichTextLabel label = new BetterRichTextLabel();
				label.Set("custom_constants/table_hseparation", 10);
				label.UpdateLabel(paragraph);
				label.MouseFilter = MouseFilterEnum.Ignore;
				AddChild(label);
			}
		}
	}
}


