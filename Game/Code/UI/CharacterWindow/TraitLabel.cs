﻿using System;
using System.Collections.Generic;
using Godot;

namespace Autonomy.UI
{
	/// <summary>
	/// Common class for label nodes used in CharacterWindow scenes.
	/// </summary>
	public class TraitLabel : Label, ITooltipDisplayable
	{
		public TraitIDs trait_id;
		
		public override void _Ready()
		{
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return Glob.AllTraits[trait_id].DisplayName;
		}

		public List<string> TooltipContents()
		{
			var trait = Glob.AllTraits[trait_id];
			var contents = new List<string>();
			if(!string.IsNullOrWhiteSpace(trait.Description))
			{
				contents.Add(trait.Description);
			}
			
			if(!string.IsNullOrWhiteSpace(trait.Effects))
			{
				contents.Add(trait.Effects);
			}
			
			if(!string.IsNullOrWhiteSpace(trait.Quote))
			{
				contents.Add($"[i][color=silver]\"{trait.Quote}\"[/color][/i]\n{trait.QuoteSource}");
			}
			return contents;
		}
	}
}