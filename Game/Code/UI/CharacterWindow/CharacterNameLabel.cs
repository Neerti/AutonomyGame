using Godot;
using System;

namespace Autonomy.UI.CharacterWindows
{
	public class CharacterNameLabel : Label
	{
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			SetText(window.Subject.DisplayName);
		}
	}
}


