﻿using Godot;

namespace Autonomy.UI.CharacterWindows.OpinionBoxes
{
	public class OpinionBox : PanelContainer
	{
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			Visible = !Helpers.IsPlayerCharacter(window.Subject);
		}
	}
}

