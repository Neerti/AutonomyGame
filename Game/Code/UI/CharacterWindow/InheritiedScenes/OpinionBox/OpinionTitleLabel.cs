using System.Collections.Generic;

using Godot;

namespace Autonomy.UI.CharacterWindows.OpinionBoxes
{
	public class OpinionTitleLabel : Label, ITooltipDisplayable
	{
		public override void _Ready()
		{
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return "Opinion";
		}
		
		public List<string> TooltipContents()
		{
			return new List<string> { Tr("OPINION_TOOLTIP") };
		}
	}
}
