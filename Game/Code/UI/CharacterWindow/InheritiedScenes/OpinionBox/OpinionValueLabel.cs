﻿using Autonomy.People.Characters;
using Godot;
using System;
using System.Collections.Generic;
using System.Text;

namespace Autonomy.UI.CharacterWindows.OpinionBoxes
{
	public class OpinionValueLabel : Label, ITooltipDisplayable
	{
		List<string> tooltip_content = new List<string>();
		
		public override void _Ready()
		{
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return null;
		}
		
		public List<string> TooltipContents()
		{
			return tooltip_content;
		}
		
		List<string> MakeTooltipText(DateTime current_date, Character player, Character subject)
		{
			float positive_scale = subject.TraitManager.GetPositiveOpinionValueScale();
			float negative_scale = subject.TraitManager.GetNegativeOpinionValueScale();
			var list = new List<string>();

			var scaler_paragraph = new StringBuilder();
			if(Math.Abs(positive_scale - 1.0f) > 0.01)
			{
				scaler_paragraph.Append($"Positives scaled by {positive_scale * 100}%");
			}
			if(Math.Abs(negative_scale - 1.0f) > 0.01)
			{
				if(scaler_paragraph.Length > 0)
				{
					scaler_paragraph.Append("\n");
				}
				scaler_paragraph.Append($"Negatives scaled by {negative_scale * 100}%");
			}
			if(scaler_paragraph.Length > 0)
			{
				list.Add(scaler_paragraph.ToString());
			}
			
			
			var modifier_paragraph = new StringBuilder("[table=2]");
			foreach(var modifier in subject.GetOpinionModifiers(player, current_date))
			{
				if(modifier_paragraph.Length > 0)
				{
					modifier_paragraph.Append("\n");
				}
				//modifier_paragraph.Append($"{modifier.Value.ToString()}[indent]{modifier.Reason}[/indent]");
				string color = modifier.Value < 0 ? "red" : "green";
				modifier_paragraph.Append($"[cell][color={color}]{modifier.Value.ToString()}[/color][/cell][cell]{modifier.Reason}[/cell]");
			}
			modifier_paragraph.Append("[/table]");
			list.Add(modifier_paragraph.ToString());
			return list;
		}
		
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			
			if(Helpers.IsPlayerCharacter(window.Subject))
			{
				SetText("--");
			}
			else
			{
				var player = Helpers.TryGetPlayerCharacter();
				if(player != null)
				{
					DateTime? current_date = Helpers.TryGetCurrentDate();
					if(current_date is null)
					{
						Text = "NO INGAME DATE FOUND";
						return;
					}
					int positive_value = window.Subject.GetPositiveOpinionValues(player, current_date.Value);
					int negative_value = window.Subject.GetNegativeOpinionValues(player, current_date.Value);
					int total_value = positive_value + Math.Abs(negative_value); // Absolute valued summed.
					//float divisor = Math.Max(total_value, 100);
					
					// Change the label text.
					// It will have a 'positive # / negative #' format.
					string opinion_string = positive_value.ToString() + "/" + negative_value.ToString();
					Text = opinion_string;
					
					// Color the text based on how much positive or negative the values are.
					// A high positive value, and low negative value will make the text green.
					// A low positive, and high negative values will instead make it red.
					// Both high positive and negative values will cause yellow text.
					// Finally, if both values are close to zero, it will be white colored.
					Color color = new Color();
					
					float hue_scale = Helpers.LinearInterpolate(0f, 120f, (float)positive_value / total_value);
					float saturation_scale = Helpers.LinearInterpolate(0f, 100f, total_value / 50f);
					
					// Normalize to 0-1.
					hue_scale /= 360f;
					saturation_scale /= 100f;
					
					// Apply.
					color = Color.FromHsv(hue_scale, saturation_scale, 1f);
					AddColorOverride("font_color", color);

					// Make the tooltip.
					tooltip_content = MakeTooltipText(current_date.Value, player, window.Subject);
					
					StringBuilder tip = new StringBuilder();
					float positive_scale = window.Subject.TraitManager.GetPositiveOpinionValueScale();
					float negative_scale = window.Subject.TraitManager.GetNegativeOpinionValueScale();
					
					if(Math.Abs(positive_scale - 1.0f) > 0.01)
					{
						tip.AppendLine("(Positives scaled by " + positive_scale * 100 + "%.)");
					}
					
					if(Math.Abs(negative_scale - 1.0f) > 0.01)
					{
						tip.AppendLine("(Negatives scaled by " + negative_scale * 100 + "%.)");
					}
					
					foreach(var modifier in window.Subject.GetOpinionModifiers(player, current_date.Value))
					{
						tip.AppendLine(modifier.Value.ToString() + " | " + modifier.Reason);
					}
					//UpdateTooltip(tip.ToString(), 60);
				}
				
			}
		}
	}
}

