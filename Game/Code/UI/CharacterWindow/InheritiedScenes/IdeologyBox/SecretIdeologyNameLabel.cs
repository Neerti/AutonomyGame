﻿using System.Collections.Generic;

using Autonomy.People;

using Godot;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class SecretIdeologyNameLabel : IdeologyNameLabel, ITooltipDisplayable
	{
		public override IdeologyIDs GetInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			return window.Subject.Ideology.IdeologyID; // This time, get the real ideology, every time.
		}
	}
}
