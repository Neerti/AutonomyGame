﻿using Autonomy.People;
using Godot;
using System;
using System.Collections.Generic;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class SecretIdeologyGroupNameLabel : IdeologyGroupNameLabel, ITooltipDisplayable
	{
		public override IdeologyGroupIDs GetInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			return window.Subject.Ideology.IdeologyGroup.IdeologyGroupID; // This time, get the real ideology, every time.
		}
	}
}
