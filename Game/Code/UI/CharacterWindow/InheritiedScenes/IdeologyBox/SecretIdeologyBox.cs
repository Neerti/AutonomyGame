﻿using Godot;
using System;

namespace Autonomy.UI
{
	public class SecretIdeologyBox : PanelContainer
	{
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");

			// TODO: Add other methods to detect fakers besides being one.
			Visible = Helpers.IsPlayerCharacter(window.Subject);
		}
	}
}

