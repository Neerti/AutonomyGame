using System.Collections.Generic;

using Godot;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class IdeologyTitleLabel : Label, ITooltipDisplayable
	{
		public override void _Ready()
		{
			MouseFilter = MouseFilterEnum.Stop;
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return "Ideology";
		}
		
		public List<string> TooltipContents()
		{
			return new List<string> { Tr("IDEOLOGY_TOOLTIP_1"), Tr("IDEOLOGY_TOOLTIP_2") };
		}
	}
}

