﻿using System.Collections.Generic;

using Autonomy.People;

using Godot;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class IdeologyGroupNameLabel : Label, ITooltipDisplayable
	{
		IdeologyGroupIDs ideology_group_id = IdeologyGroupIDs.Base;
		
		public void UpdateInfo()
		{
			ideology_group_id = GetInfo();
			Text = "(" + Glob.AllIdeologyGroups[ideology_group_id].DisplayName + ")";
		}
		
		public virtual IdeologyGroupIDs GetInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			return window.Subject.GetApparentIdeology().IdeologyGroup.IdeologyGroupID; // Give the fake one, if one exists.
		}

		public override void _Ready()
		{
			MouseFilter = MouseFilterEnum.Stop;
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			if(ideology_group_id is IdeologyGroupIDs.Base)
			{
				return null;
			}
			return Glob.AllIdeologyGroups[ideology_group_id].DisplayName;
		}
		
		public List<string> TooltipContents()
		{
			if(ideology_group_id is IdeologyGroupIDs.Base)
			{
				return new List<string>();
			}
			var group = Glob.AllIdeologyGroups[ideology_group_id];
			var list = new List<string>();
			
			if(!string.IsNullOrWhiteSpace(group.Description))
			{
				list.Add(group.Description);
			}
			return list;
		}
	}
}

