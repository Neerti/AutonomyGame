﻿using System.Collections.Generic;

using Autonomy.People;

using Godot;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class IdeologyNameLabel : Label, ITooltipDisplayable
	{
		IdeologyIDs ideology_id = IdeologyIDs.Base;
		
		public void UpdateInfo()
		{
			ideology_id = GetInfo();
			Text = Glob.AllIdeologies[ideology_id].DisplayName;
		}
		
		public virtual IdeologyIDs GetInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			return window.Subject.GetApparentIdeology().IdeologyID; // Give the fake one, if one exists.
		}

		public override void _Ready()
		{
			MouseFilter = MouseFilterEnum.Stop;
			this.SetupCustomTooltip();
		}

		public string TooltipTitle()
		{
			if(ideology_id is IdeologyIDs.Base)
			{
				return null;
			}
			return Glob.AllIdeologies[ideology_id].DisplayName;
		}

		public List<string> TooltipContents()
		{
			if(ideology_id is IdeologyIDs.Base)
			{
				return new List<string>();
			}
			var ideology = Glob.AllIdeologies[ideology_id];
			var list = new List<string>();
			
			if(!string.IsNullOrWhiteSpace(ideology.Description))
			{
				list.Add(ideology.Description);
			}
			if(!string.IsNullOrWhiteSpace(ideology.Effects))
			{
				list.Add(ideology.Effects);
			}
			if(!string.IsNullOrWhiteSpace(ideology.Quote))
			{
				list.Add($"[i][color=silver]\"{ideology.Quote}\"[/color][/i]\n{ideology.QuoteSource}");
			}
			
			return list;
		}
	}
}

