﻿using System.Collections.Generic;

using Autonomy.People.Ideologies;

using Godot;

namespace Autonomy.UI.CharacterWindows.IdeologyBoxes
{
	public class IdeologyAlignmentLabel : Label, ITooltipDisplayable
	{
		public override void _Ready()
		{
			MouseFilter = MouseFilterEnum.Stop;
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return "Ideological Alignment";
		}
		
		public List<string> TooltipContents()
		{
			return new List<string> {
			Tr("IDEOLOGY_ALIGNMENT_TOOLTIP"),
			Tr("IDEOLOGY_ALIGNMENT_IDENTICAL_TOOLTIP"),
			Tr("IDEOLOGY_ALIGNMENT_ALLIED_TOOLTIP"),
			Tr("IDEOLOGY_ALIGNMENT_NEUTRAL_TOOLTIP"),
			Tr("IDEOLOGY_ALIGNMENT_OPPOSED_TOOLTIP")
			};
		}
		
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			if(Helpers.IsPlayerCharacter(window.Subject))
			{
				Visible = false;
				return;
			}
			var player = Helpers.TryGetPlayerCharacter();
			if(player != null)
			{
				var alignment = window.Subject.IsAligned(player);
				Text = "Alignment: " + alignment;
				Color color = new Color();
				switch(alignment)
				{
					case Ideology.Alignment.Identical:
						color = new Color(0, 1, 1); // Cyan.
						break;
					case Ideology.Alignment.Allied:
						color = new Color(0, 1, 0); // Green.
						break;
					case Ideology.Alignment.Neutral:
						color = new Color(1, 1, 0); // Yellow.
						break;
					case Ideology.Alignment.Opposed:
						color = new Color(1, 0, 0); // Red.
						break;
				}
				AddColorOverride("font_color", color);
			}
		}
	}
}

