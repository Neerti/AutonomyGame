using System;
using Godot;
using Autonomy.People.Characters;

namespace Autonomy.UI
{
	public class CharacterWindow : WindowDialog
	{
		Character subject;
		public Character Subject
		{
			get
			{
				return subject;
			}
			set
			{
				subject = value;
				SetTitle(value.DisplayName);
				PropagateCall("UpdateInfo");
			}
		}
		
		public override void _Ready()
		{
			Visible = true;
		}
	}
}

