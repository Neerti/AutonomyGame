﻿using System.Collections.Generic;

using Autonomy.People.Species;

using Godot;

namespace Autonomy.UI.CharacterWindows
{
	public class SpeciesLabel : Label, ITooltipDisplayable
	{
		CharacterSpecies species;
		
		public override void _Ready()
		{
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			if(!(species is null))
			{
				return species.DisplayName;
			}
			return null;
		}
		
		public List<string> TooltipContents()
		{
			if(!(species is null))
			{
				return new List<string> { species.Description };
			}
			return new List<string>();
		}
		
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			species = window.Subject.Species;
			SetText(window.Subject.Species.DisplayName);
		}
	}
}
