using System.Collections.Generic;

using Godot;

namespace Autonomy.UI.CharacterWindows
{
	public class TraitTitleLabel : Label, ITooltipDisplayable
	{
		public override void _Ready()
		{
			this.SetupCustomTooltip();
		}
		
		public string TooltipTitle()
		{
			return "Traits";
		}
		
		public List<string> TooltipContents()
		{
			return new List<string> { Tr("TRAIT_TOOLTIP") };
		}
	}
}
