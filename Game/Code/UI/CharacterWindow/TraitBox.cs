using Godot;

namespace Autonomy.UI.CharacterWindows.TraitBoxes
{
	public class TraitBox : PanelContainer
	{
		public void UpdateInfo()
		{
			var window = (CharacterWindow)FindParent("CharacterWindow");
			
			// Update the trait list.
			var grid = GetNode<GridContainer>("VBoxContainer/GridContainer");
			foreach(var child in grid.GetChildren()) // First, get rid of all the labels.
			{
				var node = (Node)child;
				grid.RemoveChild(node);
				node.QueueFree();
			}
			
			// Now add new up to date labels.
			foreach(var trait in window.Subject.TraitManager.Traits)
			{
				var label = new TraitLabel();
				label.trait_id = trait.TraitID;
				label.Text = trait.DisplayName;
				if(window.Subject.TraitManager.HiddenTraits.Contains(trait))
				{
					label.Text += " (Hidden)";
				}
				label.AddColorOverride("font_color", new Color(trait.ColorCode));
				label.Align = Label.AlignEnum.Center;
				label.SizeFlagsHorizontal = (int)SizeFlags.ExpandFill;
				
				label.MouseFilter = MouseFilterEnum.Stop;
				
				grid.AddChild(label);
			}
		}
	}
}