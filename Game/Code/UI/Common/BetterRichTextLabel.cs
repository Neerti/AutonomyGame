using Godot;

namespace Autonomy.UI
{
	/// <summary>
	/// A <see cref="RichTextLabel"/> with additional code to make it less awful. 
	/// This may not be needed in future versions of Godot.
	/// </summary>
	// Code adapted from https://github.com/godotengine/godot/issues/18260#issuecomment-506948815.
	// HACK: This entire class. It should get torched if Godot receives a RichTextLabel refactor.
	public class BetterRichTextLabel : RichTextLabel
	{
		bool recalculate_height_required;
		
		public override void _Ready()
		{
			SizeFlagsVertical = (int)SizeFlags.ExpandFill;
			ScrollActive = false; // To prevent the scrollbar from briefly appearing (due to the idle_frame yield).
			ScrollFollowing = false; // To avoid a long content to jump as it's waiting for the label height being adjusted.
			RectClipContent = false;
			
			GetVScroll().Connect("changed", this, nameof(OnScrollbarChanged));
		}
		
		public override void _Process(float delta)
		{
			if(recalculate_height_required)
			{
				recalculate_height_required = false;
				RecalculateHeight();
			}
		}
		
		public void UpdateLabel(string markup)
		{
			BbcodeEnabled = true;
			BbcodeText = markup;
			recalculate_height_required = true;
		}
		
		// This will need updating in Godot 3.2, as 'changed' signal won't pass anything then.
		void OnScrollbarChanged(int something)
		{
			RecalculateHeight();
		}
		
		void RecalculateHeight()
		{
			RectSize = new Vector2(RectSize.x, GetVScroll().MaxValue);
			RectMinSize = new Vector2(RectSize.x, GetVScroll().MaxValue);
		}
	}
}

