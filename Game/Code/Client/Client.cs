﻿using System;
using Autonomy.People.Characters;

namespace Autonomy.Clients
{
	/// <summary>
	/// The Client object holds information involving the actual player in the 
	/// game.
	/// </summary>
	public class Client
	{
		Character current_character;
		
		/// <summary>
		/// Reference to the Character the player is controlling. 
		/// Changing this will cause the player to swap to the new character.
		/// Can be null if in 'observer mode' or the game hasn't finished 
		/// loading yet.
		/// </summary>
		public Character CurrentCharacter
		{
			get
			{
				return current_character;
			}
			set
			{
				Character old_character = current_character;
				current_character = value;
				OnCharacterChanged(old_character, current_character);
			}
		}
		
		public EventHandler<CharacterChangedEventArgs> CharacterChanged;
		
		protected virtual void OnCharacterChanged(Character old_character, Character new_character)
		{
			CharacterChanged?.Invoke(this, new CharacterChangedEventArgs(old_character, new_character));
		}
		
	}
	
	public class CharacterChangedEventArgs : EventArgs
	{
		public Character OldCharacter;
		public Character NewCharacter;
		
		public CharacterChangedEventArgs(Character old_char, Character new_char)
		{
			OldCharacter = old_char;
			NewCharacter = new_char;
		}
	}
}
