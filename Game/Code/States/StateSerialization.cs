﻿using System;
using Godot;
using Godot.Collections;
using Autonomy.People.Populations;

namespace Autonomy.States
{
	public partial class State : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			Array<string> vassal_serials = new Array<string>();
			foreach (var vassal in vassals)
			{
				vassal_serials.Add(vassal.state_serial.ToString());
			}
			
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(state_serial), state_serial.ToString()},
				{ nameof(display_name), display_name},
				{ nameof(demonym), demonym},
				{ nameof(suzerain), suzerain?.state_serial.ToString()}, // This might fail due to null.
				{ nameof(vassals), vassal_serials},
				{ nameof(population), Serialization.ObjectToJSON(population)}
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			state_serial =		Int32.Parse	((string)data[nameof(state_serial)]);
			display_name =		(string)data[nameof(display_name)];
			demonym =			(string)data[nameof(demonym)];
			
			if( (string)data[nameof(suzerain)] != null )
			{
				int suzerain_serial = Int32.Parse((string)data[nameof(suzerain)]);
				suzerain = my_universe.AllStates[suzerain_serial];
			}
			
			Godot.Collections.Array vassal_serials = (Godot.Collections.Array)data[nameof(vassals)];
			foreach (var vassal_data in vassal_serials)
			{
				int vassal_serial = Int32.Parse((string)vassal_data);
				vassals.Add(my_universe.AllStates[vassal_serial]);
			}
			
			population = (Population)Serialization.JSONToObject( (string)data[nameof(population)]);
		}
	}
}
