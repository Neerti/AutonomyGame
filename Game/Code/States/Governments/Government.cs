﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autonomy.People.Characters;
using Autonomy.People.Populations;
using Autonomy.States;

namespace Autonomy.States.Governments
{
	/// <summary>
	/// This object is a component for a State object, and holds information 
	/// about the state's political system, like who the leaders are, 
	/// how many of them there are, policies in effect, etc. 
	/// Succession is handled inside a seperate module object.
	/// </summary>
	public class Government
	{
		State my_state;
		
		public Government(State new_state)
		{
			my_state = new_state;
			succession_module = new ElectiveSuccession(this);
		}
		
		SuccessionMethod succession_module;
		public SuccessionMethod SuccessionModule => succession_module;
		
		List<Character> leaders = new List<Character>();
		public List<Character> Leaders => leaders;
		
		/// <summary>
		/// Contains Characters who were previously leaders. 
		/// Used to enforce term limits.
		/// </summary>
		List<Character> terms_served = new List<Character>();
		public List<Character> TermsServed => terms_served;
		
		int max_leaders = 1;
		public int MaxLeaders => max_leaders;

		/// <summary>
		/// Current setting for how long one or more leaders will rule for.
		/// </summary>
		public TermLengthOptions TermLength { get; private set; } = TermLengthOptions.Base;

		/// <summary>
		/// Current setting for how many times someone can become a leader.
		/// </summary>
		public TermLimitOptions TermLimit { get; } = TermLimitOptions.Base;

		/// <summary>
		/// Current setting for how many leaders can exist at once in this government.
		/// </summary>
		public LeaderSlotAllocations LeaderSlotAllocation { get; } = LeaderSlotAllocations.Base;

		/// <summary>
		/// Holds the current electorate method, which determines who can vote.
		/// </summary>
		public ElectorateMethods ElectorateMethod { get; private set; } = ElectorateMethods.Base;

		public void SetTermLengthOption(TermLengthOptions option)
		{
			TermLength = option;
		}
		
		public void SetElectorateMethodOption(ElectorateMethods option)
		{
			ElectorateMethod = option;
		}
		
		/// <summary>
		/// Holds the date for when the current term will end. 
		/// If null, the term is for life.
		/// </summary>
		DateTime? end_of_term_date;
		
		/// <summary>
		/// Attempts to determine when the current leaders' terms end. 
		/// This can return null.
		/// </summary>
		/// <returns>The date for the next election, or null if it is 
		/// a life term.</returns>
		/// <param name="current_date">The current ingame date.</param>
		public DateTime? TryGetEndOfTerm(DateTime current_date)
		{
			switch (TermLength)
			{
				case TermLengthOptions.LifeTerm:
					return null;
				case TermLengthOptions.ShortTerm:
					return current_date.AddYears(5);
				case TermLengthOptions.LongTerm:
					return current_date.AddYears(20);
				default:
					throw new Exception("Invalid term length option.");
			}
		}
		
		/// <summary>
		/// Determines if it is time for a new election to occur.
		/// </summary>
		/// <returns><c>true</c>, if end of term was reached, <c>false</c> otherwise.</returns>
		/// <param name="end_date">DateTime when the term ends.</param>
		/// <param name="current_date">DateTime holding the current date.</param>
		public bool IsEndOfTerm(DateTime end_date, DateTime current_date)
		{
			if (end_date.CompareTo(current_date) == 1)
			{
				return false; // The end of term is in the future.
			}
			return true; // Otherwise it's now (or in the past).
		}
		
		void AssignEndOfTerm(DateTime current_date)
		{
			end_of_term_date = TryGetEndOfTerm(current_date);
		}
		
		/// <summary>
		/// Called every time a day passes in the game.
		/// </summary>
		/// <param name="current_date">The current ingame date.</param>
		void DailyProcess(DateTime current_date)
		{
			if(end_of_term_date != null && IsEndOfTerm(end_of_term_date.Value, current_date))
			{
				HandleSuccession();
			}
		}
		
		void HandleSuccession()
		{
			// TODO: Redirect to the succession object.
			throw new NotImplementedException();
		}

		/// <summary>
		/// Options for what term lengths a Government object can have.
		/// </summary>
		public enum TermLengthOptions
		{
			/// <summary>
			/// Unset default term length.
			/// </summary>
			Base,
			
			/// <summary>
			/// Term lasts for five years.
			/// </summary>
			ShortTerm, 
			
			/// <summary>
			/// Term lasts for twenty years.
			/// </summary>
			LongTerm,
			
			/// <summary>
			/// Term lasts until the leader(s) die, abdicate, or get deposed.
			/// </summary>
			LifeTerm
		}
		
		/// <summary>
		/// Options for term limits for Government objects.
		/// </summary>
		public enum TermLimitOptions
		{
			/// <summary>
			/// Unset default term limit.
			/// </summary>
			Base,
			
			/// <summary>
			/// No limits exist.
			/// </summary>
			NoLimits,
			
			/// <summary>
			/// Leaders can only be a leader once.
			/// </summary>
			OneTerm,
			
			/// <summary>
			/// Leaders can only be a leader twice, consecutively or not.
			/// </summary>
			TwoTerms
		}
		
		/// <summary>
		/// Determines if any arbitrary Character can potentially become a 
		/// leader under this political system.
		/// </summary>
		/// <returns><c>true</c>, if viable for becoming leader, <c>false</c> otherwise.</returns>
		/// <param name="candidate">The character to evaluate.</param>
		public bool CanBecomeLeader(Character candidate)
		{
			// Check if this will exceed term limits.
			if(!(TermLimit is TermLimitOptions.NoLimits))
			{
				switch (TermLimit)
				{
					case TermLimitOptions.OneTerm:
						if (terms_served.Contains(candidate))
						{
							return false;
						}
						break;
					case TermLimitOptions.TwoTerms:
						if (terms_served.Count((Character C) => C == candidate) >= 2)
						{
							return false;
						}
						break;
				}
			}
			// TODO: Other checks like Electorate things.
			
			return true;
		}
		
		/// <summary>
		/// Returns the number of total leader slots available based on 
		/// the allocation option that is active.
		/// </summary>
		/// <returns>The total leader slots.</returns>
		public int GetTotalLeaderSlots()
		{
			switch (LeaderSlotAllocation)
			{
				case LeaderSlotAllocations.Solo:
					return 1;
				case LeaderSlotAllocations.Triumvirate:
					return 3;
				case LeaderSlotAllocations.Council:
					return 5;
				case LeaderSlotAllocations.TwoPerVassal:
					return 1 + (my_state.Vassals.Count * 2);
				case LeaderSlotAllocations.TwoScaledByElectorate:
					throw new NotImplementedException();
				default:
					throw new Exception("Invalid leader slot allocation option.");
			}
		}
		
		/// <summary>
		/// Configurtions for how leader slot totals are determined. 
		/// It should be noted that there must be an odd number of slots, 
		/// to help avoid ties.
		/// </summary>
		public enum LeaderSlotAllocations
		{
			/// <summary>
			/// Unset default amount. 
			/// Exceptions will be thrown if used.
			/// </summary>
			Base,
			
			/// <summary>
			/// Only one leader, with the default title of President (if democratic). 
			/// Elections (if applicable) are ran using FPTP.
			/// </summary>
			Solo,
			
			/// <summary>
			/// Three leader slots, each with the title of Triumvir.
			/// </summary>
			Triumvirate,
			
			/// <summary>
			/// Five(?) leader slots.
			/// </summary>
			Council,
			
			/// <summary>
			/// One leader slot, plus two for each vassal under the State. 
			/// Each vassal slot is filled by mini-elections for each vassal.
			/// The single non-vassal slot is elected by the vassal slots.
			/// Requires Cascading Electorate to be active.
			/// </summary>
			TwoPerVassal,
			
			/// <summary>
			/// One leader slot, plus two for every TODO amount of the  
			/// total electorate in the State (and its vassals if Cascading 
			/// Electorate is active).
			/// Slots are distributed using a proportional system.
			/// </summary>
			TwoScaledByElectorate
		}
		
		/// <summary>
		/// Returns a Population object containing the people who are allowed to 
		/// vote in an election, if applicable.
		/// </summary>
		/// <returns>The electorate.</returns>
		public Population GetElectorate()
		{
			switch (ElectorateMethod)
			{
				case ElectorateMethods.NoVoting:
					return new Population(); // Should this return null instead?
				case ElectorateMethods.Universal:
					if(cascading_electorate)
					{
						return my_state.GetAllVassalPopulations();
					}
					return my_state.Population;
				default:
					throw new Exception("Invalid electorate method option.");
			}
			
		}
		
		/// <summary>
		/// Determines if the Electorate should be composed of only this 
		/// state's population, or its population plus its vassals' population.
		/// </summary>
		bool cascading_electorate;
		public bool CascadingElectorate => cascading_electorate;
		
		public void SetCascadingElectorate(bool new_setting)
		{
			cascading_electorate = new_setting;
		}
		
		/// <summary>
		/// Options for how the Electorate is determined.
		/// </summary>
		// TODO: Make this into a class?
		public enum ElectorateMethods
		{
			/// <summary>
			/// Default setting, will cause exceptions if used.
			/// </summary>
			Base,
			
			/// <summary>
			/// All of a state's population can vote, and everyone's vote is counted equally.
			/// </summary>
			Universal,
			
			/// <summary>
			/// No elections occur, so there is no electorate.
			/// </summary>
			NoVoting
		}
		
		public static Government MakeStandardizedGovernment(Universes.Universe universe)
		{
			State state = State.MakeStandardizedState(universe);
			Government gov = new Government(state);
			gov.SetElectorateMethodOption(ElectorateMethods.Universal);
			gov.succession_module = new ElectiveSuccession(gov);
			return gov;
		}
	}
}
