﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autonomy.People.Characters;
using Autonomy.People.Populations;
using Autonomy.People.Ideologies;
using Autonomy.People;

namespace Autonomy.States.Governments
{
	/// <summary>
	/// Class for running elections to determine the next leaders.
	/// </summary>
	public class ElectiveSuccession : SuccessionMethod
	{
		public ElectiveSuccession(Government new_government) : base(new_government) { }
		
		public override List<Character> GetSuccessors(List<Character> candidates)
		{
			Population electorate = my_government.GetElectorate();
			
			List<Character> successors = new List<Character>();
			for (int i = 0; i < my_government.MaxLeaders; i++)
			{
				Character winner = RunElection(candidates, electorate);
				successors.Add(winner);
				candidates.Remove(winner); // So someone can't win twice in one cycle.
			}
			return successors;
		}
		
		// TODO: Implement an ElectionOutcome object and store stats inside?
		Character RunElection(List<Character> candidates, Population electorate)
		{
			Character winner = null;
			
			var votes = new Dictionary<Character, ulong>();
			foreach (var candidate in candidates)
			{
				votes[candidate] = 0;
			}
			
			// Go through each demographic in the electorate, one at a time.
			foreach (var demographic in electorate.Demographics)
			{
				if (demographic.IdeologyID == IdeologyIDs.Apathy) // Apathetics will never vote.
				{
					continue;
				}
			//	Godot.GD.Print("\n");
			//	Godot.GD.Print("Now competing in " + demographic);
				
				// First, determine the potential turnout for all the demographics and candidates.
				// An unexciting candidate can result in low 'turnout power', 
				// which can cause them to lose to other candidates, even if their 
				// ideological supporters outnumber the opposition.
				var turnout_powers = new Dictionary<Character, float>();
				foreach (var candidate in candidates)
				{
					turnout_powers[candidate] = CalculateTurnoutPower(candidate, demographic, candidates);
			//		Godot.GD.Print(candidate + "'s Turnout Power was: " + turnout_powers[candidate]);
				}
				
				// Next, determine how to split potential votes among candidates 
				// who managed to get some people to want to vote for them.
				// This usually only matters if more than one candidate managed to 
				// appeal to the same demographic.
				var pie_division = DivideVotingPie(turnout_powers);
				
				// Finally, assign votes to each candidate, based on their 'turnout power'.
				foreach (var candidate in candidates)
				{
			//		Godot.GD.Print(candidate + " has appealed to " + (pie_division[candidate] * 100) + "% of the demographic " + demographic);
					ulong new_votes = CalculateVotes(demographic.Size, pie_division[candidate], turnout_powers[candidate]);
					votes[candidate] += new_votes;
			//		Godot.GD.Print(candidate + " received " + new_votes + " votes from " + demographic);
				}
			}
			
			// Count the votes.
			ulong votes_to_beat = 0;
			foreach (var candidate in candidates)
			{
			//	Godot.GD.Print(candidate + " got " + votes[candidate] + " votes.");
				if (votes[candidate] > votes_to_beat)
				{
					votes_to_beat = votes[candidate];
					winner = candidate;
				}
			}
		//	Godot.GD.Print(winner + " won the election!");
			return winner;
		}
		
		/// <summary>
		/// Determines how much a given demographic likes a specific candidate. 
		/// Higher numbers mean more favor, however the number needs to cross 
		/// a certain threshold for them to get any votes.
		/// </summary>
		/// <returns>The candidate score.</returns>
		/// <param name="candidate">The candidate to be judged.</param>
		/// <param name="demographic">The demographic to judge the candidate.</param>
		/// <param name="candidates">The list of other candidates in the election.</param>
		public int CalculateCandidateScore(Character candidate, Demographic demographic, List<Character> candidates)
		{
			int score = 0;
			
			// Ideology alignment plays a big role in candidate scoring.
			switch (candidate.Ideology.GetAlignment(Glob.AllIdeologies[demographic.IdeologyID]))
			{
				case Ideology.Alignment.Identical:
					score += 125;
					break;
				case Ideology.Alignment.Allied:
					score += 75;
					
					// If the demographic's preferred ideology is not represented, 
					// and there is a candidate who is directly opposed to that 
					// ideology, some will still vote for the allied ideology, 
					// to try to prevent a worse outcome.
					if (!CandidatesHaveAlignment(candidates, demographic.IdeologyID, Ideology.Alignment.Identical))
					{
						if (CandidatesHaveAlignment(candidates, demographic.IdeologyID, Ideology.Alignment.Opposed))
						{
							score += 35; // Sufficent to cross the default threshold.
						}
					}
					// TODO: Check if their preferred ideology is weakly 
					// represented compared to an allied one that is stronger, 
					// and there is another strong opposing ideology, in order 
					// to implement the spoiler effect.
					break;
				case Ideology.Alignment.Opposed:
					score += -1000;
					break;
			}
			
			// TODO: Issues.
			
			// TODO: Stat evaluations.
			
			// TODO: Trait perceptions.
			foreach (var trait in candidate.TraitManager.Traits)
			{
				var perception = trait.GetPerceptionModifier(candidate, demographic);
				if (perception != null)
				{
					score += perception.Value.Value;
				}
			}
			
			// Incumbency advantage.
			// If the candidate is an incumbent, and the people with the same 
			// ideology as the candidate like how they're doing, they will get 
			// a small boost.
			if (my_government.Leaders.Contains(candidate) && candidate.Ideology.IdeologyID == demographic.IdeologyID)
			{
				// TODO: Add check for if things are going badly (e.g. unrest) 
				// in the state, to turn this into a malus instead of a bonus.
				score += 20;
			}
			
			return score;
		}
		
		public float CalculateTurnoutPower(Character candidate, Demographic demographic, List<Character> candidates)
		{
			int score = CalculateCandidateScore(candidate, demographic, candidates);
			int threshold = Glob.AllIdeologies[demographic.IdeologyID].VotingThreshold;
			// Influences the amount of votes that can be received.
			// Note that there are diminishing returns, as 1.0f results 
			// in half, and 2.0f results in 75%, etc.
			float result;
			
			if (score < threshold)
			{
				result = 0.0f;
			}
			else
			{
				result = (float)(score - threshold) / 100;
			}
			return result;
		}
		
		/// <summary>
		/// Determines how many votes someone will get based on the percentage 
		/// of the 'pie' they captured, and their turnout power.
		/// </summary>
		/// <returns>How many votes someone should get from the values 
		/// inputted.</returns>
		/// <param name="size">Maximum potential votes.</param>
		/// <param name="percentage_of_pie">Percentage of the max potential 
		/// votes that is 'up for grabs'.</param>
		/// <param name="turnout_power">The 'excitability' score of a candidate 
		/// that is used to determine how much of the potential slice of the 
		/// voter pie will actually vote.</param>
		public ulong CalculateVotes(ulong size, float percentage_of_pie, float turnout_power)
		{
			if (turnout_power <= 0)
			{
				// To avoid dividing by zero shennanigans.
				return 0;
			}
			
			// Maximum potential votes from this demographic.
			ulong votes = (ulong)(size * percentage_of_pie);
			
			// Reduction from people not going out to vote, even if they 
			// would have voted for the candidate.
			float turnout_percentage = 1 - (1 / (turnout_power + (1 * Math.Max(turnout_power, 1))));
			votes = (ulong)(votes * turnout_percentage);
			
			// TODO: Implement methods for voter suppression, electoral fraud, etc.
			
			return votes;
		}
		
		/// <summary>
		/// Calculates how large of a portion of potential voters each candidate 
		/// can have, based on their turnout modifier and their rivals' 
		/// modifier.
		/// </summary>
		/// <returns>A new dictionary with the candidate as the key, and 
		/// a float holding how much of the 'pie' they have as 
		/// the value.</returns>
		/// <param name="turnout_dict">Dictionary with the key being each 
		/// candidate, and the value being their 'turnout modifier'.</param>
		public Dictionary<Character, float> DivideVotingPie(Dictionary<Character, float> turnout_dict)
		{
			// Each candidate gets a certain amount of a Demographic, based 
			// on their modifier. E.g. if one candidate has a modifier of 1.5f, 
			// and the other has 0.5f, the first candidate will get 75%, and 
			// the second will get 25%.
			var divided_demographic = new Dictionary<Character, float>();
			
			// Get the total amount.
			float total = 0.0f;
			foreach (var pair in turnout_dict)
			{
				divided_demographic[pair.Key] = 0;
				total += pair.Value;
			}
			
			if (total > 0)
			{
				foreach (var pair in turnout_dict)
				{
					divided_demographic[pair.Key] = pair.Value / total;
				}
			}
			
			return divided_demographic;
		}
		
		/// <summary>
		/// Determines if any of the candidates has a matching alignment for a specific ideology ID. 
		/// Useful to determine if strategic voting should be considered.
		/// </summary>
		/// <returns><c>true</c>, if the desired alignment is present, <c>false</c> otherwise.</returns>
		/// <param name="candidates">List of candidates in the election.</param>
		/// <param name="id">Ideology ID to check alignment for.</param>
		/// <param name="alignment">Alignment result to look for.</param>
		bool CandidatesHaveAlignment(List<Character> candidates, IdeologyIDs id, Ideology.Alignment alignment)
		{
			foreach (var candidate in candidates)
			{
				if (candidate.Ideology.GetAlignment(Glob.AllIdeologies[id]) == alignment)
				{
					return true;
				}
			}
			return false;
		}
		
	}
}
