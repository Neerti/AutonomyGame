﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters;

namespace Autonomy.States.Governments
{
	/// <summary>
	/// Base class for various methods for a State's Government object to 
	/// determine who the next leader(s) should be. 
	/// </summary>
	public abstract class SuccessionMethod
	{
		protected readonly Government my_government;
		
		protected SuccessionMethod(Government new_government)
		{
			my_government = new_government;
		}
		
		/// <summary>
		/// Obtains a list of successors according to the logic built into 
		/// this class.
		/// </summary>
		/// <returns>The successors.</returns>
		public abstract List<Character> GetSuccessors(List<Character> candidates);
		
		/// <summary>
		/// Determines if the current succession method is valid. If it is not, 
		/// a new one will need to be swapped in.
		/// </summary>
		/// <returns><c>true</c>, if valid, <c>false</c> otherwise.</returns>
		public virtual bool IsValid()
		{
			return true;
		}
	}
}
