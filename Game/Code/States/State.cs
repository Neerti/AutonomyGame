﻿using System.Collections.Generic;

using Autonomy.People;
using Autonomy.People.Populations;

namespace Autonomy.States
{
	/// <summary>
	/// A State is the in-game abstraction of a nation, polity, or other group 
	/// of people under some form of government. 
	/// 
	/// The functions of the government, such as its leaders and how succession 
	/// will work, is contained in a seperate Government object.
	/// </summary>
	public partial class State
	{
		/// <summary>
		/// Holds a serial number for this state, which acts as a 
		/// unique identifier for it, particularly for serialization.
		/// </summary>
		int state_serial = -1;
		public int StateSerial => state_serial;
		
		/// <summary>
		/// The name of the state, displayed in the UI.
		/// </summary>
		string display_name;
		public string DisplayName => display_name;
		
		/// <summary>
		/// The word which identifies residents or natives of a 
		/// particular state, e.g. 'Martian'.
		/// </summary>
		string demonym;
		public string Demonym => demonym;
		
		/// <summary>
		/// A state that has authority over this one. 
		/// If null, the state is considered independent.
		/// </summary>
		State suzerain;
		public State Suzerain => suzerain;
		
		/// <summary>
		/// States which this instance has authority over, and where 
		/// this state is their suzerain.
		/// </summary>
		List<State> vassals = new List<State>();
		public List<State> Vassals => vassals;
		
		/// <summary>
		/// An instance of a Population object which represents the people who 
		/// exist inside this state.
		/// </summary>
		Population population;
		public Population Population => population;
		
		Universes.Universe my_universe;
		
		public State() { }
		
		public State(Population new_population)
		{
			population = new_population;
		//	Register(LoadedUniverse.GetUniverse());
		}
		
		public State(string new_name, Population new_population) : this(new_population)
		{
			display_name = new_name;
		}
		
		public State(string new_name, Population new_population, Universes.Universe universe) : this(new_name, new_population)
		{
			my_universe = universe;
			Register(universe);
		}
		
		public State(Universes.Universe custom_universe)
		{
			my_universe = custom_universe;
			Register(custom_universe);
		}
		
		/// <summary>
		/// Register this instance to a Universe's list of all states. 
		/// This also assigns the <see cref="state_serial"/> number.
		/// </summary>
		void Register(Universes.Universe universe)
		{
			universe.AddState(this);
		}
		
		public override string ToString()
		{
			return display_name;
		}
		
		/// <summary>
		/// Changes the name of this state.
		/// </summary>
		/// <param name="new_name">The new name for the state.</param>
		public void Rename(string new_name)
		{
			display_name = new_name;
		}
		
		/// <summary>
		/// Changes the demonym for this state.
		/// </summary>
		/// <param name="new_demonym">The new demonym for this state.</param>
		public void ChangeDemonym(string new_demonym)
		{
			demonym = new_demonym;
		}
		
		/// <summary>
		/// Assigns a serial number to this object. 
		/// This should only be done by the Universe object.
		/// </summary>
		/// <param name="new_serial">New serial number to assign.</param>
		public void SetSerial(int new_serial)
		{
			state_serial = new_serial;
		}
		
		/// <summary>
		/// Makes a State object become a vassal to this one, setting 
		/// the correct variables on both sides.
		/// </summary>
		/// <param name="new_vassal">The new vassal.</param>
		public void AddVassal(State new_vassal)
		{
			if(vassals.Contains(new_vassal))
			{
				return;
			}
			vassals.Add(new_vassal);
			new_vassal.suzerain = this;
		}
		
		/// <summary>
		/// Makes a State object stop being a vassal to this one, 
		/// setting the correct variables on both sides.
		/// </summary>
		/// <param name="former_vassal">Former vassal.</param>
		public void RemoveVassal(State former_vassal)
		{
			if(vassals.Contains(former_vassal))
			{
				vassals.Remove(former_vassal);
				former_vassal.suzerain = null;
			}
		}
		
		/// <summary>
		/// Recursively combines vassal populations, and returns the result. 
		/// This also includes this state's population.
		/// </summary>
		/// <returns>The population for all vassals.</returns>
		public Population GetAllVassalPopulations()
		{
			return RecursiveVassalPopulations(new Population());
		}
		
		/// <summary>
		/// Recursively iterates over vassals, vassals of vassals, etc, to 
		/// get a combined Population object.
		/// </summary>
		/// <returns>The vassal populations.</returns>
		/// <param name="pop">Pop.</param>
		Population RecursiveVassalPopulations(Population pop)
		{
			pop.Merge(population);
			foreach (State vassal in vassals)
			{
				return vassal.RecursiveVassalPopulations(pop);
			}
			return pop;
		}
		
		public static State MakeStandardizedState(Universes.Universe universe)
		{
			Population pop = new Population();
			pop.AddDemographic(new Demographic(150000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human));
			pop.AddDemographic(new Demographic(50000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human));
			State state = new State("Test Land", pop, universe);

			Population vassal_pop = new Population();
			vassal_pop.AddDemographic(new Demographic(2500, IdeologyIDs.SubSandwich, SpeciesIDs.Human));
			vassal_pop.AddDemographic(new Demographic(750, IdeologyIDs.TomatoSoup, SpeciesIDs.Human));
			State vassal = new State("Lesser Test Region", vassal_pop, universe);


			/*
			List<Demographic> demographics = new List<Demographic>{
				new Demographic(6000, IdeologyIDs.TechnoProgressivism, SpeciesIDs.Human),
				new Demographic(4000, IdeologyIDs.Undecided, SpeciesIDs.Human),
				new Demographic(2000, IdeologyIDs.Apathy, SpeciesIDs.Human),
				new Demographic(8000, IdeologyIDs.TODO1, SpeciesIDs.Human)
			};
			*/

			state.AddVassal(vassal);
			return state;
		}
	}
}
