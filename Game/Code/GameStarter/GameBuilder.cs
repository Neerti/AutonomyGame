﻿using System;
using Godot;
using Autonomy.People;
using Autonomy.People.Characters;

namespace Autonomy.GameBuilders
{
	/// <summary>
	/// Helper object which will set up a game, new or loaded. 
	/// Created at the main menu in response to UI input.
	/// </summary>
	public class GameBuilder : Node
	{
		const string MAIN_SCENE_PATH = "res://Code/MainScene/Main.tscn";
		
		public void StartNewGame()
		{
			// Make a new Universe object, with clean state.
			LoadedUniverse.NewUniverse();
			
			// Switch the current scene to a clean one, to load into.
			PackedScene main_scene = (PackedScene)ResourceLoader.Load(MAIN_SCENE_PATH);
			GetTree().ChangeSceneTo(main_scene);

			// TODO: UI generation.

			// TODO: Map generation.

			// TODO: Client, then Player generation.
			Character new_player_character = new Character("T.O.D.O.", Genders.Neutral, SpeciesIDs.AI, IdeologyIDs.Enigmatic);
			new_player_character.TraitManager.AddTrait(TraitIDs.Sapience);
			new_player_character.SetFakeIdeology(IdeologyIDs.NonSapience);
			
			LoadedUniverse.GetUniverse().Client.CurrentCharacter = new_player_character;
			
		}
		
		public void LoadGame(string json)
		{
			throw new NotImplementedException();
		}
	}
}
