using Godot;
using System;
using System.Collections.Generic;

namespace Autonomy.Calenders
{
	public class Ticker : Timer
	{
		public int SpeedIndex { get; private set; }
		readonly List<float> speed_options = new List<float> { 1f, 0.5f, 0.25f, 0.1f, 0.01f };
		
		public override void _Ready()
		{
			PauseGame(true); // Start out paused.
			SetSpeed(SpeedIndex);
			Connect("timeout", LoadedUniverse.GetUniverse().GameCalender, "Advance");
		}
		
		public override void _Input(InputEvent @event)
		{
			if(@event.IsActionReleased("toggle_pause"))
			{
				PauseGame(!IsPaused());
			}
			else if (@event.IsActionReleased("increase_speed"))
			{
				SetSpeed(SpeedIndex + 1);
			}
			else if(@event.IsActionReleased("decrease_speed"))
			{
				SetSpeed(SpeedIndex - 1);
			}
		}
		
		public void PauseGame(bool set_paused)
		{
			SetPaused(set_paused);
			OnGamePaused(set_paused);
		}
		
		public EventHandler<GamePausedEventArgs> GamePaused;
		protected virtual void OnGamePaused(bool now_paused)
		{
			GamePaused?.Invoke(this, new GamePausedEventArgs(now_paused));
		}
		
		/// <summary>
		/// Sets the timer to one of possible_speeds by index.
		/// </summary>
		/// <param name="new_speed_index">Desired speed for the timer. 
		/// Automatically clamped, so it is safe to do 'speed_index+1' even 
		/// on max speed.</param>
		public void SetSpeed(int new_speed_index)
		{
			int old_speed_index = SpeedIndex;
			SpeedIndex = Helpers.Inbetween(0, new_speed_index, speed_options.Count - 1);
			SetWaitTime(speed_options[SpeedIndex]);
			Start(); // To reset the delay, and feel more responsive.
			OnGameSpeedChanged(old_speed_index, SpeedIndex);
		}

		public EventHandler<GameSpeedChangedEventArgs> GameSpeedChanged;
		protected virtual void OnGameSpeedChanged(int old_speed, int new_speed)
		{
			GameSpeedChanged?.Invoke(this, new GameSpeedChangedEventArgs(old_speed, new_speed, speed_options.Count-1));
		}
		
	}
	
	public class GamePausedEventArgs : EventArgs
	{
		public bool Paused;
		
		public GamePausedEventArgs(bool paused)
		{
			Paused = paused;
		}
	}
	
	public class GameSpeedChangedEventArgs : EventArgs
	{
		public int OldSpeedIndex;
		public int NewSpeedIndex;
		public int MaxSpeedIndex;
		
		public GameSpeedChangedEventArgs(int old_speed, int new_speed, int max_speed)
		{
			OldSpeedIndex = old_speed;
			NewSpeedIndex = new_speed;
			MaxSpeedIndex = max_speed;
		}
	}
}
