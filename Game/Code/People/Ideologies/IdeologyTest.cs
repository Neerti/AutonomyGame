﻿using System;
using System.Collections.Generic;

namespace Autonomy.People.Ideologies.Test
{
	// Ideology Groups for testing.
	public class TestIdeologyGroupSoup : IdeologyGroup
	{
		public TestIdeologyGroupSoup()
		{
			ideology_group_id = IdeologyGroupIDs.DebugSoupism;
			display_name = "Soupism";
		}
	}
	
	public class TestIdeologyGroupSandwich : IdeologyGroup
	{
		public TestIdeologyGroupSandwich()
		{
			ideology_group_id = IdeologyGroupIDs.DebugSandwichism;
			display_name = "Sandwichism";
		}
	}
	
	// Ideologies for testing.
	
	// Soup-aligned.
	public class TestIdeologyTomato : Ideology
	{
		public TestIdeologyTomato()
		{
			IdeologyID = IdeologyIDs.TomatoSoup;
			IdeologyGroup = new TestIdeologyGroupSoup();
			opposing_ideology_groups = new List<IdeologyGroupIDs> { IdeologyGroupIDs.DebugSandwichism };
		}
	}
	
	// Sandwich-aligned.
	public class TestIdeologySub : Ideology
	{
		public TestIdeologySub()
		{
			IdeologyID = IdeologyIDs.SubSandwich;
			IdeologyGroup = new TestIdeologyGroupSandwich();
			opposing_ideology_groups = new List<IdeologyGroupIDs> { IdeologyGroupIDs.DebugSoupism };
		}
	}
	
	public class TestIdeologyBLT : Ideology
	{
		public TestIdeologyBLT()
		{
			IdeologyID = IdeologyIDs.BLTSandwich;
			IdeologyGroup = new TestIdeologyGroupSandwich();
			opposing_ideology_groups = new List<IdeologyGroupIDs> { IdeologyGroupIDs.DebugSoupism };
		}
	}
}
