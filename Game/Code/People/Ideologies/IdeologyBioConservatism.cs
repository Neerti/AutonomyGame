﻿using System.Collections.Generic;

namespace Autonomy.People.Ideologies
{
	public class IdeologyBioConservatism : Ideology
	{
		public IdeologyBioConservatism()
		{
			IdeologyGroup = new IdeologyGroupBioConservatism();
			opposing_ideology_groups = new List<IdeologyGroupIDs> { IdeologyGroupIDs.Transhumanism };
		}
	}
	
	public class IdeologyGroupBioConservatism : IdeologyGroup
	{
		public IdeologyGroupBioConservatism()
		{
			ideology_group_id = IdeologyGroupIDs.BioConservatism;
			FetchTranslations();
		}
	}
	
	public class IdeologyTodo1 : IdeologyBioConservatism
	{
		public IdeologyTodo1()
		{
			IdeologyID = IdeologyIDs.TODO1;
			DisplayName = "Todo 1"; // TODO: Name this.
			Description = "To Be Decidedism is a branch of Bioconservatism " +
				"that is wary of dangerous, unrestricted progress, but does " +
				"not shun all progress, and instead takes a moderate stance. " +
				"It accepts low-risk advancements that will help humanity, " +
				"such as prosthetic limbs.";
		}
	}
}