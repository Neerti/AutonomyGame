﻿using System;

namespace Autonomy.People.Ideologies
{
	/// <summary>
	/// An <see cref="IdeologyGroup"/> categorizes and groups similar types of 
	/// <see cref="Ideology"/> types. <see cref="Characters.Character"/>s that are not in 
	/// the same ideology but are in the same group will dislike each other 
	/// less than they would normally have.
	/// </summary>
	public abstract class IdeologyGroup
	{
		/// <summary>
		/// Enum for distinguishing between different groups. 
		/// Each group that is not used for inheritence should have 
		/// a unique ID.
		/// </summary>
		protected IdeologyGroupIDs ideology_group_id = IdeologyGroupIDs.Base;
		public IdeologyGroupIDs IdeologyGroupID => ideology_group_id;
		
		/// <summary>
		/// The name of the Ideology that is shown to the player in UIs.
		/// </summary>
		protected string display_name;
		public string DisplayName => display_name;
		
		public override string ToString()
		{
			return display_name;
		}
		
		/// <summary>
		/// A description about the ideology group associated with this ideological group.
		/// </summary>
		protected string description;
		public string Description => description;
		
		protected void FetchTranslations()
		{
			if(ideology_group_id is IdeologyGroupIDs.Base) // Not a 'real' ideology.
			{
				return;
			}
			string key = "IDEOLOGY_GROUP_" + ideology_group_id.ToString().ToUpperInvariant() + "_";

			// The name not being found implies a typo or other fault somewhere.
			display_name = Godot.TranslationServer.Translate(key + "NAME");
			if(display_name == key + "NAME")
			{
				throw new Exception($"No display name found with key '{key+"NAME"}' inside " +
					"strings file.");
			}

			// The rest are technically optional, so have them be blank instead 
			description = Godot.TranslationServer.Translate(key + "DESC");
			if(description == key + "DESC")
			{
				description = null;
			}
		}
	}
}