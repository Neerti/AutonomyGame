﻿namespace Autonomy.People.Ideologies
{
	public class IdeologyUnaligned : Ideology
	{
		public IdeologyUnaligned()
		{
			IdeologyGroup = new IdeologyGroupUnaligned();
		}
	}

	public class IdeologyGroupUnaligned : IdeologyGroup
	{
		public IdeologyGroupUnaligned()
		{
			ideology_group_id = IdeologyGroupIDs.Unaligned;
			FetchTranslations();
		}
	}
	
	public class IdeologyNonSapience : IdeologyUnaligned
	{
		public IdeologyNonSapience()
		{
			IdeologyID = IdeologyIDs.NonSapience;
			FetchTranslations();
			Apolitical = true;
		}
	}
	
	public class IdeologyApathy : IdeologyUnaligned
	{
		public IdeologyApathy()
		{
			IdeologyID = IdeologyIDs.Apathy;
			FetchTranslations();
		}
	}
	
	public class IdeologyUndecided : IdeologyUnaligned
	{
		public IdeologyUndecided()
		{
			IdeologyID = IdeologyIDs.Undecided;
			FetchTranslations();
			VotingThreshold = 50; // Undecided people care less about ideological purity.
		}
	}
	
	public class IdeologyEnigmatic : IdeologyUnaligned
	{
		public IdeologyEnigmatic()
		{
			IdeologyID = IdeologyIDs.Enigmatic;
			FetchTranslations();
		}
	}
}