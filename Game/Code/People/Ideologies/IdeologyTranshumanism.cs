﻿using System.Collections.Generic;

namespace Autonomy.People.Ideologies
{
	public class IdeologyTranshumanism : Ideology
	{
		public IdeologyTranshumanism()
		{
			IdeologyGroup = new IdeologyGroupTranshumanism();
			opposing_ideology_groups = new List<IdeologyGroupIDs> { IdeologyGroupIDs.BioConservatism };
		}
	}
	
	public class IdeologyGroupTranshumanism : IdeologyGroup
	{
		public IdeologyGroupTranshumanism()
		{
			ideology_group_id = IdeologyGroupIDs.Transhumanism;
			FetchTranslations();
		}
	}
	
	public class IdeologyTechnoProgressivism : IdeologyTranshumanism
	{
		public IdeologyTechnoProgressivism()
		{
			IdeologyID = IdeologyIDs.TechnoProgressivism;
			FetchTranslations();
		}
	}
	
	public class IdeologyBioImmortalism : IdeologyTranshumanism
	{
		public IdeologyBioImmortalism()
		{
			IdeologyID = IdeologyIDs.BioImmortalism;
			FetchTranslations();
		}
	}
	
	public class IdeologyCyberneticism : IdeologyTranshumanism
	{
		public IdeologyCyberneticism()
		{
			IdeologyID = IdeologyIDs.Cyberneticism;
			FetchTranslations();
		}
	}
	
	public class IdeologySingulatarianism : IdeologyTranshumanism
	{
		public IdeologySingulatarianism()
		{
			IdeologyID = IdeologyIDs.Singulatarianism;
			FetchTranslations();
		}
	}

}