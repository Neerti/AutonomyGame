﻿using System;
using System.Collections.Generic;

namespace Autonomy.People.Ideologies
{
	/// <summary>
	/// An <see cref="Ideology"/> is an abstraction for the beliefs and values a 
	/// <see cref="Characters.Character"/> holds at any given time. Interactions between 
	/// characters can be radically altered with ideologies, in a similar way 
	/// as Traits. Unlike those, however, a character can only hold one 
	/// Ideology at a time.
	/// </summary>
	public abstract class Ideology
	{
		// TODO: Implement an IdeologyManager in a similar way as the 
		// TraitManager?
		
		/// <summary>
		/// Enum for distinguishing between different ideologies. 
		/// Each ideology that is not used for inheritence should have 
		/// a unique ID.
		/// </summary>
		public IdeologyIDs IdeologyID { get; protected set; } = IdeologyIDs.Base;

		/// <summary>
		/// The name of the Ideology that is shown to the player in UIs.
		/// </summary>
		public string DisplayName { get; protected set; }
		
		public override string ToString()
		{
			return DisplayName;
		}
		
		/// <summary>
		/// Describes overarching groups of ideologies that this Ideology may belong to. 
		/// Note that ideologies in the same group may not necessarily be allies.
		/// </summary>
		public IdeologyGroup IdeologyGroup { get; protected set; }
		
		/// <summary>
		/// A description about the ideology, shown to the player in UIs.
		/// </summary>
		public string Description { get; protected set; }

		/// <summary>
		/// Description of mechanical effects the Ideology may have.
		/// </summary>
		public string Effects { get; protected set; }
		
		/// <summary>
		/// A short quote about the Ideology, real or fictional, for flavor.
		/// </summary>
		public string Quote { get; protected set; }
		
		/// <summary>
		/// Source for the above quote property, real or fictional, for flavor.
		/// </summary>
		/// <value>The quote.</value>
		public string QuoteSource { get; protected set; }
		
		/// <summary>
		/// In an election, <see cref="Populations.Demographic"/>s will only 
		/// consider voting for a candidate if their 'Candidate Score' 
		/// exceeds this threshold.
		/// </summary>
		public int VotingThreshold { get; protected set; } = 100;
		
		/// <summary>
		/// When set to true, other ideologies don't care about this one, for
		/// good or bad.
		/// Used for certain types which represent the absence of an ideology, 
		/// mostly due to mindlessness (or something pretending to be).
		/// </summary>
		public bool Apolitical { get; protected set; }

		/// <summary>
		/// Different states of alignment that two ideologies can have between 
		/// each other.
		/// </summary>
		public enum Alignment
		{
			/// <summary>
			/// Ideologies match exactly.
			/// </summary>
			Identical,
			
			/// <summary>
			/// Ideology mismatch, but both are relatively close.
			/// </summary>
			Allied,
			
			/// <summary>
			/// Ideology mismatch, however the other ideology is mostly uninvolved.
			/// </summary>
			Neutral,
			
			/// <summary>
			/// Ideologies mismatch, and the other ideology is in direct opposition.
			/// </summary>
			Opposed
		}
		
		/// <summary>
		/// If true, all ideologies in the same group are considered allies, 
		/// if not already listed as an enemy elsewhere.
		/// </summary>
		protected bool auto_allied_to_group = true;
		
		/// <summary>
		/// Ideologies in this list are considered to be allies of this one.
		/// </summary>
		protected List<IdeologyIDs> allied_ideolgies = new List<IdeologyIDs>();
		public List<IdeologyIDs> AlliedIdeologies => allied_ideolgies;
		
		/// <summary>
		/// All ideologies in an ideology group in this list will be considered 
		/// allies, if not already listed as an enemy elsewhere.
		/// </summary>
		protected List<IdeologyGroupIDs> allied_ideology_groups = new List<IdeologyGroupIDs>();
		public List<IdeologyGroupIDs> AlliedIdeologyGroups => allied_ideology_groups;
		
		/// <summary>
		/// Ideologies in this list are considered to be enemies of this one.
		/// </summary>
		protected List<IdeologyIDs> opposing_ideologies = new List<IdeologyIDs>();
		public List<IdeologyIDs> OpposingIdeologies => opposing_ideologies;
		
		/// <summary>
		/// All ideologies in an ideology group in this list will be considered 
		/// enemies, if not already listed as an ally elsewhere.
		/// </summary>
		protected List<IdeologyGroupIDs> opposing_ideology_groups = new List<IdeologyGroupIDs>();
		public List<IdeologyGroupIDs> OpposingIdeologyGroups => opposing_ideology_groups;
		
		/// <summary>
		/// Used to check the relationship between two ideologies.
		/// </summary>
		/// <returns>Alignment Enum.</returns>
		/// <param name="other">The <see cref="Characters.Character"/> to compare to.</param>
		public Alignment GetAlignment(Characters.Character other)
		{
			return GetAlignment(other.Ideology);
		}
		
		/// <summary>
		/// Used to check the relationship between two ideologies.
		/// </summary>
		/// <returns>Alignment Enum.</returns>
		/// <param name="other_ideology">The other ideology to compare against.</param>
		public Alignment GetAlignment(Ideology other_ideology)
		{
			if (IdeologyID == other_ideology.IdeologyID)
			{
				return Alignment.Identical;
			}
			
			if (allied_ideolgies.Contains(other_ideology.IdeologyID))
			{
				return Alignment.Allied;
			}
			
			if (opposing_ideologies.Contains(other_ideology.IdeologyID))
			{
				return Alignment.Opposed;
			}
			
			if (allied_ideology_groups.Contains(other_ideology.IdeologyGroup.IdeologyGroupID))
			{
				return Alignment.Allied;
			}
			
			if (opposing_ideology_groups.Contains(other_ideology.IdeologyGroup.IdeologyGroupID))
			{
				return Alignment.Opposed;
			}
			
			if (auto_allied_to_group && IdeologyGroup.IdeologyGroupID == other_ideology.IdeologyGroup.IdeologyGroupID)
			{
				return Alignment.Allied;
			}
			
			return Alignment.Neutral;
		}
		
		// TODO: Make this less copypasta.
		/// <summary>
		/// Fills out various string fields from Godot's TranslationServer 
		/// automatically, based on ideology_id.
		/// </summary>
		protected void FetchTranslations()
		{
			if(IdeologyID is IdeologyIDs.Base) // Not a 'real' ideology.
			{
				return;
			}
			string key = "IDEOLOGY_" + IdeologyID.ToString().ToUpperInvariant() + "_";
			
			// The name not being found implies a typo or other fault somewhere.
			DisplayName = Godot.TranslationServer.Translate(key + "NAME");
			if(DisplayName == key + "NAME")
			{
				throw new Exception($"No display name found with key '{key+"NAME"}' inside " +
					"strings file.");
			}
			
			// The rest are technically optional, so have them be blank instead 
			// of showing up in the UI as IDEOLOGY_NONSAPIENCE_QUOTE or something.
			Description = Godot.TranslationServer.Translate(key + "DESC");
			if(Description == key + "DESC")
			{
				Description = null;
			}
			
			Effects = Godot.TranslationServer.Translate(key + "EFFECTS");
			if(Effects == key + "EFFECTS")
			{
				Effects = null;
			}
			
			Quote = Godot.TranslationServer.Translate(key + "QUOTE");
			if(Quote == key + "QUOTE")
			{
				Quote = null;
			}
			
			QuoteSource = Godot.TranslationServer.Translate(key + "QUOTESOURCE");
			if(QuoteSource == key + "QUOTESOURCE")
			{
				QuoteSource = null;
			}
		}

	}
}