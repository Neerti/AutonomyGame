﻿using System;

namespace Autonomy.People.Characters.Opinions
{
	/// <summary>
	/// Used to package an temporary adjustment to a <see cref="Character"/>'s 
	/// opinion of another.
	/// </summary>
	public struct TemporaryOpinionModifier
	{
		public int StartingValue { get; }
		
		public string Reason { get; }
		
		public DateTime Timestamp { get; }
		
		public TimeSpan StandardDuration { get; }
		
		public TemporaryOpinionModifier(int new_value, string new_reason, DateTime new_timestamp, TimeSpan new_duration)
		{
			StartingValue = new_value;
			Reason = new_reason;
			Timestamp = new_timestamp;
			StandardDuration = new_duration;
		}
		
		// TODO: Should this go on the character class instead? Probably should.
		public int CalculateOpinionShift(DateTime current_date, int true_value, TimeSpan true_duration)
		{
			TimeSpan difference = current_date - Timestamp;
			float decay = Math.Abs(difference.Days / true_duration.Days) - 1;
			int answer = (int)(true_value * decay);
			return answer;
		}
	}
}
