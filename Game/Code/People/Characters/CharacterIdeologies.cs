﻿using System;
using Godot;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters
{
	public partial class Character : Node
	{
		/// <summary>
		/// An <see cref="Ideologies.Ideology"/> object is an abstraction for the beliefs and values a 
		/// <see cref="Character"/> holds at any given time.
		/// </summary>
		public Ideology Ideology { get; private set; }
		
		public void SetIdeology(IdeologyIDs new_ideology)
		{
			Ideology = Glob.AllIdeologies[new_ideology];
		}

		/// <summary>
		/// A fake <see cref="Ideologies.Ideology"/> object that most other 
		/// characters will think is the holder's actual ideology.
		/// </summary>
		public Ideology FakeIdeology { get; private set; }
		
		public void SetFakeIdeology(IdeologyIDs new_fake_idoelogy)
		{
			FakeIdeology = Glob.AllIdeologies[new_fake_idoelogy];
		}
		
		/// <summary>
		/// Returns the <see cref="Ideologies.Ideology"/> object that this 
		/// character appears to have.
		/// </summary>
		/// <returns>The ideology that the character says is theirs.</returns>
		public Ideology GetApparentIdeology()
		{
			if (FakeIdeology is null)
			{
				return Ideology;
			}
			return FakeIdeology;
		}
		
		public Enum IsAligned(Character other)
		{
			return GetApparentIdeology().GetAlignment(other);
		}
	}
	

}