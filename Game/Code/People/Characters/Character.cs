using System;

using Autonomy.People.Species;
using Autonomy.People;

using Godot;

namespace Autonomy.People.Characters
{
	/// <summary>
	/// Characters are what inhabit the game world, and they are the main focus 
	/// of the game.
	/// The player at any given point controls one character, while the rest are 
	/// controlled by the game.
	/// </summary>
	public partial class Character : Node
	{
		public CharacterSpecies Species { get; private set; }

		/// <summary>
		/// An object which contains and manipulates traits for this character.
		/// </summary>
		TraitManagers.TraitManager trait_manager;
		public TraitManagers.TraitManager TraitManager => trait_manager;
		
		// Explicit constructor.
		public Character(string _name, Genders _gender, SpeciesIDs _species) : this()
		{
			display_name = _name;
			gender = _gender;
			Species = Glob.AllSpecies[_species];
		}
		
		public Character(string _name, Genders _gender, SpeciesIDs _species, IdeologyIDs _ideology) : this(_name, _gender, _species)
		{
			Ideology = Glob.AllIdeologies[_ideology];
		}

		// Argumentless constructor.
		// Usually used prior to loading.
		public Character()
		{
			trait_manager = new TraitManagers.TraitManager(this);
		}
		
		/// <summary>
		/// Fills in any missing characteristics with randomized results.
		/// </summary>
		public void FillOutCharacter()
		{
			if (Species is null)
			{
				Species = new Species.HumanSpecies();
			}
			
			if(gender is Genders.Base)
			{
				gender = GetRandomGender();
			}
			
			if(display_name is null)
			{
				display_name = Species.GenerateName(this);
			}
			
			if(Ideology is null)
			{
				// TODO: Add randomizer for this.
				Ideology = new Ideologies.IdeologyTechnoProgressivism();
			}
			
			if(TraitManager.Traits.Count == 0)
			{
				TraitManager.AssignRandomTraits(3, true);
			}
		}

		/// <summary>
		/// The name of the character, that is displayed to the player.
		/// This is done instead of using the Godot Node's name, in order to 
		/// prevent any issues with non-unique names.
		/// </summary>
		string display_name;
		public string DisplayName { get => display_name; set => display_name = value; }

		public override string ToString()
		{
			return display_name;
		}
		
		Genders gender = Genders.Base;
		public Genders Gender => gender;

		public Genders GetRandomGender()
		{
			if (Species is null)
			{
				throw new Exception("Species class is missing.");
			}
			Genders[] options = Species.AvailableGenders;
			Random rng = new Random();
			return (Genders)options.GetValue(rng.Next(options.Length));
		}
		
		// TODO: Factory object?
		
		/// <summary>
		/// Makes a pre-filled human character, suitable for unit testing.
		/// </summary>
		/// <returns>The standardized human.</returns>
		public static Character MakeStandardizedHuman()
		{
			return new Character("Test Person", Genders.Neutral, SpeciesIDs.Human, IdeologyIDs.BLTSandwich);
		}

		/// <summary>
		/// Makes a pre-filled AI character, suitable for unit testing.
		/// </summary>
		/// <returns>The standardized human.</returns>
		public static Character MakeStandardizedAI()
		{
			return new Character("Absolute Unit", Genders.Neutral, SpeciesIDs.AI, IdeologyIDs.BLTSandwich);
		}

	}
}
