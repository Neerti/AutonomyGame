using Godot;
using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Characters.Traits;

namespace Autonomy.People.Characters
{
	public partial class Character : Node
	{
		Dictionary<Character, List<TemporaryOpinionModifier>> temp_opinion_modifiers = new Dictionary<Character, List<TemporaryOpinionModifier>>();
		
		/// <summary>
		/// Returns a list of <see cref="OpinionModifier"/>s for all factors 
		/// between this character and the inputted character.
		/// </summary>
		/// <returns>The list of all opinion modifiers being applied.</returns>
		/// <param name="other">The character to judge.</param>
		public List<OpinionModifier> GetOpinionModifiers(Character other, DateTime current_date)
		{
			List<OpinionModifier> list = new List<OpinionModifier>();
			
			// Traits.
			list.AddRange(GetOpinionModifiersFromTraits(other));
			
			// Ideology.
			list.AddRange(GetOpinionModifiersFromIdeology(other));

			// Temp. Modifiers
			list.AddRange(GetOpinionModifiersFromTemporaryModifiers(other, current_date));
			
			// Opinion scaling.
			list = ScaleOpinionModifiers(list);
			
			return list;
		}
		
		public List<OpinionModifier> ScaleOpinionModifiers(List<OpinionModifier> list)
		{
			float positive_opinion_scale = TraitManager.GetPositiveOpinionValueScale();
			float negative_opinion_scale = TraitManager.GetNegativeOpinionValueScale();
			
			for (int i = 0; i < list.Count; i++)
			{
				OpinionModifier modifier = list[i];
				// Don't bother scaling if nothing modified the opinion scale.
				float epsilon = 0.001f;
				if (Math.Abs(positive_opinion_scale - 1.0f) > epsilon)
				{
					if (modifier.Value > 0)
					{
						list[i] = new OpinionModifier((int)(modifier.Value * positive_opinion_scale), modifier.Reason);
					}
				}
				
				if (Math.Abs(negative_opinion_scale - 1.0f) > epsilon)
				{
					if (modifier.Value < 0)
					{
						list[i] = new OpinionModifier((int)(modifier.Value * negative_opinion_scale), modifier.Reason);
					}
				}
			}
			return list;
		}

		public int GetOpinionValues(Character other, DateTime current_date)
		{
			return SumOpinionValue(GetOpinionModifiers(other, current_date));
		}
		
		/// <summary>
		/// Returns a summed value of all positive opinion values of another character, excluding negatives.
		/// </summary>
		/// <returns>The positive opinion value.</returns>
		/// <param name="other">Character to judge.</param>
		public int GetPositiveOpinionValues(Character other, DateTime current_date)
		{
			return FilterOpinionValues(GetOpinionModifiers(other, current_date), 1);
		}
		
		/// <summary>
		/// Returns a summed value of all negative opinion values of another character, excluding positives.
		/// </summary>
		/// <returns>The positive opinion value.</returns>
		/// <param name="other">Character to judge.</param>
		public int GetNegativeOpinionValues(Character other, DateTime current_date)
		{
			return FilterOpinionValues(GetOpinionModifiers(other, current_date), -1);
		}

		int FilterOpinionValues(List<OpinionModifier> modifiers, int desired_sign)
		{
			int sum = 0;
			foreach(var mod in modifiers)
			{
				if(Math.Sign(mod.Value) == desired_sign)
				{
					sum += mod.Value;
				}
			}
			return sum;
		}

		/// <summary>
		/// Adds up all the <see cref="OpinionModifier"/>s inputted, and 
		/// outputs the combined total.
		/// </summary>
		/// <returns>The opinion value.</returns>
		/// <param name="modifiers">Modifiers.</param>
		public int SumOpinionValue(List<OpinionModifier> modifiers)
		{
			int sum = 0;
			foreach (OpinionModifier O in modifiers)
			{
				sum += O.Value;
			}
			return sum;
		}
		
		/// <summary>
		/// Gets a list of <see cref="OpinionModifier"/>s from trait interactions 
		/// between this <see cref="Character"/> and another <see cref="Character"/>.
		/// </summary>
		/// <returns>The opinion from traits.</returns>
		/// <param name="other">Other.</param>
		public List<OpinionModifier> GetOpinionModifiersFromTraits(Character other)
		{
			List<OpinionModifier> list = new List<OpinionModifier>();
			
			// First, look inside our own traits, and judge them for our traits.
			foreach (Trait t in TraitManager.Traits)
			{
				OpinionModifier? trait_opinion = t.GetOpinionIntrinsic(this, other);
				if (trait_opinion != null) // Traits that don't override the above function return null.
				{
					list.Add((OpinionModifier)trait_opinion);
				}
			}
			
			// Next, look at their traits, and judge them for their traits.
			foreach (Trait t in other.TraitManager.Traits)
			{
				if(other.TraitManager.HiddenTraits.Contains(t))
				{
					// Trait not visible to this character, skip it.
					continue;
				}
				
				OpinionModifier? trait_opinion = t.GetOpinionExtrinsic(other, this);
				if (trait_opinion != null) // Traits that don't override the above function return null.
				{
					list.Add((OpinionModifier)trait_opinion);
				}
			}
			
			// Now package it all together.
			return list;
		}
		
		public int GetOpinionValueFromTraits(Character other)
		{
			return SumOpinionValue(GetOpinionModifiersFromTraits(other));
		}
		
		/// <summary>
		/// Gets a list of <see cref="OpinionModifier"/>s from trait interactions 
		/// between this <see cref="Character"/> and another <see cref="Character"/>.
		/// <returns>The <see cref="OpinionModifier"/>s from ideological sources.</returns>
		/// <param name="other">The character being compared against.</param>
		/// </summary>
		public List<OpinionModifier> GetOpinionModifiersFromIdeology(Character other)
		{
			List<OpinionModifier> list = new List<OpinionModifier>();
			if(Ideology.Apolitical || other.GetApparentIdeology().Apolitical)
			{
				return list;
			}
			
			float ideology_friction_modifier = TraitManager.GetIdeologyFrictionModifier();
			if(ideology_friction_modifier > 0)
			{
				switch (IsAligned(other))
				{
					case Ideologies.Ideology.Alignment.Identical: // Has the same ideology.
						list.Add(new OpinionModifier((int)(10 * ideology_friction_modifier), "Fellow likeminded follower of " + Ideology + "."));
						break;
					case Ideologies.Ideology.Alignment.Allied: // In the same group.
						list.Add(new OpinionModifier((int)(-5 * ideology_friction_modifier), "We have ideological differences."));
						break;
					case Ideologies.Ideology.Alignment.Neutral: // Seperate but not opposing.
						list.Add(new OpinionModifier((int)(-10 * ideology_friction_modifier), "They don't care for the concerns of " + Ideology.IdeologyGroup + "."));
						break;
					case Ideologies.Ideology.Alignment.Opposed: // Seperate and opposing.
						list.Add(new OpinionModifier((int)(-20 * ideology_friction_modifier), "Opposing ideologies."));
						break;
				}
			}
			return list;
		}
		
		public int GetOpinionValueFromIdeology(Character other)
		{
			return SumOpinionValue(GetOpinionModifiersFromIdeology(other));
		}
		
		public List<OpinionModifier> GetOpinionModifiersFromTemporaryModifiers(Character other, DateTime current_date)
		{
			List<OpinionModifier> list = new List<OpinionModifier>();
			
			temp_opinion_modifiers.TryGetValue(other, out var potential_list);
			if(potential_list is null)
			{
				return list; // No opinion modifiers.
			}
			
			foreach(var temp in potential_list)
			{
				TimeSpan difference = current_date - temp.Timestamp;
				TimeSpan modified_duration = temp.StandardDuration;
				
				// Scale durations based on traits like Vindictive or Forgiving.
				if(Math.Sign(temp.StartingValue) == 1) // Positive value.
				{
					var duration = (int)(modified_duration.Days * TraitManager.GetPositiveOpinionDurationScale());
					modified_duration = new TimeSpan(duration, 0, 0, 0);
				}
				else if(Math.Sign(temp.StartingValue) == -1)
				{
					var duration = (int)(modified_duration.Days * TraitManager.GetNegativeOpinionDurationScale());
					modified_duration = new TimeSpan(duration, 0, 0, 0);
				}
				
				// As time goes on, the value of the modifier will decay.
				float decay_scaler = Math.Abs((difference.Days / temp.StandardDuration.Days) - 1);
				int calculated_value = (int)(temp.StartingValue * decay_scaler);
				
				DateTime expiration_date = temp.Timestamp + modified_duration;
				list.Add(new OpinionModifier(calculated_value, temp.Reason + $" (Expires {expiration_date.ToShortDateString()})"));
			}
			return list;
		}
		
		public void AddTemporaryOpinionModifier(Character target, int value, string reason, DateTime current_date, TimeSpan duration)
		{
			temp_opinion_modifiers.TryGetValue(target, out var potential_list);
			// If this character hasn't received an opinion modifier for the 
			// target before, then the list will actually be null.
			if(potential_list is null)
			{
				potential_list = new List<TemporaryOpinionModifier>();
				temp_opinion_modifiers[target] = potential_list;
			}
			var temp_opinion = new TemporaryOpinionModifier(value, reason, current_date, duration);
			potential_list.Add(temp_opinion);
		}
		
		public void RemoveTemporaryOpinionModifier(Character target, TemporaryOpinionModifier temporary_opinion_modifier)
		{
			temp_opinion_modifiers.TryGetValue(target, out var potential_list);
			if(potential_list is null)
			{
				throw new Exception("Tried to remove a TemporaryOpinionModifier " +
					"from a character which has no list to contain them.");
			}
			potential_list.Remove(temporary_opinion_modifier);
			// If the new list is empty, then make it null.
			// It will be made into a new list if other modifiers are added later.
			if(potential_list.Count == 0)
			{
				temp_opinion_modifiers[target] = null;
			}
		}
	}

}
