namespace Autonomy.People.Characters.Opinions
{
	/// <summary>
	/// Used to package an individual modifier to a <see cref="Character"/>'s 
	/// opinion of another. Contains the integer value of the opinion, and a 
	/// string holding the reason for that modifier.
	/// </summary>
	public struct OpinionModifier
	{
		/// <summary>
		/// Amount of opinion to shift towards a specific direction. 
		/// Positive numbers make someone like someone else more, 
		/// negative numbers do the opposite, making them dislike 
		/// someone.
		/// </summary>
		public int Value { get; }
		
		/// <summary>
		/// An explaination for the player about why this modifier exists.
		/// </summary>
		public string Reason { get; }
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Autonomy.Characters.Opinions.OpinionModifier"/> struct.
		/// </summary>
		/// <param name="_new_value">How much to change an opinion. Positive numbers make people like each other more, negatives do the opposite.</param>
		/// <param name="_new_reason">Explaination about why this modifier exists, for the player.</param>
		public OpinionModifier(int _new_value, string _new_reason)
		{
			Value = _new_value;
			Reason = _new_reason;
		}
	}
}
