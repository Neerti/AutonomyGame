using System;
using System.Collections.Generic;
using Autonomy.People.Species;

namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// Base Trait for biological alterations applied to humans.
	/// </summary>
	public class TraitBiological : Trait
	{
		public TraitBiological()
		{
			SpeciesRestricted = new List<SpeciesIDs> { SpeciesIDs.Human };
		}
	}
}