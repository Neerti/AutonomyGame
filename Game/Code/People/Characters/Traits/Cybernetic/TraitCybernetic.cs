﻿using System;
using System.Collections.Generic;
using Autonomy.People.Species;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// Base Trait for cybernetic alterations applied to humanoids.
	/// </summary>
	public class TraitCybernetic : Trait
	{
		/// <summary>
		/// If true, it will bother other characters, unless they are 
		/// Transhumanists or AIs.
		/// </summary>
		protected bool advanced;
		
		public TraitCybernetic()
		{
			SpeciesRestricted = new List<SpeciesIDs> { SpeciesIDs.Human };
		}
		
		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if (advanced && other.Species.GetType() == typeof(HumanSpecies) && other.Ideology.IdeologyGroup.GetType() != typeof(IdeologyGroupTranshumanism))
			{
				return new OpinionModifier(-10, holder + " has a " + DisplayName + " installed.");
			}
			return base.GetOpinionExtrinsic(holder, other);
		}
	}
}