﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Test
{
	/// <summary>
	/// Base trait to use for unit tests.
	/// </summary>
	class Basic : Trait
	{
		public Basic()
		{
			TraitID = TraitIDs.Basic;
			DisplayName = "Testing Trait A";
			Description = "If you can read this, something has gone wrong.";
		}
	}
	
	/// <summary>
	/// Conflicts with <see cref="Basic"/>, to test for mutural exclusion.
	/// </summary>
	class AntiBasic : Basic
	{
		public AntiBasic()
		{
			TraitID = TraitIDs.AntiBasic;
			DisplayName = "Testing Trait B";
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Basic };
		}
	}
	
	/// <summary>
	/// The first prerequisite for some other test traits.
	/// </summary>
	class PartA : Basic
	{
		public PartA()
		{
			TraitID = TraitIDs.PartA;
			DisplayName = "Testing Trait Part A";
		}
	}
	
	/// <summary>
	/// The second prerequisite for some other test traits.
	/// </summary>
	class PartB : Basic
	{
		public PartB()
		{
			TraitID = TraitIDs.PartB;
			DisplayName = "Testing Trait Part B";
		}
	}
	
	/// <summary>
	/// Requires <see cref="PartA"/> OR <see cref="PartB"/> in 
	/// order to be added.
	/// </summary>
	class PrereqAny : Basic
	{
		public PrereqAny()
		{
			TraitID = TraitIDs.PrereqAny;
			DisplayName = "Testing Trait Prereq OR";
			PrerequisiteTraitsAny = new List<TraitIDs> { TraitIDs.PartA, TraitIDs.PartB };
		}
	}
	
	/// <summary>
	/// Requires <see cref="PartA"/> AND <see cref="PartB"/> in 
	/// order to be added.
	/// </summary>
	class PrereqAll : Basic
	{
		public PrereqAll()
		{
			TraitID = TraitIDs.PrereqAll;
			DisplayName = "Testing Trait Prereq AND";
			PrerequisiteTraitsAll = new List<TraitIDs> { TraitIDs.PartA, TraitIDs.PartB };
		}
	}
	
	/// <summary>
	/// Is only allowed to be added to human <see cref="Character"/>s.
	/// </summary>
	class HumanOnly : Basic
	{
		public HumanOnly()
		{
			TraitID = TraitIDs.HumanOnly;
			DisplayName = "Testing Trait Human Only";
			SpeciesRestricted = new List<SpeciesIDs> { SpeciesIDs.Human };
		}
	}
	
	/// <summary>
	/// Extrinsically gives -100 from all other characters.
	/// </summary>
	class HatedByAll : Basic
	{
		public HatedByAll()
		{
			TraitID = TraitIDs.HatedByAll;
			DisplayName = "Testing Trait Everyone Hates Holder";
		}
		
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return new OpinionModifier(-100, holder + " has the testing trait that makes everyone hate them.");
		}
	}
	
	/// <summary>
	/// Intrinsically gives 100 opinion to all other characters.
	/// </summary>
	class AllLoving : Basic
	{
		public AllLoving()
		{
			TraitID = TraitIDs.AllLoving;
			DisplayName = "Testing Trait Loves Everyone Else";
		}
		
		public override OpinionModifier? GetOpinionIntrinsic(Character holder, Character other)
		{
			return new OpinionModifier(100, other + " exists.");
		}
	}
	
	/// <summary>
	/// Intrinsically gives -100 opinion to all other characters.
	/// </summary>
	class AllHating : Basic
	{
		public AllHating()
		{
			TraitID = TraitIDs.AllHating;
			DisplayName = "Testing Trait Hates Everyone Else";
		}
		
		public override OpinionModifier? GetOpinionIntrinsic(Character holder, Character other)
		{
			return new OpinionModifier(-100, other + " exists.");
		}
	}
	
	/// <summary>
	/// Doubles ideology friction.
	/// </summary>
	class DoubleIdeologyFriction : Basic
	{
		public DoubleIdeologyFriction()
		{
			TraitID = TraitIDs.DoubleIdeologyFriction;
			DisplayName = "Testing Trait Hates Other Ideologies";
			IdeologyFrictionModifier = 2f;
		}
	}
	
	/// <summary>
	/// Scales positive opinion modifiers by 2.
	/// </summary>
	class DoublesPositiveOpinion : Basic
	{
		public DoublesPositiveOpinion()
		{
			TraitID = TraitIDs.DoublesPositiveOpinion;
			DisplayName = "Testing Trait Doubles Positive Opinion Modifiers";
			PositiveOpinionValueModifier = 2f;
		}
	}
	
	/// <summary>
	/// Scales positive opinion modifiers by 0.5.
	/// </summary>
	class HalvesPositiveOpinion : Basic
	{
		public HalvesPositiveOpinion()
		{
			TraitID = TraitIDs.HalvesPositiveOpinion;
			DisplayName = "Testing Trait Halves Positive Opinion Modifiers";
			PositiveOpinionValueModifier = 0.5f;
		}
	}
	
	/// <summary>
	/// Scales negative opinion modifiers by 2.
	/// </summary>
	class DoublesNegativeOpinion : Basic
	{
		public DoublesNegativeOpinion()
		{
			TraitID = TraitIDs.DoublesNegativeOpinion;
			DisplayName = "Testing Trait Doubles Negative Opinion Modifiers";
			NegativeOpinionValueModifier = 2f;
		}
	}
	
	/// <summary>
	/// Scales negative opinion modifiers by 0.5.
	/// </summary>
	class HalvesNegativeOpinion : Basic
	{
		public HalvesNegativeOpinion()
		{
			TraitID = TraitIDs.HalvesNegativeOpinion;
			DisplayName = "Testing Trait Halves Negative Opinion Modifiers";
			NegativeOpinionValueModifier = 0.5f;
		}
	}
	
	/// <summary>
	/// Automatically hidden trait that extrinsically gives -100 from all 
	/// other characters if they can see the trait for some reason.
	/// </summary>
	class Hidden : Basic
	{
		public Hidden()
		{
			TraitID = TraitIDs.Hidden;
			DisplayName = "Testing Trait Hidden";
			AutoHide = true;
		}
		
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return new OpinionModifier(-100, holder + " has the testing trait that is supposed to be secret.");
		}
	}
}