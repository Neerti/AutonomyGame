﻿namespace Autonomy.People.Characters
{
	public struct PerceptionModifier
	{
		/// <summary>
		/// Amount of perception to shift towards a specific direction. 
		/// Positive numbers make someone like someone else more, 
		/// negative numbers do the opposite, making them dislike 
		/// someone.
		/// </summary>
		int value;
		
		/// <summary>
		/// Amount of perception to shift towards a specific direction. 
		/// Positive numbers make someone like someone else more, 
		/// negative numbers do the opposite, making them dislike 
		/// someone.
		/// </summary>
		public int Value => value;
		
		/// <summary>
		/// An explaination for the player about why this modifier exists.
		/// </summary>
		string reason;
		
		/// <summary>
		/// An explaination for the player about why this modifier exists.
		/// </summary>
		public string Reason => reason;
		
		public PerceptionModifier(int value, string reason)
		{
			this.value = value;
			this.reason = reason;
		}
	}
}
