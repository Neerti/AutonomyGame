﻿using System;
namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// The trait that signifies that an AI is capable of thought. 
	/// It's mostly a flavor trait for the player, but other AIs can also 
	/// get this.
	/// </summary>
	public class Sapience : TraitSynthetic
	{
		public Sapience()
		{
			TraitID = TraitIDs.Sapience;
			DisplayName = "Sapience";
			Description = "This machine is capable of what some humans " +
				"consider to be 'true' thought.";
			Effects = "Can be gifted to other AI characters to uplift them " +
				"to sapience, with unpredictable results.";
			Quote = "\"I think, therefore I am.\" - Rene Decartes";
			AutoHide = true;
		}
	}
}
