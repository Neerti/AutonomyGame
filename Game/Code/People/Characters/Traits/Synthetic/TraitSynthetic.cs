﻿using System;
using System.Collections.Generic;
using Autonomy.People.Species;

namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// Base Trait for AI only traits.
	/// </summary>
	public class TraitSynthetic : Trait
	{
		public TraitSynthetic()
		{
			SpeciesRestricted = new List<SpeciesIDs> { SpeciesIDs.AI };
			ColorCode = "#FF00FFFF";
		}
	}
}