using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Populations;

namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// Traits are essentially a modifier for <see cref="Character"/>s.
	/// Changes can include stat adjustments, opinion modifiers, 
	/// AI behaviour changes, or just flavor.
	/// </summary>
	public abstract class Trait : ITooltipDisplayable
	{
		/// <summary>
		/// Internal identification enum for traits. 
		/// Each 'real' trait class used in the game should have a unique ID. 
		/// Subclasses used for inheritence (e.g. TraitPersonality) should 
		/// instead keep their trait_id to Base.
		/// </summary>
		public TraitIDs TraitID { get; protected set; } = TraitIDs.Base;
		
		/// <summary>
		/// The name of the <see cref="Trait"/>, displayed in UIs.
		/// </summary>
		public string DisplayName { get; protected set; }
		
		public override string ToString()
		{
			return DisplayName;
		}
		
		/// <summary>
		/// A short description about the <see cref="Trait"/>, to add flavor.
		/// </summary>
		public string Description { get; protected set; }

		/// <summary>
		/// Description of mechanical effects the <see cref="Trait"/> actually has.
		/// </summary>
		public string Effects { get; protected set; }
		
		/// <summary>
		/// A short quote about the <see cref="Trait"/>, real or fictional, for flavor.
		/// </summary>
		public string Quote { get; protected set; }
		
		/// <summary>
		/// The source for the above quote, real or fictional, for flavor.
		/// </summary>
		public string QuoteSource { get; protected set; }
		
		/// <summary>
		/// A html color code for the color of this trait, in "#AARRGGBB" format.
		/// </summary>
		public string ColorCode { get; protected set; } = "#FFFFFFFF";

		/// <summary>
		/// List of muturally exclusive <see cref="Trait"/> types, that 
		/// conflict with this <see cref="Trait"/>.
		/// Conflicts will stop this <see cref="Trait"/> from being applied 
		/// to a <see cref="Character"/> that has one or more conflicts, 
		/// as well as block the application of <see cref="Trait"/>s 
		/// that would conflict with this trait.
		/// </summary>
		public List<TraitIDs> ConflictingTraits { get; protected set; } = new List<TraitIDs>();

		/// <summary>
		/// List of <see cref="Trait"/> types where at least one of them must 
		/// exist on a <see cref="Character"/> before this trait can be 
		/// added to them.
		/// </summary>
		public List<TraitIDs> PrerequisiteTraitsAny { get; protected set; } = new List<TraitIDs>();

		/// <summary>
		/// List of <see cref="Trait"/> types where all the traits in the list 
		/// must exist on a <see cref="Character"/> before this trait can 
		/// be added to them.
		/// </summary>
		public List<TraitIDs> PrerequisiteTraitsAll { get; protected set; } = new List<TraitIDs>();

		/// <summary>
		/// List of <see cref="Species.CharacterSpecies"/> types that this 
		/// <see cref="Trait"/> can only be be added to. If empty, it can be 
		/// added to any species.
		/// </summary>
		public List<SpeciesIDs> SpeciesRestricted { get; protected set; } = new List<SpeciesIDs>();

		/// <summary>
		/// Determines if TraitManagers should automatically hide this trait 
		/// when their character receives it.
		/// </summary>
		public bool AutoHide { get; protected set; }

		/// <summary>
		/// Used to tell various systems if this trait should not be shared 
		/// among other characters with the same trait. Mutable traits 
		/// will not be placed in the global trait dictionary.
		/// </summary>
		public bool Mutable { get; protected set; }

		/// <summary>
		/// How likely this trait is to be assigned to a character randomly 
		/// when the character is being filled out. 0 means it will never 
		/// be assigned, and 100 means it will always be assigned.
		/// </summary>
		public int RandomAssignmentOdds { get; protected set; }

		// Trait attributes.

		/// <summary>
		/// Modifies how easy or hard it is for one character to persuade 
		/// another into drifting towards another Ideology.
		/// Positive numbers make it harder, while negative numbers make it 
		/// easier.
		/// </summary>
		public int IdeologyDriftDefence { get; protected set; }

		/// <summary>
		/// Scales the amount of 'friction' between two characters that 
		/// have different Ideologies. Higher numbers will make them 
		/// hate each other more, lower numbers do the opposite.
		/// This also modifies the opinion boost that people in the same 
		/// Ideology have with each other.
		/// </summary>
		public float IdeologyFrictionModifier { get; protected set; } = 1.0f;

		/// <summary>
		/// Scales the combined value of negative <see cref="OpinionModifier"/>, 
		/// before the final opinion calculation. Higher numbers make it 
		/// easier for the character to hate others, lower numbers do 
		/// the opposite.
		/// </summary>
		public float NegativeOpinionValueModifier { get; protected set; } = 1.0f;

		/// <summary>
		/// Scales the combined value of positive <see cref="OpinionModifier"/>, 
		/// before the final opinion calculation. Higher numbers make it 
		/// easier for the character to like others, lower numbers do 
		/// the opposite.
		/// </summary>
		public float PositiveOpinionValueModifier { get; protected set; } = 1.0f;

		/// <summary>
		/// Scales the length of negative <see cref="TemporaryOpinionModifier"/>s. 
		/// Higher numbers make them last longer, while smaller ones make them 
		/// last for a shorter period of time.
		/// </summary>
		public float NegativeOpinionDurationModifier { get; protected set; } = 1.0f;

		/// <summary>
		/// Scales the length of positive <see cref="TemporaryOpinionModifier"/>s. 
		/// Higher numbers make them last longer, while smaller ones make them 
		/// last for a shorter period of time.
		/// </summary>
		public float PositiveOpinionDurationModifier { get; protected set; } = 1.0f;
		
		// Override this for conditional opinion modifiers.
		
		/// <summary>
		/// Gets a <see cref="OpinionModifier"/> from a <see cref="Trait"/> based 
		/// on how the character holding the trait (<paramref name="holder"/>) feels 
		/// about a specific other character (<paramref name="other"/>) due 
		/// to holding this trait.
		/// This affects how the trait holder may see other <see cref="Character"/>s.
		/// </summary>
		/// <returns>The opinion modifier struct, or null.</returns>
		/// <param name="holder">The <see cref="Character"/> holding this trait.</param>
		/// <param name="other">The <see cref="Character"/> who is being evaluated by the holder.</param>
		public virtual OpinionModifier? GetOpinionIntrinsic(Character holder, Character other)
		{
			return null;
		}
		
		/// <summary>
		/// Gets a <see cref="OpinionModifier"/> from a <see cref="Trait"/> 
		/// based on how the other character (<paramref name="other"/>) feels about 
		/// the holder (<paramref name="holder"/>) for holding this trait.
		/// This affects how other <see cref="Character"/>s see the trait holder.
		/// </summary>
		/// <returns>The opinion modifier struct, or null.</returns>
		/// <param name="holder">The <see cref="Character"/> holding this trait.</param>
		/// <param name="other">The <see cref="Character"/> who is evaluating the holder.</param>
		public virtual OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return null;
		}
		
		/// <summary>
		/// Gets a <see cref="PerceptionModifier"/> from the trait based on how 
		/// the general public may react to the holder having the trait.
		/// </summary>
		/// <returns>The perception modifier.</returns>
		/// <param name="holder">The <see cref="Character"/> holding this trait.</param>
		/// <param name="perceiver">The <see cref="Demographic"/> that is judging the character.</param>
		public virtual PerceptionModifier? GetPerceptionModifier(Character holder, Demographic perceiver)
		{
			return null;
		}

		/// <summary>
		/// Combines several properies about the trait into a single string, 
		/// such as descriptions, effects, etc.
		/// </summary>
		/// <returns>The trait.</returns>
		public string TooltipInformation()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if(!string.IsNullOrWhiteSpace(Description))
			{
				sb.Append(Description);
			}
			if(!string.IsNullOrWhiteSpace(Effects))
			{
				if(sb.Length > 0)
				{
					sb.Append(Environment.NewLine + Environment.NewLine);
				}
				sb.Append(Effects);
			}
			if(!string.IsNullOrWhiteSpace(Quote))
			{
				if(sb.Length > 0)
				{
					sb.Append(Environment.NewLine + Environment.NewLine);
				}
				sb.Append(Quote);
			}
			return sb.ToString();
		}

		public string TooltipTitle()
		{
			return DisplayName;
		}
		
		public List<string> TooltipContents()
		{
			var contents = new List<string>();
			if(!string.IsNullOrWhiteSpace(Description))
			{
				contents.Add(Description);
			}
			
			if(!string.IsNullOrWhiteSpace(Effects))
			{
				contents.Add(Effects);
			}
			
			if(!string.IsNullOrWhiteSpace(Quote))
			{
				contents.Add(Quote);
			}
			return contents;
		}
	}
}
