using System;
using System.Collections.Generic;
using Autonomy.People.Species;

namespace Autonomy.People.Characters.Traits
{
	/// <summary>
	/// Base Personality <see cref="Trait"/>, that is restricted to humans only.
	/// </summary>
	public class TraitPersonality : Trait
	{
		public TraitPersonality()
		{
			RandomAssignmentOdds = 10;
			SpeciesRestricted = new List<SpeciesIDs> { SpeciesIDs.Human };
		}
	}
}
