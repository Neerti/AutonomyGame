﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits.Personality
{
	// Pragmatic people are much more approachable for people outside 
	// of their ideology, however they tend to be looked down upon by 
	// more committed people in their ideology.
	// Radicals in the same ideology get really upset with pragmatic people.
	public class Pragmatic : TraitPersonality
	{
		// Emphesizes decreased opinion malus modifiers, vs ideology drift defense.
		public Pragmatic()
		{
			TraitID = TraitIDs.Pragmatic;
			DisplayName = "Pragmatic";
			Description = "Results are more important than the means of " +
				"getting them, according to this character. They focus on " +
				"obtaining practical solutions, not letting ideological " +
				"concerns get in the way. This makes them look more " +
				"approachable to those 'on the other side', but this " +
				"can also be seen as noncommital by their more " +
				"orthodox counterparts.";
			Effects = "[b]Opinion:[/b]" +
				"[indent]" +
					$"[color=red]-40[/color] from {nameof(Radical)} people with the same ideology.\n" +
					"[color=red]-20[/color] from everyone else with the same ideology.\n" +
				"[/indent]" +
				"[b]Ideological Friction:[/b] [color=green]50%[/color].";
			IdeologyFrictionModifier = 0.50f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Radical };
		}
		
		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if (holder.IsAligned(other) is Ideology.Alignment.Identical && !other.TraitManager.HasTrait(this))
			{
				// Radicals get really mad at Pragmatics.
				if (other.TraitManager.HasTrait(TraitIDs.Radical))
				{
					return new OpinionModifier(-40, $"{holder} has no conviction, and is a traitor to the cause of {other.Ideology}.");
				}
				// Others in the same ideology look down on them (except for other Pragmatics).
				return new OpinionModifier(-20, $"{holder} has little care for the ideals of {holder.Ideology}.");
			}
			
			return base.GetOpinionExtrinsic(holder, other);
		}
	}
}