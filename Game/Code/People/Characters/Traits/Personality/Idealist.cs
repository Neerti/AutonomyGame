﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> increases the value of positive 
	/// opinion modifiers, and gives a mild opinion boost from everyone.
	/// </summary>
	public class Idealist : TraitPersonality
	{
		public Idealist()
		{
			TraitID = TraitIDs.Idealist;
			DisplayName = "Idealist";
			Description = "This person believes that people are inherently " +
				"good, and that the world's getting better as time goes on. " +
				"To them, the glass is half full.";
			Effects = "Minor opinion boost from everyone that is not cynical.\n" +
				"Cynical people instead have an opinion malus towards this character.\n" +
				"Positive opinion modifiers towards other people " +
				"are scaled up.";
			Effects = "[b]Opinion:[/b]" +
				"[indent]" +
					"[color=red]-20[/color] from cynical people.\n" +
					"[color=green]+5[/color] for everyone else.\n" +
				"[/indent]" +
				"[b]Positive Opinion Modifier:[/b] [color=green]125%[/color].";
			Quote = "Don't ever become a pessimist ... a pessimist is correct " +
				"oftener than an optimist, but an optimist has more fun, and " +
				"neither can stop the march of events.";
			QuoteSource = "Robert A. Heinlein, Time Enough for Love (1973)";
			PositiveOpinionValueModifier = 1.25f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Cynical };
		}
		
		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if(other.TraitManager.HasTrait(TraitIDs.Cynical))
			{
				return new OpinionModifier(-20, holder + " is hopelessly foolish and naive.");
			}
			return new OpinionModifier(5, holder + "'s optimism is uplifting.");
		}
	}
}