﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> describes someone who
	/// </summary>
	public class Vindictive : TraitPersonality
	{
		public Vindictive()
		{
			TraitID = TraitIDs.Vindictive;
			DisplayName = "Vindictive";
			Description = "This person tends to seek revenge against people " +
				"that have wronged them, real or imagined. As such, this " +
				"tends to place them on other peoples' bad sides.";
			Effects = "Decreases general opinion by 10.\n" +
				"Negative opinion modifiers towards other people " +
				"are scaled to 150%.\n" +
				"Negative opinion modifiers last twice as long.";
			NegativeOpinionValueModifier = 1.5f;
			NegativeOpinionDurationModifier = 2.0f;
		//	conflicting_traits = new List<Type> { typeof(TraitTODO) };
		}

		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return new OpinionModifier(-10, holder + " is vindictive.");
		}
	}
}