﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> decreases the value of positive 
	/// opinion modifiers, and gives a mild opinion malus from everyone.
	/// </summary>
	public class Cynical : TraitPersonality
	{
		public Cynical()
		{
			TraitID = TraitIDs.Cynical;
			DisplayName = "Cynical";
			Description = "Distrust towards other peoples' motives clouds " +
				"this person's thoughts, going beyond mere skepticism. " +
				"They believe everyone is driven by self-interest, " +
				"instead of altruism or virtue.";
			Effects = "[b]Opinion:[/b]" +
				"[indent]" +
					"[color=green]+10[/color] from other cynics.\n" +
					"[color=red]-10[/color] from everyone else.\n" +
				"[/indent]" +
				"[b]Positive Opinion Modifier:[/b] [color=red]75%[/color].";
			// Quote candidate 1.
			Quote = "Cynic: An idealist whose rose-colored glasses have been " +
				"removed, snapped in two and stomped into the ground, " +
				"immediately improving his vision.";
			QuoteSource = "Rick Bayan, The Cynic’s Dictionary (1994)";
			// Quote candidate 2.
			//		Quote = "I once said cynically of a politician, " +
			//			"\"He'll double-cross that bridge when he comes to it.\"" +
			//			" - Oscar Levant, The Memoirs of an Amnesiac (1965)";
			PositiveOpinionValueModifier = 0.75f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Idealist };
		}
		
		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if(other.TraitManager.HasTrait(TraitIDs.Cynical))
			{
				return new OpinionModifier(10, holder + " sees the world for what it really is.");
			}
			return new OpinionModifier(-10, holder + " is a bitter, jaded pessimist.");
		}
	}
}