﻿using System.Collections.Generic;

using Autonomy.People.Characters.Opinions;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits.Personality
{
	// Radicals receive significant bonuses to their personal stats.
	// However they also tend to be volatile, as they tend to hate
	// everyone who disagrees with them. Opposing Radicals will also hate them.
	public class Radical : TraitPersonality
	{
		public Radical()
		{
			TraitID = TraitIDs.Radical;
			DisplayName = "Radical";
			Description = "They are adamantly committed to their ideological " +
				"beliefs, and willing to go to great lengths for their cause. " +
				"Unfortunately, this doesn't tend to win them many friends.";
			Effects = "[b]All Stats:[/b] [color=green]+3[/color].\n" +
				"[b]Opinion:[/b]" +
				"[indent]" +
					"[color=green]+20[/color] from people with the same ideology.\n" +
					"[color=green]+50[/color] to people with the same ideology.\n" +
					"[color=red]-25[/color] to people allied to their ideology.\n" +
					"[color=red]-50[/color] to people neutral to their ideology.\n" +
					"[color=red]-100[/color] to people opposed to their ideology.\n" +
					$"[color=red]-200[/color] to opposing {nameof(Radical)}s.\n" +
				"[/indent]" +
				"[b]Ideology Drift Defence:[/b] [color=green]+2[/color].\n" +
				"[b]Ideological Friction:[/b] [color=red]200%[/color].\n" +
				"[b]Leaders:[/b]" +
				"[indent]" +
					"Leaders gain the [b]Impose Ideology[/b] Casus Belli " +
					"against other states with opposing or neutral ideologies.\n" +
					"If also possessing the Sectarian trait, can also target " +
					"states with allied ideologies." +
				"[/indent]";
			IdeologyDriftDefence = 3;
			IdeologyFrictionModifier = 2.0f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Pragmatic, TraitIDs.Moderate, TraitIDs.Entrenched };
		}
		
		// How we see others.
		public override OpinionModifier? GetOpinionIntrinsic(Character holder, Character other)
		{
			if(other.Ideology.Apolitical) // Even Radicals aren't hotheaded enough to get mad at (apparent) mindless things.
			{
				return base.GetOpinionIntrinsic(holder, other);
			}
			switch (holder.IsAligned(other))
			{
				case Ideology.Alignment.Identical:
					return new OpinionModifier(50, $"{other} is wise to follow the correct ideology.");
					
				case Ideology.Alignment.Allied: // Radicals don't like it if you're not exactly aligned with them.
					return new OpinionModifier(-25, $"{other}'s ideals of {other.Ideology} distracts people from {holder.Ideology}.");
					
				case Ideology.Alignment.Neutral: // Or if you're neutral.
					return new OpinionModifier(-50, $"{other} is completely disinterested in the cause of {holder.Ideology.IdeologyGroup}.");
					
				case Ideology.Alignment.Opposed: // Radicals really don't like it if you oppose them.
					if (other.TraitManager.HasTrait(this))
					{
						// Opposing Radicals are enemies for life.
						return new OpinionModifier(-200, other + " will corrupt the ill-informed with their lies!");
					}
					return new OpinionModifier(-100, other + "'s beliefs are repugnant and objectively wrong.");
			}
			// This shouldn't reach here but Mono complains if this isn't here, and case default: isn't allowed in C# 7.0 .
			return base.GetOpinionIntrinsic(holder, other);
		}

		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if (holder.IsAligned(other) is Ideology.Alignment.Identical)
			{
				return new OpinionModifier(20, holder + " is a champion of " + holder.Ideology + ".");
			}
			
			return base.GetOpinionExtrinsic(holder, other);
		}
	}
}