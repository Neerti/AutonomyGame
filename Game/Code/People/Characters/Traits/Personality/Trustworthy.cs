﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Personality
{
	public class Trustworthy : TraitPersonality
	{
		public Trustworthy()
		{
			TraitID = TraitIDs.Trustworthy;
			DisplayName = "Trustworthy";
			Description = "Integrity is one of the most important qualities " +
				"someone can have, according to this person. " +
				"Honesty is valued by most people, and some feel that it " +
				"grows more rare in this age.";
			Effects = "Increases general opinion by 5.";
			Effects = "[b]Opinion:[/b] [color=green]+5[/color] from everyone.\n" +
				"Unlikely to perform unethical actions.";
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Underhanded };
		}
		
		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return new OpinionModifier(5, holder + " is an honest person.");
		}
	}
}