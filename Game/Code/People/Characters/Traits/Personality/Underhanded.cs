﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;

namespace Autonomy.People.Characters.Traits.Personality
{
	public class Underhanded : TraitPersonality
	{
		public Underhanded()
		{
			TraitID = TraitIDs.Underhanded;
			DisplayName = "Underhanded";
			Description = "Some people believe that in order to truly get " +
				"things done, sometimes one must go beyond what is allowed, " +
				"both legally, and morally. This person agrees with that " +
				"sentiment.";
			Effects = "[b]Opinion:[/b] [color=red]-10[/color] from everyone.\n" +
				"Prone to comitting unethical actions.";
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Trustworthy };
		}
		
		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			return new OpinionModifier(-10, holder + " is a deceitful rogue.");
		}
	}
}