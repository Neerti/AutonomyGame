﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> describes someone who is set in their ways, 
	/// and is very difficult to change.
	/// Radicals like them more, if in the same Ideology.
	/// </summary>
	public class Entrenched : TraitPersonality
	{
		// Emphesizes increased ideology drift defense, vs opinion malus modifiers.
		public Entrenched()
		{
			TraitID = TraitIDs.Entrenched;
			DisplayName = "Entrenched";
			Description = "They are resolute in their views, and a staunch " +
				"supporter for their Ideology.";
			Effects = "Reduces opinion of those outside their Ideology by 10.\n" +
				"Opinion malus from conflicting Ideology increased by 50%.\n" +
				"More resistant to Idealogy Drift.";
			Effects = "[b]Opinion:[/b]" +
				"[indent]" +
					$"[color=green]+10[/color] from {nameof(Radical)} people with the same ideology.\n" +
					"[color=red]-10[/color] from people opposed to their ideology.\n" +
				"[/indent]" +
				"[b]Ideology Drift Defence:[/b] [color=green]+2[/color].\n" +
				"[b]Ideological Friction:[/b] [color=red]125%[/color].";
			IdeologyDriftDefence = 2;
			IdeologyFrictionModifier = 1.25f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Moderate, TraitIDs.Radical };
		}
		
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			if(other.IsAligned(holder) is Ideology.Alignment.Opposed)
			{
				return new OpinionModifier(-10, holder + " is entrenched in the wrong ideas.");
			}
			if(other.IsAligned(holder) is Ideology.Alignment.Identical && other.TraitManager.HasTrait(TraitIDs.Radical))
			{
				return new OpinionModifier(10, holder + " will be a longstanding ally of the " + other.Ideology.DisplayName + " cause.");
			}

			return base.GetOpinionExtrinsic(holder, other);
		}
	}
}