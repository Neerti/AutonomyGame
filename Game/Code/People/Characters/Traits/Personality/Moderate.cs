﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> describes someone who is able to be persuaded 
	/// easier, and less set in their ways.
	/// Radicals like them less, if in the same Ideology.
	/// </summary>
	public class Moderate : TraitPersonality
	{
		// Emphesizes decreased ideology drift defense, vs opinion malus modifiers.
		public Moderate()
		{
			TraitID = TraitIDs.Moderate;
			DisplayName = "Moderate"; // TODO: Better name/description.
			Description = "They are open to compromise and persuasion, which " +
				"makes them look more reasonable to some.";
			Effects = "[b]Opinion:[/b]" +
				"[indent]" +
					$"[color=red]-20[/color] from {nameof(Radical)} people with the same ideology.\n" +
					"[color=green]5[/color] from everyone else.\n" +
				"[/indent]" +
				"[b]Ideology Drift Defence:[/b] [color=red]-1[/color].\n" +
				"[b]Ideological Friction:[/b] [color=green]75%[/color].";
			IdeologyDriftDefence = -1;
			IdeologyFrictionModifier = 0.75f;
			ConflictingTraits = new List<TraitIDs> { TraitIDs.Entrenched, TraitIDs.Radical };
		}
		
		// How others feel about us.
		public override OpinionModifier? GetOpinionExtrinsic(Character holder, Character other)
		{
			// Radicals in the same ideology look down upon moderates.
			if(other.TraitManager.HasTrait(TraitIDs.Radical) && other.IsAligned(holder) is Ideology.Alignment.Identical)
			{
				return new OpinionModifier(-20, $"{holder} is liable to flip-flop away from {holder.Ideology}.");
			}
			return new OpinionModifier(5, $"{holder} is amendable to compromise.");
		}
	}
}
