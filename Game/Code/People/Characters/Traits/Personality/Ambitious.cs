﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Opinions;
using Autonomy.People.Populations;
using Autonomy.People.Ideologies;

namespace Autonomy.People.Characters.Traits.Personality
{
	/// <summary>
	/// This <see cref="Trait"/> describes someone who is driven to succeed.
	/// They tend to work harder to get what they want, and as such, this 
	/// is a universally good trait for someone to have. Their enemies, 
	/// on the other hand, will not appreciate that.
	/// </summary>
	public class Ambitious : TraitPersonality
	{
		public Ambitious()
		{
			TraitID = TraitIDs.Ambitious;
			DisplayName = "Ambitious";
			Description = "Ambition drives this character, as they have an " +
				"unending desire to reach for the stars, figuratively and " +
				"sometimes literally.";
			Effects = "Boost to all stats.\n" +
				"Perceived more favorably by the public, especially those " +
				"with the same ideological position. Opposing ideologies instead " +
				"will have a malus";
			Effects = "[b]All Stats:[/b] [color=green]+2[/color].\n" +
				"[b]Public Perception (Ideology):[/b]" +
				"[indent]" +
					"[color=green]+20[/color] if Identical.\n" +
					"[color=green]+15[/color] if Allied.\n" +
					"[color=green]+10[/color] if Neutral.\n" +
					"[color=red]-20[/color] if Opposed." +
				"[/indent]";
			Quote = "Ambition is like love, impatient\nBoth of delays and rivals.";
			QuoteSource = "John Denham, The Sophy: A Tragedy (1642), Act I, scene ii";
		}
		
		public override PerceptionModifier? GetPerceptionModifier(Character holder, Demographic perceiver)
		{
			Ideology demographic_ideology = Glob.AllIdeologies[perceiver.IdeologyID];
			switch (holder.Ideology.GetAlignment(demographic_ideology))
			{
				case Ideology.Alignment.Identical:
					return new PerceptionModifier(20, "Their ambition will be a boon to " + demographic_ideology + ".");
				case Ideology.Alignment.Allied:
					return new PerceptionModifier(15, "Their ambition might end up helping " + demographic_ideology + ".");
				case Ideology.Alignment.Neutral:
					return new PerceptionModifier(10, "They work hard towards success.");
				case Ideology.Alignment.Opposed:
					return new PerceptionModifier(-20, "Their ambition presents a greater threat to our cause.");
				default:
					return base.GetPerceptionModifier(holder, perceiver);
			}
		}
	}
}
