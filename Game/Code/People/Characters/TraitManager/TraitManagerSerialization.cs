﻿using System;
using Godot.Collections;

namespace Autonomy.People.Characters.TraitManagers
{
	public sealed partial class TraitManager : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			// For the list of traits, we can just store their TraitIDs, 
			// and not the traits themselves, because they're shared among 
			// all other TraitManagers.
			Array<string> trait_ids = new Array<string>();
			Array<string> hidden_trait_ids = new Array<string>();
			
			foreach (var t in traits)
			{
				trait_ids.Add(t.TraitID.ToString());
			}
			
			foreach (var t in hidden_traits)
			{
				hidden_trait_ids.Add(t.TraitID.ToString());
			}
			
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString() },
				{ nameof(Traits), trait_ids },
				{ nameof(HiddenTraits), hidden_trait_ids }
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			Godot.Collections.Array trait_ids = (Godot.Collections.Array)data[nameof(Traits)];
			Godot.Collections.Array hidden_trait_ids = (Godot.Collections.Array)data[nameof(HiddenTraits)];
			
			foreach (var trait_id in trait_ids)
			{
				TraitIDs parsed_trait_id;
				Enum.TryParse((string)trait_id, out parsed_trait_id);
				
				AddTrait(parsed_trait_id);
				if (hidden_trait_ids.Contains(trait_id))
				{
					HideTrait(Glob.AllTraits[parsed_trait_id]);
				}
			}
			
		}
		
	}
}
