﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Traits;

namespace Autonomy.People.Characters.TraitManagers
{
	// This part handles retrieving the combined attributes of all traits.
	public sealed partial class TraitManager
	{
		public float GetIdeologyFrictionModifier()
		{
			float result = 1.0f;
			foreach (Trait trait in traits)
			{
				result *= trait.IdeologyFrictionModifier;
			}
			return result;
		}
		
		public float GetNegativeOpinionValueScale()
		{
			float result = 1.0f;
			foreach (Trait trait in traits)
			{
				result *= trait.NegativeOpinionValueModifier;
			}
			return result;
		}
		
		public float GetPositiveOpinionValueScale()
		{
			float result = 1.0f;
			foreach (Trait trait in traits)
			{
				result *= trait.PositiveOpinionValueModifier;
			}
			return result;
		}
		
		public float GetNegativeOpinionDurationScale()
		{
			float result = 1.0f;
			foreach(Trait trait in traits)
			{
				result *= trait.NegativeOpinionDurationModifier;
			}
			return result;
		}
		
		public float GetPositiveOpinionDurationScale()
		{
			float result = 1.0f;
			foreach(Trait trait in traits)
			{
				result *= trait.PositiveOpinionDurationModifier;
			}
			return result;
		}
	}
}


