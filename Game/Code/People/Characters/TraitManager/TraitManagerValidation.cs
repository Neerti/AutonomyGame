﻿using System;
using System.Collections.Generic;
using Autonomy.People.Characters.Traits;

namespace Autonomy.People.Characters.TraitManagers
{
	// This part handles trait validation.
	public sealed partial class TraitManager
	{
		/// <summary>
		/// Checks if a <see cref="Trait"/> should be allowed to be added.
		/// </summary>
		/// <returns><c>true</c>, if it is valid to add the trait, <c>false</c> otherwise.</returns>
		/// <param name="trait_id">The <see cref="TraitIDs"/> that the 
		/// <see cref="Trait"/> to be added will be keyed to in the global dictionary.</param>
		public bool CanAddTrait(TraitIDs trait_id)
		{
			return CanAddTrait(Glob.AllTraits[trait_id]);
		}
		
		/// <summary>
		/// Checks if a <see cref="Trait"/> should be allowed to be added.
		/// </summary>
		/// <returns><c>true</c>, if it is valid to add the trait, <c>false</c> otherwise.</returns>
		/// <param name="trait">The instance of the <see cref="Trait"/> to check.</param>
		public bool CanAddTrait(Trait trait)
		{
			// Test for duplicates.
			if (HasTrait(trait))
			{
				return false;
			}
			
			// Test for needed prerequisites.
			if (!HasSatisfiedPrerequisiteTraits(trait))
			{
				return false;
			}
			
			// Test for conflicts.
			if (HasConflictingTrait(trait))
			{
				return false;
			}
			
			// Test for species restrictions.
			if (!HasAllowedSpeciesTrait(trait))
			{
				return false;
			}
			
			return true;
		}
		
		/// <summary>
		/// Checks if this <see cref="Character"/> has one or more 
		/// <see cref="Trait"/>s that conflict with the inputted trait type.
		/// </summary>
		/// <returns><c>true</c>, if a conflict exists, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of trait to check for conflicts.</param>
		bool HasConflictingTrait(Trait trait)
		{
			foreach (Trait other_trait in traits)
			{
				// First, check the incoming trait for conflicts against the group.
				if (other_trait.ConflictingTraits.Contains(trait.TraitID))
				{
					return true;
				}
				// Now check the group against this trait.
				if (trait.ConflictingTraits.Contains(other_trait.TraitID))
				{
					return true;
				}
			}
			return false;
		}
		
		/// <summary>
		/// Checks if the needed prerequisite <see cref="Trait"/>s, if any, are present on 
		/// the holder.
		/// </summary>
		/// <returns><c>true</c>, if trait prerequisites are satisifed, or don't exist, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of trait to test for unmet prerequisites.</param>
		bool HasSatisfiedPrerequisiteTraits(Trait trait)
		{
			// If the trait has no prerequisites at all, a lot of work can be skipped.
			if(trait.PrerequisiteTraitsAny.Count == 0 && trait.PrerequisiteTraitsAll.Count == 0)
			{
				return true;
			}
			
			bool all_satisfied = true;
			bool any_satisfied = false;
			if(trait.PrerequisiteTraitsAny.Count == 0)
			{
				any_satisfied = true;
			}
			else
			{
				foreach (TraitIDs prereq in trait.PrerequisiteTraitsAny)
				{
					if (HasTrait(prereq))
					{
						any_satisfied = true;
						break;
					}
				}
			}
			
			if (trait.PrerequisiteTraitsAll.Count == 0)
			{
				all_satisfied = true;
			}
			else
			{
				foreach (TraitIDs prereq in trait.PrerequisiteTraitsAll)
				{
					if (!HasTrait(prereq))
					{
						all_satisfied = false;
						break;
					}
				}
			}
			
			return all_satisfied && any_satisfied;
		}
		
		/// <summary>
		/// Checks if a <see cref="Trait"/> is allowed to be added to 
		/// the holder due to species restrictions.
		/// </summary>
		/// <returns><c>true</c>, if the <see cref="Trait"/> is valid due 
		/// to species, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of <see cref="Trait"/> to check.</param>
		bool HasAllowedSpeciesTrait(Trait trait)
		{
			if (trait.SpeciesRestricted.Count > 0)
			{
				Character my_character;
				holder.TryGetTarget(out my_character);
				
				if(my_character is null)
				{
					return true;
				}
				
				if (trait.SpeciesRestricted.Contains(my_character.Species.SpeciesID))
				{
					return true;
				}
				return false;
			}
			// No restrictions in place.
			return true;
		}
		
	}
}
