﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autonomy.People.Characters.Traits;

namespace Autonomy.People.Characters.TraitManagers
{
	/// <summary>
	/// This object is responsible for holding and manipulating traits that 
	/// their character has.
	/// </summary>
	public sealed partial class TraitManager
	{
		// Fields.

		/// <summary>
		/// The <see cref="Character"/> object that is holding onto this object.
		/// </summary>
		WeakReference<Character> holder;
		
		/// <summary>
		/// A list of traits this character has. 
		/// Use the AddTrait() and RemoveTrait() methods to interact with this list.
		/// </summary>
		List<Trait> traits = new List<Trait>();
		public List<Trait> Traits => traits;
		
		/// <summary>
		/// Traits from the above list that the character is trying to hide. 
		/// Generally only visible to the character, however methods for 
		/// uncovering hidden traits in other characters will exist.
		/// </summary>
		List<Trait> hidden_traits = new List<Trait>();
		public List<Trait> HiddenTraits => hidden_traits;
		
		// Constructor.
		public TraitManager() {}
		
		public TraitManager(Character new_holder)
		{
			holder = new WeakReference<Character>(new_holder);
		}
		
		// Methods.
		public void SetHolder(Character new_holder)
		{
			holder = new WeakReference<Character>(new_holder);
		}
		
		/// <summary>
		/// Attempts to add a preinstantiated <see cref="Trait"/> to the character. 
		/// Note that this will throw an exception if attempting to supply the TraitID 
		/// of a base trait, mutable trait, or a trait that is otherwise not 
		/// included in the Glob.AllTraits dictionary.
		/// </summary>
		/// <returns><c>true</c>, if the <see cref="Trait"/> was added, <c>false</c> otherwise.</returns>
		/// <param name="trait_id">New trait's TraitID to try to add.</param>
		public bool AddTrait(TraitIDs trait_id)
		{
			if(!Glob.AllTraits.ContainsKey(trait_id))
			{
				throw new Exception("Trait being added was not found in global trait dictionary.");
			}
			return AddTrait(Glob.AllTraits[trait_id]);
		}
		
		/// <summary>
		/// Attempts to add an already instantiated <see cref="Trait"/> to the character.
		/// </summary>
		/// <returns><c>true</c>, if the <see cref="Trait"/> was added, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of <see cref="Trait"/> to try to add.</param>
		public bool AddTrait(Trait trait)
		{
			if (trait.TraitID is TraitIDs.Base)
			{
				throw new Exception("Trait \"" + trait + "\" being added has the base TraitID defined.");
			}
			
			if (CanAddTrait(trait))
			{
				// Nothing went wrong, add the trait.
				traits.Add(trait);
				
				if(trait.AutoHide)
				{
					HideTrait(trait);
				}
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Checks if the holder has this trait.
		/// </summary>
		/// <returns><c>true</c>, if the character has the trait, <c>false</c> otherwise.</returns>
		/// <param name="trait_id">TraitID to check for.</param>
		public bool HasTrait(TraitIDs trait_id)
		{
			foreach (Trait trait in traits)
			{
				if(trait.TraitID == trait_id)
				{
					return true;
				}
			}
			return false;
		}
		
		/// <summary>
		/// Checks if the holder has this trait.
		/// </summary>
		/// <returns><c>true</c>, if the character has the trait, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of trait to check for.</param>
		public bool HasTrait(Trait trait)
		{
			return HasTrait(trait.TraitID);
		}
		
		// Removal
		
		/// <summary>
		/// Removes a trait from the character.
		/// </summary>
		/// <returns><c>true</c>, if trait was removed, <c>false</c> otherwise.</returns>
		/// <param name="trait">Instance of trait to attempt to remove.</param>
		public bool RemoveTrait(Trait trait)
		{
			if (HasTrait(trait))
			{
				traits.Remove(trait);
				hidden_traits.Remove(trait);
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Removes a trait from the character.
		/// </summary>
		/// <returns><c>true</c>, if trait was removed, <c>false</c> otherwise.</returns>
		/// <param name="trait_id">TraitID of the trait to attempt to remove.</param>
		public bool RemoveTrait(TraitIDs trait_id)
		{
			foreach (Trait t in traits)
			{
				if(t.TraitID == trait_id)
				{
					return RemoveTrait(t);
				}
			}
			return false;
		}
		
		public bool HideTrait(Trait trait)
		{
			if (!HasTrait(trait))
			{
				return false;
			}
			
			if (hidden_traits.Contains(trait))
			{
				return true;
			}
			
			hidden_traits.Add(trait);
			return true;
		}
		
		/// <summary>
		/// Returns a list of random traits that do not conflict with each 
		/// other.
		/// </summary>
		/// <returns>List of traits that are compatible with each other.</returns>
		/// <param name="trait_rolls">How many traits the function 
		/// should try to get.</param>
		/// <param name="always_add">If true, the random chance for a chosen 
		/// trait to be skipped is itself skipped.</param>
		public void AssignRandomTraits(int trait_rolls, bool always_add = false)
		{
			// TODO: Clean this up to be able to output a list of trait 
			// selections instead of directly writing to the trait list.
			int max = Glob.AllPersonalityTraits.Count;
			for (int i = 0; i < trait_rolls; i++)
			{
				Trait trait = Glob.AllPersonalityTraits.ElementAt(Glob.RNG.Next(max)).Value;
				if(always_add || trait.RandomAssignmentOdds >= Glob.RNG.Next(101))
				{
					AddTrait(trait);
				}
			}
		}
	}
}
