﻿using Godot;
using Godot.Collections;
using System;

namespace Autonomy.People.Characters
{
	public partial class Character : Node, ISaveable
	{
		public Dictionary<string, object> Save()
		{
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(DisplayName), DisplayName },
				{ nameof(Gender), Gender.ToString()},
				{ nameof(Species), Species.SpeciesID.ToString()},
				{ nameof(Ideology), Ideology.IdeologyID.ToString()},
				{ nameof(TraitManager), TraitManager.Save()}
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			display_name = (string)data[nameof(DisplayName)];
			
			Genders parsed_gender;
			Enum.TryParse( (string)data[nameof(Gender)], out parsed_gender);
			gender = parsed_gender;
			
			SpeciesIDs parsed_species_id;
			Enum.TryParse( (string)data[nameof(Species)], out parsed_species_id);
			Species = Glob.AllSpecies[parsed_species_id];
			
			IdeologyIDs parsed_ideology_id;
			Enum.TryParse( (string)data[nameof(Ideology)], out parsed_ideology_id);
			Ideology = Glob.AllIdeologies[parsed_ideology_id];
			
			//fake_ideology = (Ideologies.Ideology)Activator.CreateInstance(Type.GetType(data["FakeIdeology"]));
			
			trait_manager = (TraitManagers.TraitManager)Serialization.DecodeToObject((Dictionary)data[nameof(TraitManager)]);
			trait_manager.SetHolder(this);
		}
	}
}