namespace Autonomy.People.Species
{
	/// <summary>
	/// This contains information about a <see cref="Characters.Character"/>'s species, 
	/// and influences various aspects about them, such as random name generation.
	/// This information is contained in its own class in order to take advantage 
	/// of composition over inheritance, since it is rare (but not impossible) 
	/// for a character's species to change.
	/// </summary>
	public abstract class CharacterSpecies
	{
		/// <summary>
		/// Enum for seperating different species together. 
		/// Each species defined should have a unique ID.
		/// </summary>
		protected SpeciesIDs species_id = SpeciesIDs.Base;
		public SpeciesIDs SpeciesID => species_id;
		
		/// <summary>
		/// The name of the species, for UIs.
		/// </summary>
		protected string display_name;
		public string DisplayName => display_name;

		public override string ToString()
		{
			return display_name;
		}

		/// <summary>
		/// Flavor description for the species, shown in the UI.
		/// </summary>
		protected string description;
		public string Description => description;
		
		/// <summary>
		/// Array of possible genders that can be randomly assigned to a character holding this species class.
		/// </summary>
		protected Genders[] available_genders;
		public Genders[] AvailableGenders => available_genders;
		
		/// <summary>
		/// Randomly creates a species appropiate name for a character.
		/// Can take other factors like gender into consideration, as well.
		/// </summary>
		/// <returns>The name created for the character.</returns>
		/// <param name="character_to_name">The character to receive the name.</param>
		public abstract string GenerateName(Characters.Character character_to_name);
		public abstract string GenerateName(Genders gender_to_use);
	
	}
}
