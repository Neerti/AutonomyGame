using System;
using System.IO;
using System.Collections.Generic;
using Autonomy.People.Characters;

namespace Autonomy.People.Species
{
	public class HumanSpecies : CharacterSpecies
	{
		public HumanSpecies()
		{
			species_id = SpeciesIDs.Human;
			display_name = "Human";
			description = "Humans are bipedal creatures which have " +
				"managed to escape the confines of their home planet, " +
				"Earth. Despite this achievement, they've generally " +
				"remained quarrelsome, dividing themselves into groups " +
				"with similar beliefs.";
			available_genders = new Genders[] { Genders.Female, Genders.Male, Genders.Neutral };
		}

		public override string GenerateName(Genders gender_to_use)
		{
			List<string> first_names = new List<string>();
			
			switch (gender_to_use)
			{
				case Genders.Female:
					first_names.AddRange(File.ReadAllLines(@"Data/Names/Human/first_female.txt"));
					break;
				case Genders.Male:
					first_names.AddRange(File.ReadAllLines(@"Data/Names/Human/first_male.txt"));
					break;
				case Genders.Neutral:
					// TODO: Should there be a seperate file for this?
					first_names.AddRange(File.ReadAllLines(@"Data/Names/Human/first_female.txt"));
					first_names.AddRange(File.ReadAllLines(@"Data/Names/Human/first_male.txt"));
					break;
				default:
					throw new Exception("GenerateName received unexpected gender.");
			}
			List<string> last_names = new List<string>();
			last_names.AddRange(File.ReadAllLines(@"Data/Names/Human/last.txt"));

			Random rng = new Random();
			string new_name = first_names[rng.Next(first_names.Count)] + " " + last_names[rng.Next(last_names.Count)];
			return new_name;
		}

		public override string GenerateName(Character character_to_name)
		{
			return GenerateName(character_to_name.Gender);
		}
	}
}
