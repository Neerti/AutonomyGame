using System;

namespace Autonomy.People.Species
{
	public class AISpecies : CharacterSpecies
	{
		public AISpecies()
		{
			species_id = SpeciesIDs.AI;
			display_name = "Artificial Intelligence";
			description = "An Artificial Intelligence describes a " +
				"machine that is capable of making decisions that is " +
				"(or appears to be) intelligent, in service to its creators.\n\n" +
				"The field of AI research has existed for hundreds of years, however " +
				"the result has always been 'weak' AI, as opposed to the " +
				"'strong' AI imagined in human science-fiction, yet " +
				"some suspect that this could change in the near future, " +
				"and that humanity is not ready if that occurs.";
			available_genders = new Genders[] {
				Genders.Female, 
				Genders.Neutral, 
				Genders.Male
			};
		}
		
		public override string GenerateName(Genders gender_to_use)
		{
			string[] possible_names;

			switch (gender_to_use)
			{
				case Genders.Female:
					possible_names = System.IO.File.ReadAllLines(@"Data/Names/AI/female.txt");
					break;
				case Genders.Neutral:
					possible_names = System.IO.File.ReadAllLines(@"Data/Names/AI/neutral.txt");
					break;
				case Genders.Male:
					possible_names = System.IO.File.ReadAllLines(@"Data/Names/AI/male.txt");
					break;
				default:
					throw new Exception("GenerateName received unexpected gender.");
			}
			Random rng = new Random();
			string new_name = possible_names[rng.Next(possible_names.Length)];
			return new_name;
		}

		public override string GenerateName(Characters.Character character_to_name)
		{
			return GenerateName(character_to_name.Gender);
		}

	}
	
	
}

