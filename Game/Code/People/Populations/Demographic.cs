﻿using System;
using Autonomy.People.Ideologies;
using Autonomy.People.Species;

namespace Autonomy.People.Populations
{
	/// <summary>
	/// This object represents a segment of a large group of people seperated 
	/// by specific characteristics. 
	/// These are generally held inside a Population object.
	/// </summary>
	public partial class Demographic
	{
		/// <summary>
		/// Total size for this segment of population.
		/// </summary>
		ulong size;
		public ulong Size => size;
		
		/// <summary>
		/// What ideology this segment of the population is aligned with.
		/// </summary>
		IdeologyIDs ideology_id;
		public IdeologyIDs IdeologyID => ideology_id;
		
		/// <summary>
		/// What species this segment of the population is.
		/// </summary>
		SpeciesIDs species;
		public SpeciesIDs Species => species;
		
		// Constructors.
		public Demographic(ulong _size, IdeologyIDs _ideology_id, SpeciesIDs _species)
		{
			size = _size;
			ideology_id = _ideology_id;
			species = _species;
		}
		
		public Demographic() { }
		
		/// <summary>
		/// Adds (or subtracts if given a negative number) an amount of people 
		/// to the demographic.
		/// </summary>
		public void AdjustSize(int amount)
		{
			// This stuff is needed because int + ulong doesn't work with 
			// the + operator normally.
			ulong new_size = size;
			if(amount < 0)
			{
				new_size -= (ulong)(-amount);
			}
			else
			{
				new_size += (ulong)amount;
			}
			size = new_size;
		}
		
		/// <summary>
		/// Adds (or subtracts if given a negative number) an amount of people 
		/// to the demographic.
		/// </summary>
		public void AdjustSize(ulong amount)
		{
			size += amount;
		}
		
		/// <summary>
		/// Modifies the size of the demographic by a percentage.
		/// </summary>
		/// <param name="scalar">Scalar.</param>
		public void ScaleSize(float scalar)
		{
			size = (ulong)Math.Round(size * scalar);
		}
		
		public override string ToString()
		{
			return size + " - " + species + " - " + ideology_id;
		}
		
		/// <summary>
		/// Compares an instance of Demographic against this one, to see if 
		/// they are equivalent, meaning that they have the same values 
		/// except for their size.
		/// </summary>
		/// <returns><c>true</c>, if equivalent, <c>false</c> otherwise.</returns>
		/// <param name="dem">Demographic object to compare against.</param>
		public bool IsEquivalent(Demographic dem)
		{
			return species == dem.species && ideology_id == dem.ideology_id;
		}
	}
}
