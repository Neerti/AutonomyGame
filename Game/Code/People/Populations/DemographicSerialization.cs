﻿using System;
using Godot;
using Godot.Collections;
using Autonomy.People.Ideologies;
using Autonomy.People.Species;

namespace Autonomy.People.Populations
{
	public partial class Demographic : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(Size), Size.ToString()},
				{ nameof(Species), Species.ToString()},
				{ nameof(IdeologyID), IdeologyID.ToString()}
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			size =		UInt64.Parse	((string)data[nameof(Size)]);
			
			IdeologyIDs parsed_ideology_id;
			Enum.TryParse((string)data[nameof(IdeologyID)], out parsed_ideology_id);
			ideology_id = parsed_ideology_id;
			
			SpeciesIDs parsed_species_id;
			Enum.TryParse((string)data[nameof(Species)], out parsed_species_id);
			species = parsed_species_id;
		}
	}
}
