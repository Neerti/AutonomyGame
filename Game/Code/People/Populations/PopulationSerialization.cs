﻿using System;
using Godot;
using Godot.Collections;
using Autonomy.People.Ideologies;
using Autonomy.People.Species;

namespace Autonomy.People.Populations
{
	public partial class Population : ISaveable
	{
		public Dictionary<string, object> Save()
		{
			Array<string> demographic_jsons = new Array<string>();
			foreach (var dem in demographics)
			{
				demographic_jsons.Add( Serialization.ObjectToJSON(dem) );
			}
			
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(Demographics), demographic_jsons}
			};
			
		}
		
		public void Load(Dictionary<string, object> data)
		{
			Godot.Collections.Array demographic_data = (Godot.Collections.Array)data[nameof(Demographics)];
			
			foreach (var dem in demographic_data)
			{
				Demographic D = (Demographic)Serialization.JSONToObject((string)dem);
				demographics.Add(D);
			}
			
		}
	}
}
