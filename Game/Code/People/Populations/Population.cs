﻿using System;
using System.Collections.Generic;
using Autonomy.People.Ideologies;
using Autonomy.People.Species;

namespace Autonomy.People.Populations
{
	/// <summary>
	/// This object contains a number of Demographic objects. 
	/// It also modifies them broadly, and can filter or merge its 
	/// contents into a new Population.
	/// </summary>
	public partial class Population
	{
		List<Demographic> demographics = new List<Demographic>();
		public List<Demographic> Demographics => demographics;
		
		public Population()
		{
		}
		
		public Population(List<Demographic> new_populations)
		{
			demographics = new_populations;
		}
		
		public enum PopulationFilters
		{
			Ideology,
			Species
		}
		
		/// <summary>
		/// Returns a new Population with Demographic objects which match a filter.
		/// </summary>
		/// <returns>A new Population with filtered demographic objects.</returns>
		/// <param name="filter">Distinguishing characteristic type to filter for.</param>
		/// <param name="id">Identifier to filter for.</param>
		public Population Filter(PopulationFilters filter, Enum id)
		{
			List<Demographic> filtered_populations = new List<Demographic>();
			switch (filter)
			{
				case PopulationFilters.Ideology:
					IdeologyIDs ideology_id = (IdeologyIDs)id;
					foreach (var dem in demographics)
					{
						if(dem.IdeologyID == ideology_id)
						{
							filtered_populations.Add(dem);
						}
					}
					break;
					
				case PopulationFilters.Species:
					SpeciesIDs species_id = (SpeciesIDs)id;
					foreach (var dem in demographics)
					{
						if (dem.Species == species_id)
						{
							filtered_populations.Add(dem);
						}
					}
					break;
			}
			return new Population(filtered_populations);
		}
		
		/// <summary>
		/// Returns the total number of people contained inside this container.
		/// </summary>
		/// <returns>The sum.</returns>
		public ulong Sum()
		{
			ulong total = 0;
			foreach (Demographic dem in demographics)
			{
				total += dem.Size;
			}
			return total;
		}
		
		/// <summary>
		/// Distributes new people among the container proportional to their sizes. 
		/// It can also remove people in the same manner if given a negative 
		/// value.
		/// </summary>
		public void Distribute(long amount)
		{
			ulong total_population = Sum();
			for (int i = 0; i < demographics.Count; i++)
			{
				Demographic curr = demographics[i];
				curr.ScaleSize( (float)(total_population + (ulong)amount) / total_population);
			}
		}
		
		/// <summary>
		/// Distributes new people among the container proportional to their sizes. 
		/// It can also remove people in the same manner if given a negative 
		/// value.
		/// </summary>
		public void Distribute(int amount)
		{
			Distribute((long)amount);
		}
		
		/// <summary>
		/// Scales all contained Demographics by a percentage.
		/// </summary>
		/// <param name="scalar">Value to scale by.</param>
		public void Scale(float scalar)
		{
			foreach (var dem in demographics)
			{
				dem.ScaleSize(scalar);
			}
		}
		
		public long CalculateMonthlyGrowth(ulong existing_population_size, float yearly_growth_factor)
		{
			ulong new_population_size = (ulong)(existing_population_size * yearly_growth_factor);
			long population_delta = (long)(new_population_size - existing_population_size) / 12;
			return population_delta;
		}
		
		/// <summary>
		/// Processes the population dynamics of this Population. 
		/// Called every ingame month.
		/// </summary>
		public void ProcessPopulationDynamics()
		{
			ulong current_size = Sum();
			// TODO: Variable growth rates.
			long population_delta = CalculateMonthlyGrowth(current_size, 1.01F);
			Distribute(population_delta);
		}
		
		/// <summary>
		/// Merges two Populations into a new one with both of their 
		/// contents integrated together. Be aware that the contents of the 
		/// merged list is likely to be referenced by the one or both of 
		/// the old Containers, so writing to the new container is not recommended.
		/// </summary>
		/// <returns>Instance of Population with the results of 
		/// the merge.</returns>
		/// <param name="container_to_merge_with">Population to merge with.</param>
		public Population Merge(Population container_to_merge_with)
		{
			Population merged_population = new Population(Demographics);
			foreach (Demographic dem in container_to_merge_with.demographics)
			{
				merged_population.AddDemographic(dem);
			}
			return merged_population;
		}
		
		/// <summary>
		/// Integrates a Population object into this Population, 
		/// adding it into an existing equivalent object if needed.
		/// </summary>
		/// <param name="dem">Pop.</param>
		public void AddDemographic(Demographic dem)
		{
			Demographic equivalent_demographic = TryGetEquivalentDemographic(dem);
			// If this container already has a Population with these 
			// characteristics, make that one bigger instead.
			if (equivalent_demographic != null)
			{
				equivalent_demographic.AdjustSize(dem.Size);
				return;
			}
			// Otherwise add it to the container directly.
			demographics.Add(dem);
		}
		
		/// <summary>
		/// Returns an equivalent population object to the input, if one 
		/// exists.
		/// </summary>
		/// <returns>The equivalent population instance, or null.</returns>
		Demographic TryGetEquivalentDemographic(Demographic dem)
		{
			foreach (var D in demographics)
			{
				if(D.IsEquivalent(dem))
				{
					return D;
				}
			}
			return null;
		}

		public override string ToString()
		{
			var builder = new System.Text.StringBuilder();
			builder.AppendLine("[");
			foreach (var dem in demographics)
			{
				builder.AppendLine(dem.ToString());
			}
			builder.AppendLine("]");
			return builder.ToString();
		}
	}
}
