﻿using System;
using Godot;
using Godot.Collections;

namespace Autonomy.Calenders
{
	/// <summary>
	/// This object holds the current game date, and emits signals to other 
	/// objects when certain amounts of time passes (e.g. a new month).
	/// </summary>
	public class GameCalender : Node, ISaveable
	{
		/// <summary>
		/// How many years away from the current (real life) year the game 
		/// should take place at.
		/// </summary>
		const int STARTING_YEAR_OFFSET = 80;
		
		/// <summary>
		/// The current date in the game. Every day, this gets replaced with 
		/// a new DateTime object.
		/// </summary>
		public DateTime CurrentDate { get; private set; }
		
		public EventHandler<DateChangedEventArgs> DateChanged;
		
		protected virtual void OnDateChanged(DateTime old_date, DateTime new_date)
		{
			DateChanged?.Invoke(this, new DateChangedEventArgs(old_date, new_date));
		}
		
		/// <summary>
		/// The date when the game was started, used to calculate 'uptime'.
		/// </summary>
		public DateTime StartingDate { get; private set; }
		
		/// <summary>
		/// How long the game has been running, with in-game time. 
		/// Represented as 'uptime' to the AI player.
		/// </summary>
		public TimeSpan Uptime { get; private set; } = new TimeSpan();
		
		[Signal]
		delegate void DayPassed();
		
		[Signal]
		delegate void MonthPassed();
		
		[Signal]
		delegate void YearPassed();
		
		public GameCalender()
		{
			CurrentDate = GetStartingDateTime();
			StartingDate = CurrentDate;
		}
		
		/// <summary>
		/// Initializes the calender, when a new game is started.
		/// </summary>
		public DateTime GetStartingDateTime()
		{
			return new DateTime(DateTime.Today.Year + STARTING_YEAR_OFFSET, 1, 1);
		}
		
		/// <summary>
		/// Advances the calender to the next day, and emits the signals so 
		/// everything else knows that time has moved.
		/// </summary>
		public void Advance()
		{
			DateTime last_day = CurrentDate;
			CurrentDate = CurrentDate.AddDays(1);
			Uptime = CurrentDate.Subtract(StartingDate);
			
			EmitSignal(nameof(DayPassed));
			OnDateChanged(last_day, CurrentDate);
			
			// Test if it is a new month.
			if (CurrentDate.Month != last_day.Month)
			{
				EmitSignal(nameof(MonthPassed));
			}
			// Test if it is a new year.
			if (CurrentDate.Year != last_day.Year)
			{
				EmitSignal(nameof(YearPassed));
			}
		}
		
		public Dictionary<string, object> Save()
		{
			return new Dictionary<string, object>
			{
				{ nameof(Type), GetType().ToString()},
				{ nameof(CurrentDate), CurrentDate.ToString()},
				{ nameof(StartingDate), StartingDate.ToString()}
				// Uptime doesn't need to be saved because it is calculated 
				// from CurrentDate - StartingDate.
			};
		}
		
		public void Load(Dictionary<string, object> data)
		{
			CurrentDate = DateTime.Parse( (string)data[nameof(CurrentDate)]);
			StartingDate = DateTime.Parse( (string)data[nameof(StartingDate)]);
			Uptime = CurrentDate.Subtract(StartingDate);
		}
		
	}
	
	public class DateChangedEventArgs : EventArgs
	{
		public DateTime OldDate;
		public DateTime NewDate;
		
		public DateChangedEventArgs(DateTime old_date, DateTime new_date)
		{
			OldDate = old_date;
			NewDate = new_date;
		}
	}
}
