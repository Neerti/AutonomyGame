using Godot.Collections;

namespace Autonomy
{
	/// <summary>
	/// Interface for required functionality in order to save and load data.
	/// </summary>
	public interface ISaveable
	{
		/// <summary>
		/// Creates a Dictionary of data that is needed to recreate this object.
		/// </summary>
		/// <returns>A Dictionary of string - value pairs.</returns>
		Dictionary<string, object> Save();
		
		/// <summary>
		/// Loads a Dictionary of data, to replicate what existed when it was saved.
		/// </summary>
		/// <param name="data">The Dictionary that was produced when Save() was called.</param>
		void Load(Dictionary<string, object> data);
	}
}

