﻿using System;
using System.Collections.Generic;

namespace Autonomy
{
	/// <summary>
	/// Interface for UI systems to retrieve tooltip information about the object implementing it.
	/// </summary>
	public interface ITooltipDisplayable
	{
		/// <summary>
		/// Retrieves a string which should be the 'title' of the tooltip, 
		/// generally the name of what is being displayed.
		/// </summary>
		/// <returns>The title.</returns>
		string TooltipTitle();
		
		/// <summary>
		/// Retrieves information about the 'body' of the tooltip. 
		/// Each string in the list represents one section of the tooltip, and 
		/// inbetween sections is a horizontal rule, seperating them.
		/// </summary>
		/// <returns>A list of strings which represent seperated paragraphs.</returns>
		List<string> TooltipContents();
	}
}
