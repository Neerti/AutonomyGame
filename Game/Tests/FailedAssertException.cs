﻿using System;

namespace Autonomy.Test
{
	// TODO: Find a better way to do this than using exceptions.
	
	/// <summary>
	/// This exception is thrown when an assertion fails when using the 
	/// Assert static class for unit testing. It is caught by the runner, 
	/// which fails the test.
	/// </summary>
	[Serializable]
	public class FailedAssertException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T:FailedAssertException"/> class
		/// </summary>
		public FailedAssertException()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:FailedAssertException"/> class
		/// </summary>
		/// <param name="message">A <see cref="T:System.String"/> that describes the exception. </param>
		public FailedAssertException(string message) : base(message)
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:FailedAssertException"/> class
		/// </summary>
		/// <param name="message">A <see cref="T:System.String"/> that describes the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. </param>
		public FailedAssertException(string message, Exception inner) : base(message, inner)
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:FailedAssertException"/> class
		/// </summary>
		/// <param name="context">The contextual information about the source or destination.</param>
		/// <param name="info">The object that holds the serialized object data.</param>
		protected FailedAssertException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
		{
		}
	}
}
