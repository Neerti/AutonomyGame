﻿using System;
using System.Collections.Generic;
using Autonomy.People;
using Autonomy.People.Characters;

namespace Autonomy.Test
{
	public class TraitTests
	{
		// TODO: Move into TraitManager tests?
		
		[UnitTest("Ensure that AddTrait() cannot add duplicates.")]
		public void UniqueTraitTest()
		{
			// Arrange.
			Character character = Character.MakeStandardizedHuman();
			
			// Act.
			character.TraitManager.AddTrait(TraitIDs.Basic);
			character.TraitManager.AddTrait(TraitIDs.Basic);
			
			// Assert.
			Assert.IsEqual(character.TraitManager.Traits.Count, 1);
		}
		
		[UnitTest("Ensure that AddTrait() cannot add conflicting traits together.")]
		[TestArguments(null, TraitIDs.Basic, TraitIDs.AntiBasic)]
		[TestArguments(null, TraitIDs.AntiBasic, TraitIDs.Basic)]
		public void ConflictingTraitTest(TraitIDs first, TraitIDs second)
		{
			// Arrange.
			Character character = Character.MakeStandardizedHuman();

			// Act.
			character.TraitManager.AddTrait(first);
			character.TraitManager.AddTrait(second);

			// Assert.
			Assert.IsEqual(character.TraitManager.Traits.Count, 1);
		}
		
		[UnitTest("Ensure that traits with prerequisites are correctly handled.")]
		[TestArguments(null, new TraitIDs[] { TraitIDs.PartA, TraitIDs.PrereqAny }, 2)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.PrereqAny }, 0)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.PartA, TraitIDs.PartB, TraitIDs.PrereqAll }, 3)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.PartA, TraitIDs.PrereqAll }, 1)]
		public void PrereqAnyTraitTest(TraitIDs[] trait_ids, int expected_count)
		{
			// Arrange.
			Character character = Character.MakeStandardizedHuman();
			
			// Act.
			foreach(var id in trait_ids)
			{
				character.TraitManager.AddTrait(id);
			}
			
			// Assert.
			Assert.IsEqual(character.TraitManager.Traits.Count, expected_count);
		}
		
		[UnitTest("Ensures that traits that are species restricted function correctly.")]
		public void SpeciesRestrictionTest()
		{
			// Arrange.
			Character human = Character.MakeStandardizedHuman();
			Character ai = Character.MakeStandardizedAI();
			
			// Act.
			human.TraitManager.AddTrait(TraitIDs.HumanOnly);
			ai.TraitManager.AddTrait(TraitIDs.HumanOnly);
			
			// Assert.
			Assert.IsEqual(human.TraitManager.Traits.Count, 1);
			Assert.IsEqual(ai.TraitManager.Traits.Count, 0);
		}

		[UnitTest("Ensures that characters can act on extrinsic opinion modifiers.")]
		public void OpinionModifierTest()
		{
			// Arrange.
			Character loves_other = Character.MakeStandardizedHuman();
			Character hates_other = Character.MakeStandardizedHuman();
			
			// Act.
			loves_other.TraitManager.AddTrait(TraitIDs.HatedByAll);
			loves_other.TraitManager.AddTrait(TraitIDs.AllLoving);
			
			// Assert.
			Assert.IsEqual(hates_other.GetOpinionValueFromTraits(loves_other), -100);
			Assert.IsEqual(loves_other.GetOpinionValueFromTraits(hates_other), 100);
		}
		
		[UnitTest("Ensures that ideology friction modifiers on traits function properly.")]
		public void IdeologyFrictionTraitTest()
		{
			// Arrange.
			Character A = Character.MakeStandardizedHuman();
			Character B = Character.MakeStandardizedHuman();
			A.SetIdeology(IdeologyIDs.TomatoSoup);

			// Act.
			A.TraitManager.AddTrait(TraitIDs.DoubleIdeologyFriction);

			// Assert.
			Assert.IsEqual(A.GetOpinionValueFromIdeology(B), -40);
		}

		[UnitTest("Ensures that opinion scaling functions properly, in both directions.")]
		[TestArguments(null, new TraitIDs[] { TraitIDs.AllLoving, TraitIDs.DoublesPositiveOpinion }, 200)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.AllLoving, TraitIDs.HalvesPositiveOpinion }, 50)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.AllHating, TraitIDs.DoublesNegativeOpinion }, -200)]
		[TestArguments(null, new TraitIDs[] { TraitIDs.AllHating, TraitIDs.HalvesNegativeOpinion }, -50)]
		public void OpinionScaleTraitTest(TraitIDs[] trait_ids, int expected_opinion)
		{
			// Arrange.
			Character A = Character.MakeStandardizedHuman();
			Character B = Character.MakeStandardizedHuman();

			// Act.
			foreach(var id in trait_ids)
			{
				A.TraitManager.AddTrait(id);
			}
			List<People.Characters.Opinions.OpinionModifier> list = A.GetOpinionModifiersFromTraits(B);
			list = A.ScaleOpinionModifiers(list);

			// Assert.
			Assert.IsEqual(A.SumOpinionValue(list), expected_opinion);
		}

		[UnitTest("Ensures that hidden traits do not influence opinions from others by default.")]
		public void HiddenTraitTest()
		{
			// Arrange.
			Character A = Character.MakeStandardizedHuman();
			Character B = Character.MakeStandardizedHuman();

			// Act.
			A.TraitManager.AddTrait(TraitIDs.Hidden);

			// Assert.
			Assert.IsEqual(B.GetOpinionValueFromTraits(A), 0);
		}

		[UnitTest("Ensures that TraitManager.AssignRandomTraits() is functional.")]
		public void RandomTraitTest()
		{
			// Arrange.
			Character A = Character.MakeStandardizedHuman();
			Character B = Character.MakeStandardizedHuman();

			// Act.
			A.TraitManager.AssignRandomTraits(5, true);

			// Assert.
			Assert.Greater(A.TraitManager.Traits.Count, 0);
		}
	}
}
