﻿using System;
using System.Collections.Generic;
using Autonomy.Research;
using Autonomy.Research.TechnologyNodes;
using Autonomy.Research.TechnologyTrees;
using Autonomy.Research.TechnologyTrees.AI;
using Autonomy.Research.TechnologyTrees.Testing;

namespace Autonomy.Test
{
	public class TechnologyTreeTest
	{
		[UnitTest("Test tech tree has correct amount of nodes.")]
		public void TestTreeNodeCountTest()
		{
			// Arrange.
			TechnologyTree tree = new TestTree();
			
			// Act.
			tree.BuildTree();
			
			// Assert.
			Assert.IsEqual(tree.Nodes.Count, 5); // TODO: Make this not a magic number.
		}
		
		[UnitTest("GetAllParents method is functional and accurate.")]
		public void GetAllParentsTest()
		{
			// Arrange.
			TechnologyTree tree = new TestTree();
			tree.BuildTree();
			
			// Act.
			TechnologyNode node = tree.Nodes[TechIDs.TestAB];
			List<TechnologyNode> parent_nodes = tree.GetAllParentsOfNode(node);
			
			// Assert.
			Assert.True(parent_nodes.Contains(tree.Nodes[TechIDs.TestRoot]));
			Assert.True(parent_nodes.Contains(tree.Nodes[TechIDs.TestA]));
			Assert.True(parent_nodes.Contains(tree.Nodes[TechIDs.TestB]));
			Assert.False(parent_nodes.Contains(tree.Nodes[TechIDs.TestA2]));
		}

		[UnitTest("Prerequisite check is functional.")]
		[TestArguments(null, TechIDs.TestAB, false)]
		[TestArguments(null, TechIDs.TestA, true)]
		public void PrerequisiteTest(TechIDs id, bool expected_result)
		{
			// Arrange.
			TechnologyTree tree = new TestTree();
			tree.BuildTree();
			
			// Act.
			TechnologyNode node = tree.Nodes[id];
			
			// Assert.
			Assert.IsEqual(node.CanBeResearched(), expected_result);
		}
		
		// TODO: Add the regular tech tree here, when it exists.
		
		[UnitTest("No unreachable nodes exist.")]
		[TestArguments(null, typeof(TestTree))]
		[TestArguments(null, typeof(AITree))]
		public void NoUnreachableNodesTest(Type tree_type)
		{
			// Arrange.
			TechnologyTree tree = (TechnologyTree)Activator.CreateInstance(tree_type);
			tree.BuildTree();
			
			// Assert.
			Assert.IsEqual(tree.CheckForUnreachableNodes().Count, 0);
		}
		
		[UnitTest("TechnologyTree serialization.")]
		public void SerializationTest()
		{
			// Arrange.
			TechnologyTree tree = new TestTree();
			tree.BuildTree();
			tree.Nodes[TechIDs.TestRoot].AddProgress(100);
			tree.Nodes[TechIDs.TestA].AddProgress(50);
			
			// Act.
			string json = Serialization.ObjectToJSON(tree);
			TechnologyTree loaded_tree = (TechnologyTree)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.IsEqual(tree.Nodes.Count, loaded_tree.Nodes.Count);
			Assert.True(loaded_tree.Nodes[TechIDs.TestRoot].Finished);
			Assert.IsEqual(loaded_tree.Nodes[TechIDs.TestA].ResearchProgress, 50);
		}
	}
}