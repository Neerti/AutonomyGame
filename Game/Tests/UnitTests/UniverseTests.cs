﻿using Autonomy.States;
using Autonomy.Universes;

namespace Autonomy.Test
{
	public class UniverseTests
	{
		[UnitTest("Serialization.")]
		public void SerializationTest()
		{
			// Arrange.
			var universe = new Universe();
			universe.Initialize();
			
			for(int i = 0; i < 10; i++)
			{
				State state = State.MakeStandardizedState(universe);
			}
			
			// Act.
			string json = Serialization.ObjectToJSON(universe);
			var loaded_universe = (Universe)Serialization.JSONToObject(json);

			// Assert.
			Assert.IsEqual(universe.AllStates.Count, loaded_universe.AllStates.Count);
		}
	}
}