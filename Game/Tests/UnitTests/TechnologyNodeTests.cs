﻿using Autonomy.Research.TechnologyNodes;

namespace Autonomy.Test
{
	public class TechnologyNodeTests
	{
		const int TEST_COST = 100;
		
		[UnitTest("Progress overflow is returned properly.")]
		public void ProgressOverflowTest()
		{
			// Arrange.
			TechnologyNode test_node = new TestNode();
			
			// Act.
			int overflow = test_node.AddProgress(TEST_COST + 50);
			
			// Assert.
			Assert.IsEqual(overflow, 50);
		}
		
		[UnitTest("Finished bool is set when completed.")]
		public void FinishedTest()
		{
			// Arrange.
			TechnologyNode test_node = new TestNode();
			
			// Act.
			test_node.AddProgress(TEST_COST);
			
			// Assert.
			Assert.True(test_node.Finished);
		}
		
		[UnitTest("Saving and loading of TechnologyNodes.")]
		public void SerializationTest()
		{
			// Arrange.
			TechnologyNode test_node = new TestNode();
			test_node.AddProgress(TEST_COST);
			
			// Act.
			string json = Serialization.ObjectToJSON(test_node);
			TechnologyNode loaded_node = (TechnologyNode)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.IsEqual(test_node.ResearchProgress, loaded_node.ResearchProgress);
			Assert.IsEqual(test_node.ResearchCost, loaded_node.ResearchCost);
			Assert.IsEqual(test_node.Finished, loaded_node.Finished);
		}
		
		public class TestNode : TechnologyNode
		{
			public TestNode()
			{
				research_cost = TEST_COST;
			}
		}
	}
}