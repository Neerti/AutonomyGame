﻿using Autonomy.People.Characters;
using Autonomy.People.Characters.TraitManagers;

namespace Autonomy.Test
{
	public class TraitManagerTests
	{
		[UnitTest("Ensures that TraitManager objects can save and load traits.")]
		public void UniqueTraitTest()
		{
			// Arrange.
			TraitManager manager = new TraitManager(Character.MakeStandardizedHuman());
			manager.AddTrait(TraitIDs.AllLoving);
			manager.AddTrait(TraitIDs.HatedByAll);
			manager.HideTrait(Glob.AllTraits[TraitIDs.HatedByAll]);
			
			// Act.
			string json = Serialization.ObjectToJSON(manager);
			TraitManager loaded_manager = (TraitManager)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.True(loaded_manager.HasTrait(TraitIDs.AllLoving));
			Assert.True(loaded_manager.HasTrait(TraitIDs.HatedByAll));
			Assert.Greater(loaded_manager.HiddenTraits.Count, 0);
		}
	}
}

/*

*/
