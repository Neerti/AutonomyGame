﻿using System.Collections.Generic;

using Autonomy.People;
using Autonomy.People.Characters;
using Autonomy.People.Populations;
using Autonomy.States.Governments;

namespace Autonomy.Test
{
	// The module needs to be burned with fire and rewritten to make unit testing
	// it easier.
	public class ElectiveSuccessionTests
	{
		[UnitTest("Candidate Score test.")]
		public void CandidateScoreTest()
		{
			// Arrange.
			var government = Government.MakeStandardizedGovernment(new Universes.Universe());
			var succession = new ElectiveSuccession(government);
			var candidate_soup = Character.MakeStandardizedHuman();
			candidate_soup.SetIdeology(IdeologyIDs.TomatoSoup);

			var candidates = new List<Character> { candidate_soup };

			var dem_soup = new Demographic(10000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			var dem_sandwich = new Demographic(10000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);
			
			// Assert.
			Assert.IsEqual(succession.CalculateCandidateScore(candidate_soup, dem_soup, candidates), 125);
			Assert.IsEqual(succession.CalculateCandidateScore(candidate_soup, dem_sandwich, candidates), -1000);
		}
		
		[UnitTest("Turnout Power test.")]
		public void TurnoutPowerTest()
		{
			// Arrange.
			var government = Government.MakeStandardizedGovernment(new Universes.Universe());
			var succession = new ElectiveSuccession(government);

			var candidate_soup = Character.MakeStandardizedHuman();
			candidate_soup.SetIdeology(IdeologyIDs.TomatoSoup);

			var candidates = new List<Character> { candidate_soup };
			var dem_soup = new Demographic(10000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			var dem_sandwich = new Demographic(10000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);

			// Assert.
			Assert.IsEqual(succession.CalculateTurnoutPower(candidate_soup, dem_soup, candidates), 0.25f);
			Assert.IsEqual(succession.CalculateTurnoutPower(candidate_soup, dem_sandwich, candidates), 0.0f);
		}

		[UnitTest("DivideVotingPie() test.")]
		public void DivideVotingPieTest()
		{
			// Arrange.
			var government = Government.MakeStandardizedGovernment(new Universes.Universe());
			var succession = new ElectiveSuccession(government);

			var candidate_soup = Character.MakeStandardizedHuman();
			candidate_soup.SetIdeology(IdeologyIDs.TomatoSoup);
			
			var candidate_sandwich = Character.MakeStandardizedHuman();
			candidate_sandwich.SetIdeology(IdeologyIDs.BLTSandwich);

			var dict = new Dictionary<Character, float>
			{
				[candidate_soup] = 1.5f,
				[candidate_sandwich] = 0.5f
			};
			
			// Assert.
			Assert.IsEqual(succession.DivideVotingPie(dict)[candidate_soup], 0.75f);
			Assert.IsEqual(succession.DivideVotingPie(dict)[candidate_sandwich], 0.25f);
		}
		
		[UnitTest("CalculateVotes() test.")]
		[TestArguments(null, 1000UL, 1.0f, 1.0f, 500UL)]
		[TestArguments(null, 1000UL, 1.0f, 2.0f, 750UL)]
		[TestArguments(null, 1000UL, 0.5f, 1.0f, 250UL)]
		[TestArguments(null, 1000UL, 1.0f, 0.5f, 333UL)]
		[TestArguments(null, 1000UL, 0f, 0f, 0UL)]
		public void CalculateVoteTest(ulong size, float percentage, float turnout_power, ulong expected_votes)
		{
			// Arrange.
			var government = Government.MakeStandardizedGovernment(new Universes.Universe());
			var succession = new ElectiveSuccession(government);

			// Act.

			// Assert.
			Assert.IsEqual(succession.CalculateVotes(size, percentage, turnout_power), expected_votes);
		}
	}
}