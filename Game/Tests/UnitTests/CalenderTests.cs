﻿using System;
using Autonomy.Calenders;

namespace Autonomy.Test
{
	public class CalenderTests
	{
		[UnitTest("Ensures that the calender increments days properly.")]
		public void IncrementDayTest()
		{
			// Arrange.
			GameCalender calender = new GameCalender();
			
			// Act.
			calender.Advance();
			
			// Assert.
			Assert.IsEqual(calender.CurrentDate.CompareTo(calender.StartingDate), 1);
		}
		
		[UnitTest("Ensures that the uptime variable is being updated.")]
		public void UptimeTest()
		{
			// Arrange.
			GameCalender calender = new GameCalender();
			
			// Act.
			calender.Advance();
			
			// Assert.
			Assert.IsEqual(calender.Uptime.Days, 1);
		}
		
		[UnitTest("Ensures that the GameCalender object's current date can be saved and loaded properly.")]
		public void SerializationTest()
		{
			// Arrange.
			GameCalender calender = new GameCalender();
			
			// Act.
			calender.Advance();
			string json = Serialization.ObjectToJSON(calender);
			GameCalender loaded_calender = (GameCalender)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.IsEqual(loaded_calender.CurrentDate, calender.CurrentDate);
			Assert.IsEqual(loaded_calender.Uptime, calender.Uptime);
		}
	}
}
