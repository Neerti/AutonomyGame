﻿using Autonomy;
using Autonomy.States;
using Autonomy.People.Populations;

namespace Autonomy.Test
{
	public class StateTest
	{
		[UnitTest("GetAllVassalPopulations() functions.")]
		public void StateVasselPopTest()
		{
			// Arrange.
			Universes.Universe micro_universe = new Universes.Universe();
			State state = State.MakeStandardizedState(micro_universe);
			
			// Act.
			Population total_pop = state.GetAllVassalPopulations();
			
			// Assert.
			Assert.IsEqual(total_pop.Demographics.Count, 3);
		}

		[UnitTest("State serialization.")]
		public void SerializationTest()
		{
			// Arrange.
			Universes.Universe micro_universe = new Universes.Universe();
			State state = State.MakeStandardizedState(micro_universe);
			
			// Act.
			string json = Serialization.ObjectToJSON(state);
			State loaded_state = (State)Serialization.JSONToObject(json, null, null, micro_universe); // TODO: State constructor for custom universes should get shaven down later.
			
			// Assert.
			Assert.IsEqual(state.DisplayName, loaded_state.DisplayName);
			Assert.IsEqual(state.Vassals.Count, loaded_state.Vassals.Count);
			Assert.IsEqual(state.Vassals[0], loaded_state.Vassals[0]);
			Assert.IsEqual(state.Population.ToString(), loaded_state.Population.ToString());
		}
	}
}