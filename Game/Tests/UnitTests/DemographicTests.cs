﻿using Autonomy.People;
using Autonomy.People.Populations;

namespace Autonomy.Test
{
	public class DemographicTests
	{
		[UnitTest("Size adjustment test.")]
		public void AdjustTest()
		{
			// Arrange.
			var dem = new Demographic(100, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);
			
			// Act.
			dem.AdjustSize(900);
			
			// Assert.
			Assert.IsEqual(dem.Size, 1000);
		}

		[UnitTest("Scaling test.")]
		[TestArguments(null, 1.5f, 150UL)]
		[TestArguments(null, 0.5f, 50UL)]
		public void ScaleTest(float scale, ulong expected_size)
		{
			// Arrange.
			var dem = new Demographic(100, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);
			
			// Act.
			dem.ScaleSize(scale);
			
			// Assert.
			Assert.IsEqual(dem.Size, expected_size);
		}

		[UnitTest("Serialization test.")]
		public void SerializationTest()
		{
			// Arrange.
			var dem = new Demographic(100, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);

			// Act.
			string json = Serialization.ObjectToJSON(dem);
			var loaded_dem = (Demographic)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.IsEqual(dem.Size, loaded_dem.Size);
			Assert.IsEqual(dem.Species, loaded_dem.Species);
			Assert.IsEqual(dem.IdeologyID, loaded_dem.IdeologyID);
		}
	}
}