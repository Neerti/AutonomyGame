﻿using Autonomy.People;
using Autonomy.People.Species;

namespace Autonomy.Test
{
	public class SpeciesTests
	{
		[UnitTest("Species random name generator.")]
		[TestArguments(null, Genders.Female, SpeciesIDs.Human)]
		[TestArguments(null, Genders.Neutral, SpeciesIDs.Human)]
		[TestArguments(null, Genders.Male, SpeciesIDs.Human)]
		[TestArguments(null, Genders.Female, SpeciesIDs.AI)]
		[TestArguments(null, Genders.Neutral, SpeciesIDs.AI)]
		[TestArguments(null, Genders.Male, SpeciesIDs.AI)]
		public void SpeciesNameTest(Genders gender, SpeciesIDs id)
		{
			// Arrange.
			CharacterSpecies species = Glob.AllSpecies[id];
			
			// Act.
			string name = species.GenerateName(gender);
			
			// Assert.
			Assert.IsNotNull(name);
		}
	}
}