﻿using Autonomy.People;
using Autonomy.People.Characters;
using Autonomy.People.Ideologies;

namespace Autonomy.Test
{
	public class IdeologyTests
	{
		[UnitTest("Ensures that ideology alignment functions properly.")]
		[TestArguments(null, IdeologyIDs.BLTSandwich, IdeologyIDs.BLTSandwich, Ideology.Alignment.Identical)]
		[TestArguments(null, IdeologyIDs.BLTSandwich, IdeologyIDs.SubSandwich, Ideology.Alignment.Allied)]
		[TestArguments(null, IdeologyIDs.BLTSandwich, IdeologyIDs.Undecided, Ideology.Alignment.Neutral)]
		[TestArguments(null, IdeologyIDs.BLTSandwich, IdeologyIDs.TomatoSoup, Ideology.Alignment.Opposed)]
		public void IdeologyAlignmentTest(IdeologyIDs first, IdeologyIDs second, Ideology.Alignment expected_alignment)
		{
			// Arrange.
			Character A = Character.MakeStandardizedHuman();
			Character B = Character.MakeStandardizedHuman();

			// Act.
			A.SetIdeology(first);
			B.SetIdeology(second);

			// Assert.
			Assert.IsEqual(A.Ideology.GetAlignment(B), expected_alignment);
		}
	}
}