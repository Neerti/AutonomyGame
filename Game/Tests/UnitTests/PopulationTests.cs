﻿using System.Collections.Generic;

using Autonomy.People;
using Autonomy.People.Populations;

namespace Autonomy.Test
{
	public class PopulationTests
	{
		[UnitTest("Add to empty container test.")]
		public void AddToEmptyTest()
		{
			// Arrange.
			var pop = new Population();
			var dem = new Demographic(1000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			
			// Act.
			pop.AddDemographic(dem);
			
			// Assert.
			Assert.IsEqual(pop.Sum(), 1000UL);
		}
		
		[UnitTest("Merge equivalent demographics test.")]
		public void MergeEquivalentTest()
		{
			// Arrange.
			var pop = new Population();
			var dem1 = new Demographic(1000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			var dem2 = new Demographic(500, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			
			// Act.
			pop.AddDemographic(dem1);
			pop.AddDemographic(dem2);
			
			// Assert.
			Assert.IsEqual(pop.Demographics.Count, 1);
		}
		
		[UnitTest("Merge distinct demographics test.")]
		public void MergeDistinctTest()
		{
			// Arrange.
			var pop = new Population();
			var dem1 = new Demographic(1000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human);
			var dem2 = new Demographic(500, IdeologyIDs.BLTSandwich, SpeciesIDs.Human);
			
			// Act.
			pop.AddDemographic(dem1);
			pop.AddDemographic(dem2);
			
			// Assert.
			Assert.IsEqual(pop.Demographics.Count, 2);
		}
		
		[UnitTest("Merge two populations test.")]
		public void MergePopsTest()
		{
			// Arrange.
			var dems1 = new List<Demographic> {
				new Demographic(1000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human),
				new Demographic(500, IdeologyIDs.SubSandwich, SpeciesIDs.Human)
			};
			
			var dems2 = new List<Demographic> {
				new Demographic(2000, IdeologyIDs.SubSandwich, SpeciesIDs.Human),
				new Demographic(1500, IdeologyIDs.TomatoSoup, SpeciesIDs.Human)
			};
			
			var pop1 = new Population(dems1);
			var pop2 = new Population(dems2);

			// Act.
			var merged_pop = pop1.Merge(pop2);

			// Assert.
			Assert.IsEqual(merged_pop.Demographics.Count, 3);
		}

		[UnitTest("Filter ideology test.")]
		public void FilterIdeologyTest()
		{
			// Arrange.
			var dems = new List<Demographic> {
				new Demographic(5000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human),
				new Demographic(500, IdeologyIDs.TomatoSoup, SpeciesIDs.Human),
				new Demographic(2000, IdeologyIDs.TomatoSoup, SpeciesIDs.AI)
			};
			var pop = new Population(dems);
			
			// Act.
			var filtered_pop = pop.Filter(Population.PopulationFilters.Ideology, IdeologyIDs.TomatoSoup);
			
			// Assert.
			Assert.IsEqual(filtered_pop.Demographics.Count, 2);
		}

		[UnitTest("Distribute test.")]
		[TestArguments(null, 1000, 12000UL)]
		[TestArguments(null, -1000, 10000UL)]
		public void DistrubuteTest(int amount_to_give, ulong expected_size)
		{
			// Arrange.
			var dems = new List<Demographic> {
				new Demographic(10000, IdeologyIDs.BLTSandwich, SpeciesIDs.Human),
				new Demographic(1000, IdeologyIDs.TomatoSoup, SpeciesIDs.Human)
			};
			var pop = new Population(dems);
			
			// Act.
			pop.Distribute(amount_to_give);
			
			// Assert.
			Assert.IsEqual(pop.Sum(), expected_size);
		}
		
		[UnitTest("Serialization test.")]
		public void SerializationTest()
		{
			// Arrange.
			var dems = new List<Demographic> {
				new Demographic(5500, IdeologyIDs.BLTSandwich, SpeciesIDs.Human),
				new Demographic(2250, IdeologyIDs.TomatoSoup, SpeciesIDs.Human)
			};
			var pop = new Population(dems);
			
			// Act.
			string json = Serialization.ObjectToJSON(pop);
			var loaded_pop = (Population)Serialization.JSONToObject(json);
			
			// Assert.
			Assert.IsEqual(pop.Sum(), loaded_pop.Sum());
			Assert.IsEqual(pop.Demographics.Count, loaded_pop.Demographics.Count);
		}
	}
}