﻿using System;
using Autonomy.States.Governments;

namespace Autonomy.Test
{
	public class GovernmentTests
	{
		DateTime end_date = new DateTime(2205, 1, 1);
		
		[UnitTest("IsEndOfTerm() test.")]
		public void EndOfTermCheckTest()
		{
			// Arrange.
			Universes.Universe micro_universe = new Universes.Universe();
			Government government = Government.MakeStandardizedGovernment(micro_universe);
			
			// Act.
			government.SetTermLengthOption(Government.TermLengthOptions.ShortTerm);
			
			// Assert.
			// End was yesterday.
			Assert.True(government.IsEndOfTerm(end_date, end_date.AddDays(1)));
			
			// End is today.
			Assert.True(government.IsEndOfTerm(end_date, end_date));
			
			// End is tomarrow.
			Assert.False(government.IsEndOfTerm(end_date, end_date.AddDays(-1)));
		}
	}
}