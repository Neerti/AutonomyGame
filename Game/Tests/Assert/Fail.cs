﻿namespace Autonomy.Test
{
	public static partial class Assert
	{
		/// <summary>
		/// Intentionally fails the unit test being ran.
		/// </summary>
		/// <param name="message">String to include in the failure message.</param>
		public static void Fail(string message)
		{
			throw new FailedAssertException(message);
		}
	}
}
