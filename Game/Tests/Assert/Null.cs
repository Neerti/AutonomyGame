﻿using System;

namespace Autonomy.Test
{
	public static partial class Assert
	{
		[UnitTest("Unit Tests - Null Assert")]
		[TestArguments(true, null)]
		[TestArguments(false, 0)]
		[TestArguments(false, "null")]
		[TestArguments(false, 'a')]
		static bool Null(object a)
		{
			return a is null;
		}
		
		/// <summary>
		/// Asserts that the object passed is null, or fails the unit test.
		/// </summary>
		/// <param name="a">Something which must be null to pass the unit test.</param>
		public static void IsNull(object a)
		{
			if(Null(a))
			{
				return;
			}
			throw new FailedAssertException();
		}
		
		/// <summary>
		/// Asserts that the object passed exists and is not null, or fails the unit test.
		/// </summary>
		/// <param name="a">Something which must exist to pass the unit test.</param>
		public static void IsNotNull(object a)
		{
			if(!Null(a))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}
