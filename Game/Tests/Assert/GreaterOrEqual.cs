﻿namespace Autonomy.Test
{
	public static partial class Assert
	{
		/// <summary>
		/// Asserts that the first parameter is greater than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void GreaterOrEqual(int a, int b)
		{
			if(GreaterComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void GreaterOrEqual(long a, long b)
		{
			if(GreaterComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void GreaterOrEqual(float a, float b)
		{
			if(GreaterComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void GreaterOrEqual(double a, double b)
		{
			if(GreaterComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}
