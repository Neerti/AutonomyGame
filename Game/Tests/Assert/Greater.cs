﻿namespace Autonomy.Test
{
	public static partial class Assert
	{
		// Integers comparator.
		[UnitTest("Unit Tests - Greater Than - Integer")]
		[TestArguments(true, 1, 0)]
		[TestArguments(false, 0, 1)]
		[TestArguments(false, 1, 1)]
		static bool GreaterComparator(long a, long b)
		{
			return a > b;
		}
		
		// Floating point comparator.
		[UnitTest("Unit Tests - Greater Than - Floating Point")]
		[TestArguments(true, 1f, 0f)]
		[TestArguments(false, 0f, 1f)]
		[TestArguments(false, 1f, 1f)]
		static bool GreaterComparator(double a, double b)
		{
			return a > b;
		}
		
		/// <summary>
		/// Asserts that the first parameter is greater than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Greater(int a, int b)
		{
			if(GreaterComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Greater(long a, long b)
		{
			if(GreaterComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Greater(float a, float b)
		{
			if(GreaterComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is greater than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Greater(double a, double b)
		{
			if(GreaterComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}