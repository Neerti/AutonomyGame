﻿namespace Autonomy.Test
{
	public static partial class Assert
	{
		// Integers comparator.
		[UnitTest("Unit Tests - Less Than - Integer")]
		[TestArguments(true, 0, 1)]
		[TestArguments(false, 1, 0)]
		[TestArguments(false, 1, 1)]
		static bool LessComparator(long a, long b)
		{
			return a < b;
		}
		
		// Floating point comparator.
		[UnitTest("Unit Tests - Less Than - Floating Point")]
		[TestArguments(true, 0f, 1f)]
		[TestArguments(false, 1f, 0f)]
		[TestArguments(false, 1f, 1f)]
		static bool LessComparator(double a, double b)
		{
			return a < b;
		}
		
		/// <summary>
		/// Asserts that the first parameter is less than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Less(int a, int b)
		{
			if(LessComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
		
		/// <summary>
		/// Asserts that the first parameter is less than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Less(long a, long b)
		{
			if(LessComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
		
		/// <summary>
		/// Asserts that the first parameter is less than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Less(float a, float b)
		{
			if(LessComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
		
		/// <summary>
		/// Asserts that the first parameter is less than the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void Less(double a, double b)
		{
			if(LessComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}