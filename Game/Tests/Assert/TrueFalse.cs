﻿using System;

namespace Autonomy.Test
{
	public static partial class Assert
	{
		static bool BooleanComparator(bool a)
		{
			return a is true;
		}
		
		/// <summary>
		/// Asserts that the boolean passed is true, or fails the unit test.
		/// </summary>
		/// <param name="a">A bool which must be true to pass the unit test.</param>
		public static void True(bool a)
		{
			if(BooleanComparator(a))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the boolean passed is false, or fails the unit test.
		/// </summary>
		/// <param name="a">A bool which must be false to pass the unit test.</param>
		public static void False(bool a)
		{
			if(!BooleanComparator(a))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}