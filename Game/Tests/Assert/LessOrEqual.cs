﻿namespace Autonomy.Test
{
	public static partial class Assert
	{
		/// <summary>
		/// Asserts that the first parameter is less than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		[UnitTest("Unit Tests - Less Than or Equals - Integer")]
		[TestArguments(null, 0, 1)]
		[TestArguments(null, 1, 1)]
		public static void LessOrEqual(int a, int b)
		{
			if(LessComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is less than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		public static void LessOrEqual(long a, long b)
		{
			if(LessComparator(a, b) || EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is less than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		/// <param name="epsilon">How close the floating point numbers need to be 
		/// to be considered equal.</param>
		public static void LessOrEqual(float a, float b, float epsilon = 0.001f)
		{
			if(LessComparator(a, b) || EqualComparator(a, b, epsilon))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is less than, or equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first number to compare.</param>
		/// <param name="b">The second number to compare.</param>
		/// <param name="epsilon">How close the floating point numbers need to be 
		/// to be considered equal.</param>
		public static void LessOrEqual(double a, double b, float epsilon = 0.001f)
		{
			if(LessComparator(a, b) || EqualComparator(a, b, epsilon))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}
