﻿using System;

namespace Autonomy.Test
{
	public static partial class Assert
	{
		// Integer comparator.
		[UnitTest("Unit Tests - Equality - Integer")]
		[TestArguments(true, 0, 0)]
		[TestArguments(false, 0, 1)]
		[TestArguments(false, 1, 0)]
		[TestArguments(true, 1, 1)]
		static bool EqualComparator(long a, long b)
		{
			return a == b;
		}
		
		// Integer comparator.
		[UnitTest("Unit Tests - Equality - Integer")]
		[TestArguments(true, 0UL, 0UL)]
		[TestArguments(false, 0UL, 1UL)]
		[TestArguments(false, 1UL, 0UL)]
		[TestArguments(true, 1UL, 1UL)]
		static bool EqualComparator(ulong a, ulong b)
		{
			return a == b;
		}

		// Floating point comparator.
		[UnitTest("Unit Tests - Equality - Floating Point")]
		[TestArguments(true, 1.0f, 1.0f, 0.001f)]
		[TestArguments(false, 0.9f, 1.0f, 0.001f)]
		static bool EqualComparator(double a, double b, float epsilon = 0.001f)
		{
			if(Math.Abs(a - b) < epsilon)
			{
				return true;
			}
			return false;
		}
		

		// Object comparator.
		static bool EqualComparator(object a, object b)
		{
			return a.Equals(b);
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(int a, int b)
		{
			if(EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(long a, long b)
		{
			if(EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(ulong a, ulong b)
		{
			if(EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(float a, float b, float epsilon = 0.001f)
		{
			if(EqualComparator(a, b, epsilon))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(double a, double b, float epsilon = 0.001f)
		{
			if(EqualComparator(a, b, epsilon))
			{
				return;
			}
			throw new FailedAssertException();
		}

		/// <summary>
		/// Asserts that the first parameter is equal to the second one, or fails the unit test.
		/// </summary>
		/// <param name="a">The first object to compare.</param>
		/// <param name="b">The second object to compare.</param>
		public static void IsEqual(object a, object b)
		{
			if(EqualComparator(a, b))
			{
				return;
			}
			throw new FailedAssertException();
		}
	}
}