﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Autonomy.Test
{
	/// <summary>
	/// Class which contains information to perform a specific unit test, and 
	/// the results of the test.
	/// </summary>
	public class TestInfo
	{
		public MethodInfo MethodToTest { get; }
		public string Description { get; }
		public object ExpectedReturn { get; }
		public object[] Arguments { get; }
		public bool Failed { get; private set; }
		
		StringBuilder failed_message = new StringBuilder();
		
		public TestInfo(MethodInfo method, string desc, object expected, params object[] args)
		{
			MethodToTest = method;
			Description = desc;
			ExpectedReturn = expected;
			Arguments = args;
		}
		
		public string FailureMessage()
		{
			return failed_message.ToString();
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine($"Name: {MethodToTest.Name}");
			if(!(Description is null))
			{
				sb.AppendLine($"Description: {Description}");
			}
			
			sb.AppendLine($"Returns: {DetailedToString(ExpectedReturn)}");
			
			if(Arguments?.Length > 0)
			{
				sb.AppendLine($"Parameters:");
				foreach(var arg in Arguments)
				{
					sb.AppendLine($"{DetailedToString(arg)}");
				}
			
			}
			return sb.ToString();
		}

		public void RunTest()
		{
			object thing = null;
			object returned = null;
			
			if(!MethodToTest.IsStatic)
			{
				thing = Activator.CreateInstance(MethodToTest.DeclaringType);
			}
			
			try
			{
				returned = MethodToTest.Invoke(thing, Arguments);
			}
			catch(TargetInvocationException ex)
			{
				if(ex.InnerException is FailedAssertException) // An Assert failed.
				{
					Failed = true;
					failed_message.AppendLine($"Assertion failed during test.");
					failed_message.AppendLine(ex.InnerException.Message);
					// TODO: Put more information about what assertion failed, 
					// inside the exception.
				}
				else // Something else went wrong.
				{
					Failed = true;
					failed_message.AppendLine($"Exception was thrown during test. - {ex}");
				}
			}
			catch(Exception ex) // Something else went wrong elsewhere.
			{
				Failed = true;
				failed_message.AppendLine($"Exception was thrown during test. - {ex}");
			}
			
			
			if(!(ExpectedReturn is null))
			{
				if(!ExpectedReturn.Equals(returned))
				{
					Failed = true;
					failed_message.AppendLine($"Expected " +
						$"'{DetailedToString(ExpectedReturn)}', but " +
						$"returned '{DetailedToString(returned)}' instead.");
				}
			}
		}
		
		string DetailedToString(object thing)
		{
			if(thing is null)
			{
				return "NULL";
			}
			return $"{thing.ToString()} ({thing.GetType()})";
		}
	}
	
	
}


