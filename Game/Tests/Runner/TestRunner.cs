using Godot;
using System.Collections.Generic;
using System.Diagnostics;

namespace Autonomy.Test
{
	/// <summary>
	/// This object runs all the test classes defined for it, and outputs the result.
	/// </summary>
	public class TestRunner : Node
	{
		int failed_tests;
		int tests_ran;
		
		public override void _Ready()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();
			
			LoadedUniverse.NewUniverse();
			
			GD.Print("Running automated tests.");
			bool overall_success = RunTests();
			if(overall_success)
			{
				GD.Print("*** All Tests Passed [" + tests_ran + "] ***");
			}
			else
			{
				GD.Print("!!! [" + failed_tests + "/" + tests_ran + "] Tests Failed !!!");
			}
			stopwatch.Stop();
			GD.Print("Tests took " + stopwatch.Elapsed.TotalMilliseconds + " milliseconds (" + stopwatch.Elapsed.TotalSeconds + " seconds) to run.");
			OS.RequestAttention();
		}
		
		/// <summary>
		/// Runs all the tests written for the game.
		/// </summary>
		/// <returns><c>true</c>, if no errors occured in testing, <c>false</c> otherwise.</returns>
		public bool RunTests()
		{
			DoTests(TestFinder.FindTests());
			return failed_tests == 0;
		}
		
		
		public bool DoTests(List<TestInfo> tests)
		{
			foreach(var test in tests)
			{
			//	GD.Print(test);
				test.RunTest();
				if(test.Failed)
				{
					failed_tests++;
					GD.Print(test.ToString());
					GD.Print(test.FailureMessage());
				}
				tests_ran++;
			}
			return failed_tests == 0;
		}
	}
	
}

