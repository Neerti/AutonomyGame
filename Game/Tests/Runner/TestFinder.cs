﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Autonomy.Test
{
	/// <summary>
	/// Helper class which locates all unit tests defined in the code.
	/// </summary>
	public static class TestFinder
	{
		public static List<TestInfo> FindTests()
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			Type[] types = assembly.GetTypes();
			
			List<TestInfo> tests = new List<TestInfo>();
			
			foreach(var class_type in types)
			{
				foreach(var method in class_type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
				{
					var unit_test_attribute = method.GetCustomAttribute<UnitTest>();
					if(unit_test_attribute is null)
					{
						continue; // No UnitTest attribute means this isn't a test.
					}
					
					var test_argument_attributes = method.GetCustomAttributes<TestArguments>();
					if(test_argument_attributes.ToList().Count > 0)
					{
						// Each argument attribute will make a seperate, isolated test.
						foreach(var test_argument in test_argument_attributes)
						{
							TestInfo info = new TestInfo(
								method, 
								unit_test_attribute.Description, 
								test_argument.ExpectedReturn, 
								test_argument.Arguments
								);
							tests.Add(info);
						}
					}
					else // No argument attribute implies a single void method test, with no parameters.
					{
						TestInfo info = new TestInfo(
							method, 
							unit_test_attribute.Description, 
							null, 
							null
							);
						tests.Add(info);
					}
				}
			}
			
			return tests;
		}
		
	}
}
