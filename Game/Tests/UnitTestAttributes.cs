﻿using System;
namespace Autonomy.Test
{
	/// <summary>
	/// Attribute which tells the TestRunner object that the method is a test.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public sealed class UnitTest : Attribute
	{
		public UnitTest(string description)
		{
			Description = description;
		}
		
		public UnitTest() { }
		
		public string Description { get; }
	}
	
	/// <summary>
	/// Attribute which tells the TestRunner to use specific arguments, and what 
	/// to expect back, when testing a method that also has the UnitTest attribute. 
	/// The first parameter is always for what the expected return value is, use 
	/// null if it is a void method. The other parameters are the parameters 
	/// to test the method with. 
	/// Can be added multiple times to run the same method with different inputs.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public sealed class TestArguments : Attribute
	{
		public TestArguments(params object[] args)
		{
			if(!(args is null))
			{
				ExpectedReturn = args[0];
				Arguments = new object[args.Length-1];
				Array.Copy(args, 1, Arguments, 0, args.Length-1);
			}
		}
		
		public object[] Arguments { get; }
		public object ExpectedReturn { get; }
	}
}
